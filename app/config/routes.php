<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'Login/login_main';

$route['kiosk/kiosk_main'] = 'kiosk/Kiosk_main';

$route['Xray/Xray_main'] = 'Xray/Xray_main';

$route['lab'] = 'Lab';

$route['api/lab/v1/mobile/(.*)'] = 'api_lab/$1';
$route['api/xray/v1/mobile/(.*)'] = 'api_xray/$1';

$route['api/xray/sendqueue'] = 'api_xray/sendqueue';
$route['api/xray/getwaitinghold'] = 'api_xray/getWaitingHold';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
