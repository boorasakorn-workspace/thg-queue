<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login_md extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        //$this->load->database('database');

        $this->db_xray = $this->load->database('db_xray', TRUE);
    }

    public function login_md()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');


        $array = array(
            'username' => $username,
            'password' => $password
        );

        $this->db_xray->select('*')
            ->from('user_auth')
            ->where($array);

        $query = $this->db_xray->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
}
