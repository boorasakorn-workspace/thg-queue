<?php
defined('BASEPATH') or exit('No direct script access allowed');

class api_xray extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('assets');
        $this->load->helper('json');
        $this->load->helper('recursiveconvert');
        $this->load->helper('calculate');
        $this->load->model('Xray/Xray_md');
        $this->load->model('Xray/Xray_select_md');
    }

    public function get($queueno, $version = 'v1')
    {
        switch ($version):
            case 'v1':
                $this->load->model('MDL_APIXray');
                $result = $this->MDL_APIXray->getDataApi($queueno);
                echo json_encode($result);
                break;
        endswitch;
    }

    public function sendqueue()
    {

        $hn = $this->input->post('hn');
        $vn = $this->input->post('vn');
        $queue_vn = $this->input->post('queue_vn');
        $xray_location_id = $this->input->post('xray_location_id');

        $ordernew = ($this->input->post('ordernew') == null) ? '' : $this->input->post('ordernew');

        $result_patient = $this->Xray_md->search_patient(); // ค้นหา patient [จาก db เดิม]
        $result_order = $this->Xray_md->list_xraycategory(); // ดึงรายการ xray category

        $check_close_all = $this->Xray_md->checkCloseAll();


        if (($check_close_all[0]['count_hn'] != 0 && $check_close_all[1]['count_hn'] != 0) && $check_close_all[0]['count_hn'] == $check_close_all[1]['count_hn'] && $check_close_all != false) { // hn เก่า ในระบบ xray เคยปิดไปหมดแล้ว
            $ordernew = 'insert_pt_novw';
        }


        $this->genOrder($result_patient, $result_order, $ordernew, $xray_location_id);
    }

    public function genOrder($result_patient, $result_order, $ordernew, $xray_location_id)
    {
        $insert_PatientDetail = $this->Xray_md->Ins_PatientDetailLocation($result_patient, $result_order, $ordernew, $xray_location_id); // insert patientdetail , patientxray , processcontrol

        echo json_encode(['log' => true]);
    }

    public function getWaitingHold()
    {

        $xray_location = $this->input->post('location_id');

        $result = $this->Xray_md->dataWaitingHold();

        if ($result != false) {
            $result[0]['waiting'] = intval($result[0]['waiting'] - $result[0]['queuehold']);

            echo json_encode(['status' => true, 'xraylocation' => $result[0]['xray_location'], 'waiting' => $result[0]['waiting'], 'hold' => $result[0]['queuehold']]);
        } else {

            echo json_encode(['status' => false, 'xraylocation' => $xray_location, 'waiting' => '0', 'hold' => '0']);
        }
    }
}
