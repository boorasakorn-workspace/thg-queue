<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MDL_APIXray extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);
        $this->db_xray = $this->load->database('db_xray', TRUE);
    }

    function getDataApi($secure_code)
    {

        $get_head_secure_code = '';
        $get_head_tokenno = '';
        $get_head_hn = '';
        $get_head_nation = '';
        $result_format_info = '';
        $result_format_xray = '';

        $get_head = $this->db->select('*')
            ->from('vw_api_mobile_labxray')
            ->where('secure_code', $secure_code)->limit('1')->get();

        if ($get_head->num_rows() > 0) {
            $result_head =  $get_head->result_array();

            $get_head_secure_code = $result_head[0]['secure_code'];
            $get_head_tokenno = $result_head[0]['tokenno'];
            $get_head_hn = $result_head[0]['hn'];
            $get_head_nation = $result_head[0]['nation'];

            $get_vw_patient = $this->db->select('*')
                ->from('vw_api_mobile_labxray')
                ->where('secure_code', $result_head[0]['secure_code'])
                ->get()->result_array();

            $result_format_info = [];
            foreach ($get_vw_patient as $key => $value) {

                $cwhen_current = 'null';
                $text_current = 'null';

                if ($value['qclose'] != null) {
                    $text_current = 'Close';
                    $cwhen_current = date('Y-m-d H:i:s', strtotime($value['qclose']));
                } else if ($value['qcomplete'] != null) {
                    $text_current = 'Complete';
                    $cwhen_current = date('Y-m-d H:i:s', strtotime($value['qcomplete']));
                } else if ($value['qcancel'] != null) {
                    $text_current = 'Cancel';
                    $cwhen_current = date('Y-m-d H:i:s', strtotime($value['qcancel']));
                }


                $text1_call_hold = 'null';
                $time1_call_Hold = 'null';

                if ($value['callcwhen'] != null && $value['holdcwhen'] != null) {

                    $text1_call_hold = (strtotime($value['callcwhen']) >  strtotime($value['holdcwhen'])) ? 'Call' : 'Hold';
                    $time1_call_Hold = (strtotime($value['callcwhen']) >  strtotime($value['holdcwhen'])) ? date('Y-m-d H:i:s', strtotime($value['callcwhen'])) : date('Y-m-d H:i:s', strtotime($value['holdcwhen']));
                } else if ($value['callcwhen'] != null && $value['holdcwhen'] == null) {

                    $time1_call_Hold = date('Y-m-d H:i:s', strtotime($value['callcwhen']));
                    $text1_call_hold = 'Call';
                } else if ($value['callcwhen'] == null && $value['holdcwhen'] != null) {

                    $time1_call_Hold = date('Y-m-d H:i:s', strtotime($value['holdcwhen']));
                    $text1_call_hold = 'Hold';
                }

                $format_information = [
                    "vn" => $value['vn'],
                    "department_code" =>   $value['doctorcode'],
                    "department_name_th" =>   $value['doctorname'],
                    "department_name_en" =>   null,
                    "visit_start" =>   $value['visit_date'],
                    "send_doctor_status" =>   $value['patient_status'],
                    "send_doctor_status_time" =>   $value['visit_date'],
                    "doctor_code" =>   $value['doctorcode'],
                    "doctor_name_th" =>   $value['doctor_send_nameth'],
                    "doctor_name_en" =>  $value['doctor_send_nameen'],
                    "doctor_slot" =>   $value['doctor_slot_time'],
                    "doctor_room" =>   $value['doctor_room'],
                    "queue_waiting" =>   number_format($value['waittime']),
                    "queue_event_call_hold" =>   $text1_call_hold,
                    "queue_event_call_hold_time" =>   $time1_call_Hold,
                    "queue_call_room" =>   $value['queue_call_room'],
                    "queue_current_status" =>   $text_current,
                    "queue_current_status_time" => $cwhen_current,
                ];
                array_push($result_format_info, $format_information);
            }
        } else {
            $result_format_info = '';
        }

        $getvw_xray = $this->db_xray->select('*')
            ->from('vw_api_mobile_xray')
            ->where('secure_code', $secure_code)->order_by('patientdetailuid', 'asc')->get();


        if ($getvw_xray->num_rows() > 0) {
            $result_pdxray = $getvw_xray->result_array();

            $result_format_xray = [];
            foreach ($result_pdxray as $valuexray) {

                $text_call_hold = 'null';
                if ($valuexray['callholdstatus'] != null) {
                    $text_call_hold = ($valuexray['callholdstatus'] == '4') ? 'Call' : 'Hold';
                }

                $time_call_hold = 'null';
                if ($valuexray['callholdtime'] != null) {
                    $time_call_hold = date('Y-m-d H:i:s', strtotime($valuexray['callholdtime']));
                }


                $text_current = 'null';
                if ($valuexray['currentstatus'] != null) {
                    $text_current = ($valuexray['currentstatus'] == '6') ? 'Complete' : 'Close';
                }

                $time_current = 'null';
                if ($valuexray['currenttime'] != null) {
                    $time_current = date('Y-m-d H:i:s', strtotime($valuexray['currenttime']));
                }

                $format_pdxray = [
                    "patientdetailuid" => $valuexray['patientdetailuid'],
                    "patientxrayuid" => $valuexray['patientxrayuid'],
                    "patientxraytype" => $valuexray['xraycategoryuid'],
                    "ordername" => $valuexray['ordername'],
                    "queueno" =>  $valuexray['queueno'],
                    "vn" => $valuexray['en'],
                    "send_status" => $valuexray['sendstatus_px'],
                    'send_time' => $valuexray['send_time'],
                    "waiting" => $valuexray['waiting'],
                    "call_hold" => $text_call_hold,
                    "call_hold_time" => $time_call_hold,
                    "call_room" => $valuexray['call_list'],
                    "current_status" => $text_current,
                    "current_status_time" => $time_current,
                ];

                array_push($result_format_xray, $format_pdxray);
            }
        } else {
            $result_format_xray = '';
        }


        $result_api = [
            "secure" =>    $get_head_secure_code,
            "queueno" => $get_head_tokenno,
            "hn" =>     $get_head_hn,
            "language" => $get_head_nation,
            "infomation" => $result_format_info,
            'dataxray' => $result_format_xray
        ];

        return $result_api; //$result_head //$result_api
    }
}
