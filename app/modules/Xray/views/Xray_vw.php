<div class="management-container">
    <input type="hidden" id="box_search_tmp">
    <div id="ManagementOuterTab" class="header row no-gutters" style="padding-left: 1.2vw; height: 6%;">

        <div class="col-12" style="display:flex;align-items: center;justify-content: space-between;">
            <div>
                <span class="main_h">DASHQUEUE</span>
            </div>
            <div>
                <i class="fas fa-sign-out-alt" style="font-size:16px; padding-right:10px;" id="logout"></i>
            </div>
        </div>
    </div>


    <div id="ManagementDashboard" class="content" style="position: relative;height:94%; border:none;">

        <!-- Management Content -->
        <div class=" row no-gutters menu_style">

            <div class="col-9" style="padding: 10px; display: flex; align-items: center ;" id="area_main_order">

            </div>

            <div class="col-3 center" style="padding: 4px; border: 1px solid #96aec5;border-radius: .3rem;">
                <div class="row col-12 center loding_log" id="loding_log_scan" style="display:none;">
                    <i id="loading" class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
                </div>
                <div class="" style="width: 100%; height: 100%; background-color: #33b5e5;border-radius: .3rem;">
                    <div class="col-12 center" style="height: 40%;">
                        <?= single_img('img/logo_thonburi_br.png', array('style' => 'width: 80%;')) ?>
                    </div>
                    <div class="col-12 center" style="height: 60%; ">
                        <div>
                            <span style="font-size: 1.6vw; color: #fff; -webkit-text-stroke: .3px #fff;">Scan HN เข้ารับบริการ</span>
                            <input type="text" id="box_search" class="border" style="height: 3.2vw; width: 100%; border-radius: 1.4rem;    outline: none; text-align: center;">
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <!-- Dashboard Table -->
        <div class=" nowrap_container " id='tb_order_all'>
            <table id="" class="table_data  tb_main table table-striped table-bordered  ui-datatable borderless" style=" margin-top: 0 !important;">
                <thead id="head_list_patient">
                    <tr>
                        <th>#</th>
                        <th>Send</th>
                        <th>No</th>
                        <th>Name</th>
                        <th>HN</th>
                        <th>VN</th>
                        <th>รอ(นาที)</th>
                        <th>Order X-ray</th>
                        <th>Call</th>
                        <th>Print</th>
                        <th>Note</th>
                        <th>ปิดคิว</th>
                        <th>ปิดคิว HN ทั้งหมด</th>
                    </tr>
                </thead>
                <tbody style="" id="body_list_patient">

                </tbody>
            </table>
        </div>
        <!-- /Dashboard Table -->


        <!-- Dashboard Table -->
        <div class=" nowrap_container " id='tb_order_other' style="display: none;">
            <table id="" class="table_data  tb_pn table table-striped table-bordered  ui-datatable borderless" style=" margin-top: 0 !important;">
                <thead id="head_list_patient_other">
                    <tr>
                        <th>#</th>
                        <th>No</th>
                        <th>Call</th>
                        <th>Name</th>
                        <th>HN</th>
                        <th>VN</th>
                        <th>รอ(นาที)</th>
                        <th>Order X-ray</th>
                        <th>Room</th>
                        <!-- <th>Print</th> -->
                        <th>Note</th>
                        <th>Hold</th>
                        <th>ตรวจเสร็จ</th>
                        <th>ปิดคิว</th>
                    </tr>
                </thead>
                <tbody style="" id="body_list_patient_other">

                </tbody>
            </table>
        </div>
        <!-- /Dashboard Table -->



        <script>
            var TableRowCount = 1;
        </script>
        <!-- /Management Content -->

    </div>



    <!-- modal print -->
    <div class="modal" tabindex="-1" role="dialog" id="md_print">
        <div class="modal-dialog modal-dialog-centered" style="max-width: 700px;" role="document">
            <div class="modal-content" style="border: 2px solid #344fa7;">
                <div class="modal-header center" style="min-height: 3rem; padding: .8rem;">
                    <h5 class="modal-title">เลือก Print</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!-- <div class="modal-header center" style="min-height: 3rem; padding: .6rem;">
                    <h5 class="modal-title">เลือก Print</h5>
                </div> -->
                <div class="modal-body col-12" style="padding: 0rem 1rem; min-height: 250px;">
                    <div class="container-fluid">

                        <div class="row" style="margin-top:10px;">
                            <table class="table table-bordered tb_print" style="font-size: 1.555rem;">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>HN</th>
                                        <th>VN</th>
                                        <th>Location</th>
                                        <th>Re-Print</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody_print">

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
                <div class="row col-12" style="display:none;">
                    <input class="col-4" type="hidden" id="patientdetail_uid_print">
                    <input class="col-4" type="hidden" id="patientxray_uid_print">
                    <input class="col-4" type="hidden" id="xraycategory_uid_print">
                    <input class="col-4" type="hidden" id="check_status_type_print">
                    <input class="col-4" type="hidden" id="location_now_print">
                    <input class="col-4" type="hidden" id="secure_code_print">
                </div>
            </div>
        </div>
    </div>

    <!-- modal send -->
    <div class="modal" tabindex="-1" role="dialog" id="md_send">
        <div class="modal-dialog modal-dialog-centered" style="max-width: 700px;" role="document">
            <div class="modal-content">
                <div class="modal-header center" style="min-height: 3rem; padding: .8rem;">
                    <h5 class="modal-title">เลือกส่งคิว</h5>
                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button> -->
                </div>
                <div class="modal-body center">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12 icon_row row" style="padding-bottom: .8rem;" id="area_order_send">

                            </div>
                            <div class="row col-12" style="display: none;">
                                <input class="col-4" type="hidden" id="PatientXray_uid">
                                <input class="col-4" type="hidden" id="PatientDetail_uid">
                                <input class="col-4" type="hidden" id="status_type">
                                <input class="col-4" type="hidden" id="location_now_send">
                            </div>
                        </div>
                        <div class="row center col-12" style="padding: .8rem;">
                            <div class="norad-btn_row">
                                <button class="btn_sm_green" style="margin-right: 20px;" id="conf_send">ตกลง</button>
                                <button class="btn_sm_gray" class="close" data-dismiss="modal">ยกเลิก</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- modal counter -->
    <div class="modal" tabindex="-1" role="dialog" id="md_counter">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header center" style="min-height: 3rem; padding: .8rem;">
                    <h5 class="modal-title">เรียกคิวห้อง <span id='tital_counter'></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="row align-items-end  modal-body main-btn_row">
                    <div class="col-12 center" style=" display: block;" id="list_call">

                    </div>
                    <div class="row col-12" style="display: block;">
                        <input class="col-4" type="hidden" id="order_location_uid">
                        <input class="col-4" type="hidden" id="patientdetail_uid_counter">
                        <input class="col-4" type="hidden" id="patientxray_uid_counter">
                        <input class="col-4" type="hidden" id="status_type_counter">
                        <input class="col-4" type="hidden" id="location_now_counter">
                        <input class="col-4" type="hidden" id="nation_counter">
                        <input class="col-4" type="hidden" id="queueno_counter">
                        <input class="col-4" type="hidden" id="xraycategoryuid_counter">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- modal note -->
    <div class="modal" tabindex="-1" role="dialog" id="md_note">
        <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 600px;">
            <div class="modal-content">
                <div class="modal-header center" style="min-height: 3rem; padding: .8rem;">
                    <h5 class="modal-title">Note</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body center">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col" style="padding: .8rem;">
                                <textarea class="form-control" rows="5" id="textarea_note" name="textarea_note"></textarea>
                            </div>
                        </div>
                        <div class="row col-12" style="display:none;">
                            <input class="col-4" type="hidden" id="patientdetail_uid_note">
                            <input class="col-4" type="hidden" id="patientxray_uid_note">
                            <input class="col-4" type="hidden" id="check_status_type_note">
                            <input class="col-4" type="hidden" id="location_now_note">
                        </div>
                        <div class="row">
                            <div class="col-12 center">
                                <button class="btn btn-flat btn-success" id='conf_note'>บันทึก</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- modal hold -->
    <div class="modal" tabindex="-1" role="dialog" id="md_hold">
        <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 600px;">
            <div class="modal-content">
                <div class="modal-header center" style="min-height: 3rem; padding: .8rem;">
                    <h5 class="modal-title">Hold</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body center">
                    <div class="container-fluid" style="padding: 0">
                        <div class="row col-12" style="padding: 0 0 .8rem 0;">
                            <div class="col-12">
                                <select class="form-control" id="hold_message-select" style="font-size: 1.5rem;">
                                </select>
                            </div>
                            <div class="col-12" style="padding-top: 1rem">
                                <textarea class="form-control" rows="5" id="hold_message" name="hold_message"></textarea>
                            </div>
                        </div>
                        <div class="row col-12" style="display: none;">
                            <input class="col-4" type="hidden" id="patientdetail_uid_hold">
                            <input class="col-4" type="hidden" id="patientxray_uid_hold">
                            <input class="col-4" type="hidden" id="check_status_type_hold">
                            <input class="col-4" type="hidden" id="location_now_hold">
                        </div>
                        <div class="row">
                            <div class="col" style="text-align:center;">
                                <button class="btn btn-flat btn-success" id="conf_hold">บันทึก</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- modal complete -->
    <div class="modal" tabindex="-1" role="dialog" id="md_complete">
        <div class="modal-dialog modal-dialog-centered" style="max-width: 600px;" role="document">
            <div class="modal-content" style="border: 2px solid #344fa7;">
                <div class="modal-header center" style="min-height: 3rem; padding: .8rem;">
                    <h5 class="modal-title">ตรวจเสร็จ</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body center">
                    <div class="container-fluid">
                        <div class="row center">
                            <h2>ยืนยันการตรวจเสร็จ ?</h2>
                        </div>
                        <div class="row">
                            <div class="col-12 form-control center" style="padding-bottom: .8rem;min-height: 120px; border: none;">
                                <h1 style="font-size: 4rem;" id="complete_show"></h1>
                            </div>
                        </div>
                        <div class="row center col-12">
                            <div class="norad-btn_row">
                                <button class="btn_sm_green" style="margin-right: 20px;" id="conf_complete">คกลง</button>
                                <button class="btn_sm_gray" class="close" data-dismiss="modal">ไม่</button>
                            </div>
                        </div>
                        <div class="row col-12" style="display:none;">
                            <input class="col-4" type="hidden" id="patientdetail_uid_complete">
                            <input class="col-4" type="hidden" id="patientxray_uid_complete">
                            <input class="col-4" type="hidden" id="check_status_type_complete">
                            <input class="col-4" type="hidden" id="location_now_complete">
                            <input class="col-4" type="hidden" id="hn_complete">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- modal close Queue -->
    <div class="modal" tabindex="-1" role="dialog" id="md_closeq">
        <div class="modal-dialog modal-dialog-centered" style="max-width: 600px;" role="document">
            <div class="modal-content" style="border: 2px solid #344fa7;">
                <div class="modal-header center" style="min-height: 3rem; padding: .8rem;">
                    <h5 class="modal-title">ปิดคิว</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body center">
                    <div class="container-fluid">
                        <div class="row center">
                            <h2>ยืนยันการปิดคิว ?</h2>
                        </div>
                        <div class="row">
                            <div class="col-12 form-control center" style="padding-bottom: .8rem;min-height: 120px; border: none;">
                                <h1 style="font-size: 4rem;" id="closeq_show"></h1>
                            </div>
                        </div>
                        <div class="row col-12" style="display:none;">
                            <input class="col-4" type="hidden" id="patientdetail_uid_closeq">
                            <input class="col-4" type="hidden" id="patientxray_uid_closeq">
                            <input class="col-4" type="hidden" id="check_status_type_closeq">
                            <input class="col-4" type="hidden" id="location_now_closeq">
                            <input class="col-4" type="hidden" id="hn_closeq">
                        </div>
                        <div class="row center col-12">
                            <div class="norad-btn_row">
                                <button class="btn_sm_green" style="margin-right: 20px;" id="conf_closeq">ตกลง</button>
                                <button class="btn_sm_gray" class="close" data-dismiss="modal">ไม่</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- modal note -->
    <div class="modal" tabindex="-1" role="dialog" id="md_alert_other">
        <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 600px;">
            <div class="modal-content">
                <div class="modal-header center" style="min-height: 3rem; padding: .8rem;">
                    <h5 class="modal-title">แจ้งเตือน</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body center">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col" style="padding: .8rem; font-size:2rem;" id="alert_text">

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 center">
                                <button class="btn_sm_green" data-dismiss="modal">ตกลง</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- modal note -->
    <div class="modal" tabindex="-1" role="dialog" id="md_order_newqueue">
        <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 600px;">
            <div class="modal-content">
                <div class="modal-header center" style="min-height: 3rem; padding: .8rem;">
                    <h5 class="modal-title">แจ้งเตือน</h5>
                </div>
                <div class="modal-body center">
                    <div class="row col-12 center loding_log2" id="loding_log_scan2" style="display:none;">
                        <i id="loading" class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
                    </div>
                    <div class="container-fluid">
                        <input type="hidden" id="hn_new_queue">
                        <div class="row">
                            <div class="col" style="padding: .8rem; font-size:2.4rem;" id="">
                                คุณต้องการขอคิวหรือไม่
                            </div>
                        </div>
                        <div class="row center col-12">
                            <div class="norad-btn_row">
                                <button class="btn_sm_blue" style="margin-right: 20px;" id="ordernewqueue">ขอคิว</button>
                                <button class="btn_sm_gray" class="close" data-dismiss="modal">ยกเลิก</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- modal duplicate hn -->
    <div class="modal" tabindex="-1" role="dialog" id="md_alert_dduplicate">
        <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 600px;">
            <div class="modal-content">
                <div class="modal-header center" style="min-height: 3rem; padding: .8rem;">
                    <h5 class="modal-title">แจ้งเตือน</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body center">
                    <div class="container-fluid">
                        <div class="row">
                            <div class='col-12'>
                                <span style="padding: .8rem; font-size:2rem; font-weight:bold;" id="showhnduplicate"><span>
                            </div>
                            <div class='col-12'>
                                <span style="padding: .8rem; font-size:2rem;"> VN ที่เคย Scan ผ่านไปแล้ว </span>
                            </div>
                            <div class="col" style="padding: .8rem; font-size:1.8rem;" id="alert_text_duplicate">
                                <table class="table table-bordered tb_dup" style="font-size: 1.555rem;">
                                    <thead>
                                        <tr>
                                            <th>VN</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbody_dup">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class='col-12'>
                                <span style="padding: .8rem; font-size:2rem;"> VN ที่เพิ่มมาใหม่ </span>
                            </div>
                            <div class="col" style="padding: .8rem; font-size:1.8rem;" id="alert_text_duplicate">
                                <table class="table table-bordered tb_newhn" style="font-size: 1.555rem;">
                                    <thead>
                                        <tr>
                                            <th>VN</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbody_newhn">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 center">
                                <button class="btn btn-flat btn-success" data-dismiss="modal" id="closemdnewvn" data-scannew=''>ปิด</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- modal printoutofloc -->
    <div class="modal" tabindex="-1" role="dialog" id="md_printoutofloc">
        <div class="modal-dialog modal-dialog-centered" style="max-width: 700px;" role="document">
            <div class="modal-content" style="border: 2px solid #344fa7;">
                <div class="modal-header center" style="min-height: 3rem; padding: .8rem;">
                    <h5 class="modal-title">เลือก Print location ที่ไม่ไช้ระบบคิว</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!-- <div class="modal-header center" style="min-height: 3rem; padding: .6rem;">
                    <h5 class="modal-title">เลือก Print</h5>
                </div> -->
                <div class="modal-body col-12" style="padding: 0rem 1rem; min-height: 250px;">
                    <div class="container-fluid">

                        <div class="row" style="margin-top:10px;">
                            <table class="table table-bordered tb_print" style="font-size: 1.555rem;">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>HN</th>
                                        <th>VN</th>
                                        <th>Location</th>
                                        <th>Re-Print</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody_printoutofloc">
                                </tbody>
                            </table>
                        </div>
                        <div class="row" style="margin-top:10px;">
                            <button class="btn_sm_green" id="btn_after_ourloc" data-dismiss="modal" style="margin-left: auto; margin-right:auto;">ปิด</button>
                        </div>


                    </div>
                </div>
                <div class="row col-12" style="display:none;">
                </div>
            </div>
        </div>
    </div>



    <!-- modal closeallpt -->
    <div class="modal" tabindex="-1" role="dialog" id="closeallpt">
        <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 600px;">
            <div class="modal-content">
                <div class="modal-header center" style="min-height: 3rem; padding: .8rem;">
                    <h5 class="modal-title">ยืนยันการปิดคิว HN</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body center">
                    <div class="container-fluid" style="padding: 0">
                        <div class="row col-12" style="padding: 0 0 .8rem 0;">
                            <div class="col-12">
                                <span style="padding: .8rem; font-size:2rem;"> คุณต้องการปิดคิวของ HN นี้ทั้งหมดหรือไม่ </span>
                            </div>
                        </div>
                        <div class="row col-12" style="display: none;">
                            <input class="col-4" type="hidden" id="hn_closeallpt">
                            <input class="col-4" type="hidden" id="admin_login_uid_closeallpt">
                            <input class="col-4" type="hidden" id="status_type_closeallpt">
                        </div>
                        <div class="row">
                            <div class="col" style="text-align:center;">
                                <button class="btn_sm_green" id="conf_closeallpt" style="margin-right: 20px;">ตกลง</button>
                                <button class="btn_sm_gray" data-dismiss="modal">ปิด</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- modal closeallpt -->
    <div class="modal" tabindex="-1" role="dialog" id="md_noorder">
        <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 600px;">
            <div class="modal-content">
                <div class="modal-header center" style="min-height: 3rem; padding: .8rem;">
                    <h5 class="modal-title">ไม่พบ Order</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body center">
                    <div class="container-fluid" style="padding: 0">
                        <div class="row col-12 center loding_log3" id="loding_click_noorder" style="display:none;">
                            <i id="loading_noorder" class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
                        </div>
                        <div class="row col-12" style="padding: 0 0 .8rem 0;">
                            <div class="col-12">
                                <span style="padding: .8rem; font-size:2rem;"> กรุณาเลือก order เพื่อส่งคิวผู้ป่วยที่ Scan เข้ามาใหม่เข้าห้องตรวจ </span>
                            </div>
                        </div>
                        <div class="row col-12" style="display: block;">
                            <input type="hidden" id="hn_noorder">
                            <input type="hidden" id="locationuid_login_noorder">
                            <input type="hidden" id="admin_login_uid_noorder">
                        </div>
                        <div class="row">
                            <div class="col" style="text-align:center;">
                                <button class="btn_sm_green" id="conf_createnoorder" style="margin-right: 20px;">ตกลง</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal" tabindex="-1" role="dialog" id="md_alert">
        <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 600px;">
            <div class="modal-content">
                <div class="modal-header center" style="min-height: 3rem; padding: .8rem;">
                    <h5 class="modal-title" id="title_mdalert"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body center">
                    <div class="container-fluid" style="padding: 0">
                        <div class="row col-12" style="padding: 0 0 .8rem 0;">
                            <div class="col-12">
                                <span style="padding: .8rem; font-size:2rem;" id="head_mdalert"> </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col" style="text-align:center;">
                                <button class="btn_sm_green" style="margin-right: 20px;" data-dismiss="modal">ตกลง</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal" tabindex="-1" role="dialog" id="md_gen_newq">
        <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 600px;">
            <div class="modal-content">
                <div class="modal-header center" style="min-height: 3rem; padding: .8rem;">
                    <h5 class="modal-title" id="title_mdalert_newq"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body center">
                    <div class="container-fluid" style="padding: 0">
                        <div class="row col-12" style="padding: 0 0 .8rem 0;">
                            <div class="col-12">
                                <span style="padding: .8rem; font-size:2rem;" id="head_mdalert_newq"> ระบบ Gen หมายเลขบริการให้เรียบร้อยแล้ว <br>
                                    กรุณาเลือก order เพื่อส่งคิวผู้ป่วยที่ Scan เข้ามาใหม่ เข้าห้องตรวจ </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col" style="text-align:center;">
                                <button class="btn_sm_green" style="margin-right: 20px;" id="close_gen_newq" data-dismiss="modal">ตกลง</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal" tabindex="-1" role="dialog" id="md_noorder_closeall">
        <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 600px;">
            <div class="modal-content">
                <div class="modal-header center" style="min-height: 3rem; padding: .8rem;">
                    <h5 class="modal-title">คิวนี้เสร็จสิ้นไปแล้ว</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body center">
                    <div class="container-fluid" style="padding: 0">
                        <div class="row col-12 center loding_log3" id="loding_click_closeall" style="display:none;">
                            <i id="loading_noorder" class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
                        </div>
                        <div class="row col-12" style="padding: 0 0 .8rem 0;">
                            <div class="col-12">
                                <span style="padding: .8rem; font-size:2rem;"> คิวนี้เสร็จสิ้นไปแล้ว หากต้องการส่งคิวเข้ารับบริการอีกครั้ง <br>
                                    กรุณาเลือก order เพื่อส่งคิวผู้ป่วยที่ Scan เข้ามาใหม่ เข้าห้องตรวจ </span>
                            </div>
                        </div>
                        <div class="row col-12" style="display: block;">
                            <input type="hidden" id="hn_noorder_closeall">
                            <input type="hidden" id="locationuid_login_noorder_closeall">
                            <input type="hidden" id="admin_login_uid_noorder_closeall">
                        </div>
                        <div class="row">
                            <div class="col" style="text-align:center;">
                                <button class="btn_sm_green" id="conf_createnoorder_closeall" style="margin-right: 20px;">ตกลง</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>