<script type="text/javascript">
    function redraw() {
        $('.table_data').DataTable().columns.adjust();

        // $('#body_list_patient tr:nth-child(1) > td').each(function(index){
        //     index+=1;
        //     let b4width = $('#head_list_patient tr > th:nth-child('+index+')').width();
        //     console.log("BEFORE : " + b4width + " , " + $(this).width());
        //     $('#head_list_patient tr > th:nth-child('+index+')').css('width', $(this).width() );
        //     let afwidth = $('#head_list_patient tr > th:nth-child('+index+')').width();
        //     console.log("AFTER : " + afwidth);
        // });

        /*
        if ($('#head_list_patient tr').width() != $('#body_list_patient tr').width()) {
            console.log("NOT EQUAL");
            $('#head_list_patient tr > th:last-child').css('padding-right', '20px');
        } else {
            console.log("EQUAL");
            $('#head_list_patient tr > th:last-child').css('padding-right', '0px');
        }
        if ($('#head_list_patient_other tr').width() != $('#bodylist_patient_other tr').width()) {
            console.log("OTHER NOT EQUAL");
            $('#head_list_patient_other tr > th:last-child').css('padding-right', '20px');
        } else {
            console.log("OTHER EQUAL");
            $('#head_list_patient_other tr > th:last-child').css('padding-right', '0px');
        }
        */

    }

    $(document).ready(function() {
        $('.table_data').dataTable({
            ordering: false,
            scrollY: true,
            dom: 'rt'
        });
        $(document).on('click', '.btn_pr', function() {
            $(".btn_pr").removeClass('active');
            $(this).addClass('active');
            $(this).children('div').each(function() {
                if ($(this).children('span.num_pn').length) {
                    $('.num_pn').removeClass('active');
                    $(this).children('span.num_pn').addClass('active');
                }
            });
        })
        $(document).on('click', '.icon_td', function() {
            $(this).addClass('active');
        });
        $(document).on('click', '.icon_x', function() {
            $(this).addClass('active');
        });
    });

    $(document).ready(function() {


        console.log(<?= json_encode($this->session->userdata('userlogin')); ?>, 'userlogin');


        var admin_login = JSON.parse('<?= json_encode($this->session->userdata('userlogin')); ?>');

        try {
            var admin_login_uid = admin_login['uid'];
            var user_roleuid = admin_login['user_roleuid'];
            var locationuid_login = admin_login['locationuid'];
            var sublocationuid_login = admin_login['sublocationuid'];
            var locationuser = admin_login['locationuser'];
        } catch (e) {

            var base_url = "<?= base_url() ?>";

            window.location.href = base_url + 'login/login_main';
        }

        // window.sessionStorage.setItem('userlogin_js', '<?= json_encode($this->session->userdata('userlogin')); ?>');
        // setInterval(function() {
        //     console.log(window.sessionStorage.getItem('userlogin_js'));
        // }, 3000);



        var socket = io.connect('<?= APITRIGGER ?>/nurse');
        var loop_socket = '';
        var count_target = 0;
        var free_loop_socket = '';

        socket.on('patientxray', function(data) {
            // alert('patientxray');
            // console.log(data['locationuid'] + ' = ' + locationuid_login);
            console.log('patientxray socket');

            if (data['locationuid'] == locationuid_login) {

                var value_order = $(".btn_order[active='active']").val();

                if (loop_socket == '') {
                    loop_socket = 'in';
                    console.log(count_target);

                    list_all($(".btn_order[active='active']").val());

                    call_list_order(); // list order main
                    btn_order_click(); // event click order
                    $(".btn_order[value='" + value_order + "']").trigger('click').attr('active', 'active').find('.num_pn').addClass('active');

                } else {
                    count_target += 1;
                    if (free_loop_socket != '') {
                        console.log('zaza');
                        list_all($(".btn_order[active='active']").val());

                        call_list_order(); // list order main
                        btn_order_click(); // event click order
                        $(".btn_order[value='" + value_order + "']").trigger('click').attr('active', 'active').find('.num_pn').addClass('active');

                        free_loop_socket = '';
                        count_target = 0;
                        loop_socket = '';


                    }
                }
            }


            //console.log(count_target + ' ' + loop_socket);
        });



        $("#logout").click(function() {
            $.ajax({
                url: "<?= base_url() ?>Xray/cleatsession",
                success: function(data) {
                    var base_url = "<?= base_url() ?>";
                    window.location.href = base_url + 'login/login_main';
                }
            });
        });

        $("#box_search").keyup(function(e) {

            if (e.keyCode == 13) {

                var check_secure = $("#box_search").val().split('/');
                var data = $("#box_search").val().split('');
                var score = 0;

                $("#box_search").val('');
                for (var i = 0; i < data.length; i++) {
                    if (data[i] == 0) {
                        score += 1;
                    } else {
                        break;
                    }
                }
                var result = score - 1;

                for (var i = 0; i < data.length; i++) {

                    if (i > result) {
                        if (data[i] != ' ') {
                            $("#box_search").val(function() {
                                return this.value + data[i];
                            });
                        }
                    }
                }

                $("#loding_log_scan").css('display', 'flex');

                if ($("#box_search").val() == '') {
                    alert('กรุณากรอกข้อมูลก่อน');
                    $("#loding_log_scan").css('display', 'none');
                    return false;
                }


                setTimeout(function() {
                    $.ajax({
                        url: "<?= base_url() ?>Xray/search_data",
                        type: "post",
                        async: false,
                        data: {
                            hn: $.trim($("#box_search").val()),
                            locationuid_login: locationuid_login,
                            admin_login_uid: admin_login_uid
                        },
                        success: function(data) {
                            console.log(JSON.parse(data));
                            var check = JSON.parse(data);
                            $("#loding_log_scan").css('display', 'none');

                            if (check.status == 'noorder') {
                                $("#md_noorder").modal('show');

                                $("#hn_noorder").val($.trim($("#box_search").val()));
                                $("#locationuid_login_noorder").val(locationuid_login);
                                $("#admin_login_uid_noorder").val(admin_login_uid);

                            } else if (check.status == 'findOrderNotSend') { // ปิดยังไม่ครบ และ มี order ใหม่

                                $("#title_mdalert").html('พบ order ใหม่');
                                $("#head_mdalert").html('พบ order ใหม่ กรุณาส่งคิวเข้าห้อง');
                                $("#md_alert").modal('show');

                            } else if (check.status == 'NoCloseAllNoOrderNew') { // ปิดยังไม่ครบ และ ไม่มี order ใหม่

                            } else if (check.status == 'CloseallCompNoOrderNew') { // ปิดครบ และ ไม่มี order ใหม่

                                $("#md_noorder_closeall").modal('show');
                                $("#hn_noorder_closeall").val($.trim($("#box_search").val()));
                                $("#locationuid_login_noorder_closeall").val(locationuid_login);
                                $("#admin_login_uid_noorder_closeall").val(admin_login_uid);

                            } else if (check.status == 'CloseallCompFindNotSendOrderNew') {

                                $("#title_mdalert").html('พบ order ใหม่');
                                $("#head_mdalert").html('พบ order ใหม่');
                                $("#md_alert").modal('show');

                            } else if (check.status == 'GenNewQueue') { // ปิดครบ และ ไม่มี order ใหม่

                                $("#title_mdalert_newq").html('แจ้งเตือน');
                                $("#md_gen_newq").modal('show');

                            } else if (check.status == 'notFind') {

                                $("#title_mdalert").html('ไม่พบข้อมูล');
                                $("#head_mdalert").html('ไม่พบข้อมูล HN');
                                $("#md_alert").modal('show');

                            } else if (check.status == 'findOrderNotClose') {
                                $("#title_mdalert").html('คิวนี้มีอยู่ในระบบแล้ว');
                                $("#head_mdalert").html('คิวนี้มีอยู่ในระบบแล้ว');
                                $("#md_alert").modal('show');
                            }

                            $("#box_search_tmp").val($("#box_search").val());
                            $("#box_search").val('');
                        },
                        error: function(data) {
                            console.log(data);
                            $("#loding_log_scan").css('display', 'none');
                        }
                    });
                }, 10); //setTimeout
            }
        });



        call_list_order();

        function call_list_order() {
            $.ajax({
                url: "<?= base_url() ?>Xray/list_order",
                type: "post",
                async: false,
                data: {
                    user_roleuid: user_roleuid
                },
                success: function(data) {
                    //console.log(data);
                    $("#area_main_order").html(JSON.parse(data));
                }
            });
        }

        if (user_roleuid == '1') {
            $(".btn_order[value='order_all']").trigger('click').attr('active', 'active');
            list_all('order_all');
        } else {
            var value_order = $(".btn_order").first().val();
            call_list_order();
            btn_order_click();
            $(".btn_order[value='" + value_order + "']").trigger('click').attr('active', 'active').find('.num_pn').addClass('active');
        }

        btn_order_click();

        function btn_order_click() { //กดเรัยกรายการ แต่ละ order   
            $(".btn_order").click(function() {
                $(".btn_order").attr('active', '');
                $(this).attr('active', 'active');
                list_all($(this).val());
            });
        }


        function list_all(status) {
            $.ajax({
                url: "<?= base_url() ?>Xray/list_patient",
                type: "post",
                //async: false,
                data: {
                    status: status
                },
                success: function(data) {
                    console.log(JSON.parse(data));

                    var data = JSON.parse(data);

                    if (data != false) {

                        if (data['status'] == 'order_all') {
                            $("#tb_order_other").css('display', 'none');
                            $("#tb_order_all").css('display', 'block');
                            $("#body_list_patient").html(data['data']);
                        } else {
                            $("#tb_order_all").css('display', 'none');
                            $("#tb_order_other").css('display', 'block');
                            $("#body_list_patient_other").html(data['data']);
                        }

                    } else {

                        if ($(".btn_order[active='active']").val() == 'order_all') {
                            $("#tb_order_all").css('display', 'block');
                            $("#tb_order_other").css('display', 'none');
                            // $("#body_list_patient").html('');
                            // $("#body_list_patient_other").html('');
                            $("#tb_order_all").find('table').DataTable().row.add([
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                ''
                            ]).draw(false);
                        } else {
                            $("#tb_order_all").css('display', 'none');
                            $("#tb_order_other").css('display', 'block');
                            // $("#body_list_patient").html('');
                            // $("#body_list_patient_other").html('');
                            $("#tb_order_other").find('table').DataTable().row.add([
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                ''
                            ]).draw(false);
                        }

                    }

                    free_loop_socket = 'loop';
                    // ใช้เรียก function click ให้ตามมาหลังจาก เขียน ปุ่มลงหน้าเว็บเสร็จ
                    call_action_click();
                    // ใช้เรียก function click ให้ตามมาหลังจาก เขียน ปุ่มลงหน้าเว็บเสร็จ

                }
            });



        } //function

        // ใช้เรียก function click
        click_add_note();
        click_add_hold();
        // ใช้เรียก function click

        function call_action_click() {
            $(".btn_send").click(function() {
                var value = $(this).attr('uid');
                var status_type = $(this).attr('status_type');

                $.ajax({
                    url: "<?= base_url() ?>Xray/getorder_send",
                    type: "post",
                    async: false,
                    data: {
                        status: status,
                        patiendetailuid: value
                    },
                    success: function(data) {
                        console.log(JSON.parse(data));
                        $("#PatientDetail_uid").val(value);
                        $("#status_type").val(status_type);
                        $("#location_now_send").val(admin_login_uid);
                        $("#area_order_send").html(JSON.parse(data));
                        $("#md_send").modal('show');
                        click_md_send();
                    }
                });

            });
            $(".btn_print").click(function() {

                var patientdetail_uid = $(this).attr('patientdetail_uid');
                var patientxray_uid = $(this).attr('patientxray_uid');
                var xraycategory_uid = $(this).attr('status_type');

                var check_status_type = $(this).attr('status_type');
                var secure_code = $(this).attr('secure_code');

                if (check_status_type == 'order_all') {
                    $.ajax({
                        url: "<?= base_url() ?>Xray/select_list_print",
                        type: "post",
                        data: {
                            patientdetail_uid: patientdetail_uid,
                            check_status_type: check_status_type
                        },
                        success: function(data) {
                            console.log(JSON.parse(data));
                            var data = JSON.parse(data);
                            $("#patientdetail_uid_print").val(patientdetail_uid);
                            $("#patientxray_uid_print").val(patientxray_uid);
                            $("#xraycategory_uid_print").val(xraycategory_uid);
                            $("#check_status_type_print").val(check_status_type);
                            $("#location_now_print").val(admin_login_uid);
                            $("#secure_code_print").val(secure_code);

                            $("#tbody_print").html(data['data']);
                            $("#md_print").modal('show');
                            click_add_print();
                        }
                    });

                } else {

                    $.ajax({
                        url: "<?= base_url() ?>Xray/select_list_print",
                        type: "post",
                        data: {
                            patientxray_uid: patientxray_uid,
                            check_status_type: check_status_type
                        },
                        success: function(data) {
                            console.log(JSON.parse(data));
                            var data = JSON.parse(data);
                            $("#patientdetail_uid_print").val(patientdetail_uid);
                            $("#patientxray_uid_print").val(patientxray_uid);
                            $("#xraycategory_uid_print").val(xraycategory_uid);
                            $("#check_status_type_print").val(check_status_type);
                            $("#location_now_print").val(admin_login_uid);

                            $("#tbody_print").html(data['data']);
                            $("#md_print").modal('show');
                            click_add_print();
                        }
                    });

                }


            });

            $(".btn_note").click(function() {

                var patientdetail_uid = $(this).attr('patientdetail_uid');
                var patientxray_uid = $(this).attr('patientxray_uid');
                var check_status_type = $(this).attr('status_type');


                $.ajax({
                    url: "<?= base_url() ?>Xray/getnote",
                    type: "post",
                    data: {
                        patientdetail_uid: patientdetail_uid,
                        patientxray_uid: patientxray_uid,
                        check_status_type: check_status_type
                    },
                    success: function(data) {
                        console.log(JSON.parse(data));
                        var data = JSON.parse(data);

                        if (data['data'] != false) {
                            $("#textarea_note").val(data['data'][0]['notedetail']);
                        } else {
                            $("#textarea_note").val('');
                        }


                        $("#patientdetail_uid_note").val(patientdetail_uid);
                        $("#patientxray_uid_note").val(patientxray_uid);

                        $("#check_status_type_note").val(check_status_type);
                        $("#location_now_note").val(admin_login_uid);

                        $("#md_note").modal('show');

                    }
                });



            });

            $(".btn_counter").click(function() {

                var order_locatin_uid = $(this).attr('status_type');
                var patientdetail_uid = $(this).attr('patientdetail_uid');
                var patientxray_uid = $(this).attr('patientxray_uid');
                var status_type = $(this).attr('status_type');
                var queueno = $(this).attr('queueno');
                var nation = $(this).attr('nation');
                var xraycategoryuid = $(this).attr('xraycategoryuid_px');

                $.ajax({
                    url: "<?= base_url() ?>Xray/select_counter_call",
                    type: "post",
                    data: {
                        status_type: status_type
                    },
                    success: function(data) {
                        console.log(JSON.parse(data));
                        var data = JSON.parse(data);

                        $title = '';
                        if (status_type == '3') {
                            $title = 'U/S';
                        } else if (status_type == '1') {
                            $title = 'X - ray';
                        } else if (status_type == '2') {
                            $title = 'CT';
                        } else if (status_type == '4') {
                            $title = 'Bonedent';
                        } else if (status_type == '5') {
                            $title = 'Mammogram';
                        } else if (status_type == '6') {
                            $title = 'Special';
                        }

                        $("#tital_counter").html($title);

                        $("#list_call").html(data['data']);
                        $("#order_location_uid").val(order_locatin_uid);
                        $("#patientdetail_uid_counter").val(patientdetail_uid);
                        $("#patientxray_uid_counter").val(patientxray_uid);
                        $("#status_type_counter").val(status_type);
                        $("#location_now_counter").val(admin_login_uid);
                        $("#nation_counter").val(nation);
                        $("#queueno_counter").val(queueno);
                        $("#xraycategoryuid_counter").val(xraycategoryuid);

                        $("#md_counter").modal('show');
                        click_call_counter();

                    }
                });

            });



            $(".btn_hold").click(function() {

                var patientdetail_uid = $(this).attr('patientdetail_uid');
                var patientxray_uid = $(this).attr('patientxray_uid');
                var check_status_type = $(this).attr('status_type');
                $.ajax({
                    url: "<?= base_url() ?>Xray/select_list_hold",
                    type: "post",
                    data: {
                        patientdetail_uid: patientdetail_uid,
                        patientxray_uid: patientxray_uid,
                        check_status_type: check_status_type
                    },
                    success: function(data) {
                        console.log(JSON.parse(data));
                        var data = JSON.parse(data);

                        $("#hold_message-select option").remove();
                        $("#hold_message-select").append(data['data']);
                        $("#hold_message").val(data['holdtext']);
                        $("#patientdetail_uid_hold").val(patientdetail_uid);
                        $("#patientxray_uid_hold").val(patientxray_uid);
                        $("#check_status_type_hold").val(check_status_type);
                        $("#location_now_hold").val(admin_login_uid);
                        $("#md_hold").modal('show');


                    }
                });

            });
            $(".btn_complete").click(function() {

                var patientdetail_uid = $(this).attr('patientdetail_uid');
                var patientxray_uid = $(this).attr('patientxray_uid');
                var check_status_type = $(this).attr('status_type');
                var queueno = $(this).attr('queueno');
                var hn = $(this).attr('hn');

                $("#complete_show").html(queueno);
                $("#patientdetail_uid_complete").val(patientdetail_uid);
                $("#patientxray_uid_complete").val(patientxray_uid);

                $("#check_status_type_complete").val(check_status_type);
                $("#location_now_complete").val(admin_login_uid);
                $("#hn_complete").val(hn);

                $("#md_complete").modal('show');
            });
            $(".btn_closeq").click(function() {

                var patientdetail_uid = $(this).attr('patientdetail_uid');
                var patientxray_uid = $(this).attr('patientxray_uid');
                var check_status_type = $(this).attr('status_type');
                var queueno = $(this).attr('queueno');
                var hn = $(this).attr('hn');

                $("#closeq_show").html(queueno);
                $("#patientdetail_uid_closeq").val(patientdetail_uid);
                $("#patientxray_uid_closeq").val(patientxray_uid);

                $("#check_status_type_closeq").val(check_status_type);
                $("#location_now_closeq").val(admin_login_uid);
                $("#hn_closeq").val(hn);

                $("#md_closeq").modal('show');
            });


            // $("#btn_after_ourloc").click(function() {
            //     $("#md_alert_dduplicate").modal('show');
            // });


            $(".closeptall").click(function() {

                $("#hn_closeallpt").val($(this).attr('hn'));
                $("#admin_login_uid_closeallpt").val(admin_login_uid);
                $("#status_type_closeallpt").val($(this).attr('status_type'));

                $("#closeallpt").modal('show');
            });


        } // function btn action

        function closemdduplicate() {
            $("#closemdnewvn").click(function() {
                if ($(this).attr('data-scannew') != '') {
                    $("#md_alert_other").modal('show');
                }

            });
        }

        function call_fn_btnoutofloc() {
            $(".btn_print_outofloc").click(function() {
                var hn = $(this).attr('hn');
                var en = $(this).attr('en');
                var patientdetailuid = $(this).attr('patientdetail_uid');
                var check_status_type_print = $(this).attr('status_type');
                var location_now_print = admin_login_uid;
                var queueno = $(this).attr('queueno');
                var nation = $(this).attr('nation');
                var secure_code = $(this).attr('secure_code');

                $.ajax({
                    url: "<?= base_url() ?>Xray/add_print",
                    type: "post",
                    data: {
                        hn: hn,
                        en: en,
                        patientdetailuid: patientdetailuid,
                        check_status_type_print: check_status_type_print,
                        location_now_print: location_now_print,
                        sublocationuid_login: sublocationuid_login,
                        queueno: queueno,
                        nation: nation,
                        secure_code: secure_code
                    },
                    success: function(data) {
                        console.log(data);
                        list_all(check_status_type_print);
                    }
                });
            });
        }

        function click_md_send() {
            $(".select_order_send").click(function() {
                var data = $(this).find('img');

                if ($(this).attr('active') != '') {
                    $(this).attr('active', '');
                    check_send_img('unactive', data);
                } else {
                    $(this).attr('active', 'active');
                    check_send_img('active', data);
                }
            });
        }


        function check_send_img(check, data) {
            var length_path = data.attr('src').split('/').length;
            var img_fd = data.attr('src').split('/', length_path - 1);
            var result_img = img_fd.join('/');
            var name_last_fd = data.attr('src').split('/')[length_path - 1];

            var new_img = '';

            if (check == 'active') {
                if (name_last_fd == 'US_00.png') {
                    new_img = result_img + '/US_04.png';

                } else if (name_last_fd == 'X-ray_00.png') {
                    new_img = result_img + '/X-ray_04.png';

                } else if (name_last_fd == 'CT_00.png') {
                    new_img = result_img + '/CT_04.png';

                } else if (name_last_fd == 'Bone_00.png') {
                    new_img = result_img + '/Bone_04.png';

                } else if (name_last_fd == 'Mmg_00.png') {
                    new_img = result_img + '/Mmg_04.png';

                } else if (name_last_fd == 'Spc_00.png') {
                    new_img = result_img + '/Spc_04.png';
                }
            } else if (check == 'unactive') {

                if (name_last_fd == 'US_04.png') {
                    new_img = result_img + '/US_00.png';

                } else if (name_last_fd == 'X-ray_04.png') {
                    new_img = result_img + '/X-ray_00.png';

                } else if (name_last_fd == 'CT_04.png') {
                    new_img = result_img + '/CT_00.png';

                } else if (name_last_fd == 'Bone_04.png') {
                    new_img = result_img + '/Bone_00.png';

                } else if (name_last_fd == 'Mmg_04.png') {
                    new_img = result_img + '/Mmg_00.png';

                } else if (name_last_fd == 'Spc_04.png') {
                    new_img = result_img + '/Spc_00.png';
                }
            }
            data.attr('src', new_img);
        }

        function click_call_counter() {

            $(".call_counter").click(function() {
                var roomno = $(this).attr('roomno');
                var room_uid = $(this).attr('room_uid');
                var locationuid = $(this).attr('locationuid');
                var order_location_uid = $("#order_location_uid").val();
                var patientdetail_uid = $("#patientdetail_uid_counter").val();
                var patientxray_uid = $("#patientxray_uid_counter").val();
                var status_type = $("#status_type_counter").val();
                var location_now_counter = $("#location_now_counter").val();
                var nation_counter = $("#nation_counter").val();
                var queueno_counter = $("#queueno_counter").val();
                var xraycategoryuid_counter = $("#xraycategoryuid_counter").val();
                var locationuser = locationuser;

                $.ajax({
                    url: "<?= base_url() ?>Xray/add_callqueue",
                    type: "post",
                    data: {
                        roomno: roomno,
                        room_uid: room_uid,
                        locationuid: locationuid,
                        order_location_uid: order_location_uid,
                        patientdetail_uid: patientdetail_uid,
                        patientxray_uid: patientxray_uid,
                        status_type: status_type,
                        location_now_counter: location_now_counter,
                        nation_counter: nation_counter,
                        queueno_counter: queueno_counter,
                        xraycategoryuid_counter: xraycategoryuid_counter,
                        locationuid_login: locationuid_login,
                        sublocationuid_login: sublocationuid_login,
                        locationuser: locationuser
                    },
                    success: function(data) {
                        console.log(JSON.parse(data));
                        $("#md_counter").modal('hide');
                        list_all(status_type);
                    }
                });
            });
        }

        function click_add_hold() {

            $("#conf_hold").click(function() {

                var groupprocessuid = $("#hold_message-select option:selected").attr('groupprocessuid');
                var message_uid = $("#hold_message-select option:selected").attr('message_uid');
                var patientdetail_uid = $("#patientdetail_uid_hold").val();
                var patientxray_uid = $("#patientxray_uid_hold").val();
                var text_area_hold = $("#hold_message").val();
                var check_status_type = $("#check_status_type_hold").val();
                var location_now_hold = $("#location_now_hold").val();

                $.ajax({
                    url: "<?= base_url() ?>Xray/add_hold",
                    type: "post",
                    data: {
                        groupprocessuid: groupprocessuid,
                        patientdetail_uid: patientdetail_uid,
                        patientxray_uid: patientxray_uid,
                        text_area_hold: text_area_hold,
                        message_uid: message_uid,
                        check_status_type: check_status_type,
                        location_now_hold: location_now_hold,
                        locationuid_login: locationuid_login
                    },
                    success: function(data) {
                        console.log(data);
                        $("#md_hold").modal('hide');
                        list_all(check_status_type);

                    }
                });
            });
        }

        function click_add_note() {

            $("#conf_note").click(function() {

                var patientdetail_uid = $("#patientdetail_uid_note").val();
                var patientxray_uid = $("#patientxray_uid_note").val();
                var text_area_note = $("#textarea_note").val();

                var check_status_type_note = $("#check_status_type_note").val();
                var location_now_note = $("#location_now_note").val();

                if (check_status_type_note == 'order_all') {
                    $.ajax({
                        url: "<?= base_url() ?>Xray/add_note",
                        type: "post",
                        data: {
                            patientdetail_uid: patientdetail_uid,
                            text_area_note: text_area_note,
                            check_status_type_note: check_status_type_note,
                            location_now_note: location_now_note,
                            locationuid_login: locationuid_login
                        },
                        success: function(data) {
                            console.log(data);
                            $("#md_note").modal('hide');
                            list_all(check_status_type_note);

                        }
                    });
                } else {
                    $.ajax({
                        url: "<?= base_url() ?>Xray/add_note",
                        type: "post",
                        data: {
                            patientdetail_uid: patientdetail_uid,
                            patientxray_uid: patientxray_uid,
                            text_area_note: text_area_note,
                            check_status_type_note: check_status_type_note,
                            location_now_note: location_now_note
                        },
                        success: function(data) {
                            console.log(data);
                            $("#md_note").modal('hide');
                            list_all(check_status_type_note);

                        }
                    });
                }

            });
        }


        function click_add_print() {

            $(".click_print").click(function() {

                var patientdetail_uid_print = $("#patientdetail_uid_print").val();
                var patientxray_uid_print = $("#patientxray_uid_print").val();
                var xraycategory_uid_print = $("#xraycategory_uid_print").val();

                var check_status_type_print = $("#check_status_type_print").val();
                var location_now_print = $("#location_now_print").val();
                var secure_code_print = $("#secure_code_print").val();

                var hn = $(this).attr('hn');
                var en = $(this).attr('en');
                var queueno = $(this).attr('queueno');
                var nation = $(this).attr('nation');
                var patientdetailuid = $(this).attr('patientdetailuid');


                $.ajax({
                    url: "<?= base_url() ?>Xray/add_print",
                    type: "post",
                    data: {
                        patientdetail_uid_print: patientdetail_uid_print,
                        patientxray_uid_print: patientxray_uid_print,
                        xraycategory_uid_print: xraycategory_uid_print,
                        hn: hn,
                        en: en,
                        patientdetailuid: patientdetailuid,
                        check_status_type_print: check_status_type_print,
                        location_now_print: location_now_print,
                        locationuid_login: locationuid_login,
                        sublocationuid_login: sublocationuid_login,
                        queueno: queueno,
                        nation: nation,
                        secure_code: secure_code_print
                    },
                    success: function(data) {
                        console.log(data);
                        $("#md_print").modal('hide');
                        list_all(check_status_type_print);

                    }
                });
            });
        }



        $("#conf_send").click(function() {

            //alert($("#PatientDetail_uid").val() + ' ' + $("#status_type").val());
            //return false;

            var data = [];
            var data2 = [];
            $(".select_order_send").each(function() {
                if ($(this).attr('active') != '') {
                    data.push($(this).val());
                    data2.push($(this).attr('patientxrayuid'));
                }
            });

            // alert($("#PatientDetail_uid").val() + ' ' + $("#status_type").val() + ' ' + data + ' ' + data2);
            // return false;

            $.ajax({
                url: "<?= base_url() ?>Xray/update_ptxray",
                type: "post",
                async: false,
                data: {
                    patientdetail: $("#PatientDetail_uid").val(),
                    status_type: $("#status_type").val(),
                    xray_category_sel: data,
                    patientxrayuid: data2,
                    location_now_send: $("#location_now_send").val(),
                    locationuid_login: locationuid_login
                },
                success: function(data) {
                    console.log(JSON.parse(data));
                    $("#md_send").modal('hide');
                    list_all($("#status_type").val());

                    var value_order = $(".btn_order[active='active']").val();
                    call_list_order();
                    btn_order_click();
                    $(".btn_order[value='" + value_order + "']").trigger('click').attr('active', 'active').find('.num_pn').addClass('active');
                },
                error: function(data) {
                    console.log(data);
                }
            });


        });

        $("#conf_complete").click(function() {

            var patientdetail_uid_complete = $("#patientdetail_uid_complete").val();
            var patientxray_uid_complete = $("#patientxray_uid_complete").val();
            var check_status_type_complete = $("#check_status_type_complete").val();
            var location_now_complete = $("#location_now_complete").val();
            var hn_complete = $("#hn_complete").val();

            $.ajax({
                url: "<?= base_url() ?>Xray/update_complete",
                type: "post",
                async: false,
                data: {
                    patientdetail_uid: patientdetail_uid_complete,
                    patientxray_uid: patientxray_uid_complete,
                    check_status_type: check_status_type_complete,
                    location_now: location_now_complete,
                    locationuid_login: locationuid_login,
                    admin_login_uid: admin_login_uid,
                    hn: hn_complete
                },
                success: function(data) {
                    console.log(JSON.parse(data));
                    $("#md_complete").modal('hide');
                    list_all(check_status_type_complete);

                    var value_order = $(".btn_order[active='active']").val();
                    call_list_order();
                    btn_order_click();
                    $(".btn_order[value='" + value_order + "']").trigger('click').attr('active', 'active').find('.num_pn').addClass('active');
                }
            });

        });


        $("#conf_closeq").click(function() {

            var patientdetail_uid = $("#patientdetail_uid_closeq").val();
            var patientxray_uid = $("#patientxray_uid_closeq").val();
            var check_status_type = $("#check_status_type_closeq").val();
            var location_now = $("#location_now_closeq").val();
            var hn = $("#hn_closeq").val();
            $.ajax({
                url: "<?= base_url() ?>Xray/update_closeq",
                type: "post",
                async: false,
                data: {
                    patientdetail_uid: patientdetail_uid,
                    patientxray_uid: patientxray_uid,
                    check_status_type: check_status_type,
                    location_now: location_now,
                    locationuid_login: locationuid_login,
                    admin_login_uid: admin_login_uid,
                    admin_login: admin_login_uid,
                    hn: hn
                },
                success: function(data) {
                    console.log('checkkkk')
                    console.log(JSON.parse(data));
                    $("#md_closeq").modal('hide');
                    list_all(check_status_type);

                    var value_order = $(".btn_order[active='active']").val();
                    call_list_order();
                    btn_order_click();
                    $(".btn_order[value='" + value_order + "']").trigger('click').attr('active', 'active').find('.num_pn').addClass('active');
                }
            });

        });



        $("#conf_closeallpt").click(function() {

            var hn = $("#hn_closeallpt").val();
            var admin_login = $("#admin_login_uid_closeallpt").val();
            var status_type = $("#status_type_closeallpt").val();


            $.ajax({
                url: "<?= base_url() ?>Xray/closeallpt",
                type: "post",
                async: false,
                data: {
                    status_type: status_type,
                    hn: hn,
                    admin_login: admin_login,
                    locationuid_login: locationuid_login
                },
                success: function(data) {
                    console.log(JSON.parse(data));
                    $("#closeallpt").modal('hide');
                    list_all(status_type);

                    var value_order = $(".btn_order[active='active']").val();
                    call_list_order();
                    btn_order_click();
                    $(".btn_order[value='" + value_order + "']").trigger('click').attr('active', 'active').find('.num_pn').addClass('active');
                }
            });

        });



        $("#conf_createnoorder").click(function() {

            $("#loding_click_noorder").css('display', 'flex');

            setTimeout(() => {
                $.ajax({
                    url: "<?= base_url() ?>Xray/search_data",
                    type: "post",
                    async: false,
                    data: {
                        hn: $.trim($("#box_search_tmp").val()),
                        locationuid_login: locationuid_login,
                        admin_login_uid: admin_login_uid,
                        noorderrequest: 'confirm'
                    },
                    success: function(data) {
                        console.log(JSON.parse(data));
                        var check = JSON.parse(data);
                        $("#loding_click_noorder").css('display', 'none');
                        $("#md_noorder").modal('hide');

                        list_all($(".btn_order[active='active']").val());
                        $("#box_search").val('');
                        var value_order = $(".btn_order[active='active']").val();
                        call_list_order();
                        btn_order_click();
                        $(".btn_order[value='" + value_order + "']").trigger('click').attr('active', 'active').find('.num_pn').addClass('active');

                        // if (check.check_order_api_all == 'NotFindOrderInApi') { //ไม่มี order 
                        //     $("#alert_text").html('กรุณาเลือก Order เพื่อส่งคิวผู้ป่วยที่ Scan เข้ามาใหม่เข้าห้องตรวจ');
                        //     $("#md_alert_other").modal('show');
                        // }

                        if (check.resdetail.showprint != '') { //popup สั่งปริ้น

                            $("#tbody_printoutofloc").html(check.resdetail.showprint);
                            $("#md_printoutofloc").modal('show');

                            call_fn_btnoutofloc();

                        }

                    },
                    error: function(data) {
                        $("#md_noorder").modal('hide');
                        $("#loding_click_noorder").css('display', 'none');
                        console.log(data);
                    }
                });
            }, 50);

        });


        $("#conf_createnoorder_closeall").click(function() {


            $("#loding_click_closeall").css('display', 'flex');


            setTimeout(() => {
                $.ajax({
                    url: "<?= base_url() ?>Xray/search_data",
                    type: "post",
                    async: false,
                    data: {
                        hn: $.trim($("#box_search_tmp").val()),
                        locationuid_login: locationuid_login,
                        admin_login_uid: admin_login_uid,
                        completeorderagain: 'confirm'
                    },
                    success: function(data) {
                        console.log(data);
                        var check = JSON.parse(data);
                        $("#loding_click_closeall").css('display', 'none');

                        $("#md_noorder_closeall").modal('hide');

                        list_all($(".btn_order[active='active']").val());
                        $("#box_search_tmp").val('');
                        var value_order = $(".btn_order[active='active']").val();
                        call_list_order();
                        btn_order_click();
                        $(".btn_order[value='" + value_order + "']").trigger('click').attr('active', 'active').find('.num_pn').addClass('active');


                        if (check.status == 'GenNewQueue') { // ปิดครบ และ ไม่มี order ใหม่

                            $("#title_mdalert_newq").html('แจ้งเตือน');
                            $("#md_gen_newq").modal('show');

                        }

                        // if (check.check_order_api_all == 'NotFindOrderInApi') { //ไม่มี order 
                        //     $("#alert_text").html('กรุณาเลือก Order เพื่อส่งคิวผู้ป่วยที่ Scan เข้ามาใหม่เข้าห้องตรวจ');
                        //     $("#md_alert_other").modal('show');
                        // }

                    },
                    error: function(data) {
                        $("#md_noorder_closeall").modal('hide');
                        $("#loding_click_closeall").css('display', 'none');
                        console.log(data);
                    }
                });
            }, 50);

        });

        // $("#close_gen_newq").click(function() {
        //     $("#alert_text").html('กรุณาเลือก Order เพื่อส่งคิวผู้ป่วยที่ Scan เข้ามาใหม่เข้าห้องตรวจ');
        //     $("#md_alert_other").modal('show');
        // });


        $('.modal').modal({
            show: false,
            backdrop: 'static'
        });;

        $(window).on('resize', function() {
            redraw();
        });
        $(document).on('DOMSubtreeModified', "#body_list_patient", function() {
            redraw();
        });
        $(document).on('DOMSubtreeModified', "#body_list_patient_other", function() {
            redraw();
        });


    }); //document
</script>