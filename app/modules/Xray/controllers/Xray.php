<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Xray extends MY_Controller
{



	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('assets');
		$this->load->model('Xray_md');
		$this->load->model('Xray_select_md');
		// $this->load->module('StepProcessing/PersonType');
	}


	public function xraytemplate($page, $title, $script)
	{
		$this->load->module('Template_Module');

		$Template = array(
			'Module' => 'Xray',
			'Site_Title' => $title,
			'Content' => $page,
			'Script' => array(
				'Script' => $script,
			),
			'viewCSS' => array(
				'css' => 'Xray_style',
			),
		);
		$this->template_module->Template('Xray_tem', $Template);
	}

	public function Xray_main()
	{
		$page = array('Main' => 'Xray_vw');
		$title = 'Xray';
		$script = 'Xray_script';
		$this->xraytemplate($page, $title, $script);
	}


	public function getApiOrder()
	{

		// $result_api = [
		// ['XrayType' => '1'],
		// ['XrayType' => '2']
		// ];
		$check_order_all = 'NotFindOrderInApi';
		$result_api = [];
		$mock_api = [
			['XrayType' => '1'],
			['XrayType' => '2']
		];

		$result_patient_check = $this->Xray_md->search_patient(); // ค้นหา vw patient [จาก db เดิม]

		if ($result_patient_check != false) { //เจอใน view

			foreach ($result_patient_check as $valueGetVw) {
				$result_get_rrder = [];

				$ch_get = curl_init();
				curl_setopt($ch_get, CURLOPT_URL, APIAUTOPT . '/api/v1/groupxray?HN=' . $valueGetVw['hn'] . '&VN=' . $valueGetVw['vn'] . '&date=' . date('Ymd', strtotime('+543 years')) . '&Checked=');
				curl_setopt($ch_get, CURLOPT_RETURNTRANSFER, true);
				$output_order = json_decode(curl_exec($ch_get), true); //$mock_api
				curl_close($ch_get);

				// if ($valueGetVw['vn'] == '0002/01') {
				// $output_order = [
				// 	['XrayType' => '1X']
				// ];
				// } else {
				//$output_order = [];
				// }


				if (!empty($output_order)) {
					foreach ($output_order as $dataApi) {
						$categoryuid = '';
						if ($dataApi['XrayType'] == '1X') {
							$categoryuid = '1';   //xray
						} else if ($dataApi['XrayType'] == 'CT') {
							$categoryuid = '2';	//ct
						} else if ($dataApi['XrayType'] == '3U') {
							$categoryuid = '3';	//us
						} else if ($dataApi['XrayType'] == 'BD') {
							$categoryuid = '4';	//BD
						} else if ($dataApi['XrayType'] == '2S') {

							if ($dataApi['XrayPart'] == '') {
								$categoryuid = '5';	//mg
							} else if ($dataApi['XrayPart'] == 'A2' || $dataApi['XrayPart'] == 'E1' || $dataApi['XrayPart'] == 'F1' || $dataApi['XrayPart'] == 'G1' || $dataApi['XrayPart'] == 'H1' || $dataApi['XrayPart'] == 'I1' || $dataApi['XrayPart'] == 'J1' || $dataApi['XrayPart'] == 'K1' || $dataApi['XrayPart'] == 'L1' || $dataApi['XrayPart'] == 'M1' || $dataApi['XrayPart'] == 'N1' || $dataApi['XrayPart'] == 'O1' || $dataApi['XrayPart'] == 'Q1') {
								$categoryuid = '6';	//sx
							}
						}
						array_push($result_get_rrder, $categoryuid);
					}
					array_push($result_api, ['hn' => $valueGetVw['hn'], 'vn' => $valueGetVw['vn'], 'order' => $result_get_rrder]);
					$check_order_all = 'FindOrderInApi';
				}
			}

			return ['data' => $result_api, 'check_order_api_all' => $check_order_all];
		} else { // ไม่เจอในview
			return ['data' => $result_api, 'check_order_api_all' => $check_order_all];
		}
	}

	public function search_data()
	{

		$ordernew = ($this->input->post('ordernew') == null) ? '' : $this->input->post('ordernew');


		$get_result_api = $this->getApiOrder();

		$result_api = $get_result_api['data'];


		$noOrderRequest = ($this->input->post('noorderrequest') == null) ? '' : $this->input->post('noorderrequest');

		$completeOrderAgain = ($this->input->post('completeorderagain') == null) ? '' : $this->input->post('completeorderagain');


		$check_close_all = $this->Xray_md->checkCloseAll();

		$check_api_type = '0';


		if (($check_close_all[0]['count_hn'] == 0 && $check_close_all[1]['count_hn'] == 0)) { // ไม่เคยมีในระบบ xray ในวันนี้


			$result_patient = $this->Xray_md->search_patient(); // ค้นหา vw patient [จาก db เดิม]

			if ($result_patient == false) { //ไม่เจอใน view

				$create_new_queue = $this->Xray_md->createNewQueue(); // ค้นใน pt information
				if ($create_new_queue == false) {
					echo json_encode(['status' => 'notFind', 'check_order_api_all' => $get_result_api['check_order_api_all'], 'log' => false, 'massage' => 'ไม่มีข้อมูลคนไข้ในโรงพยาบาล', 'data' => $result_patient]);
				} else {
					$result_order = $this->Xray_md->list_xraycategory(); // ดึงรายการ xray category
					$result_patient_info = [];
					$addcolum_ptinfo = [];
					$ordernew = 'insert_pt_novw';
					foreach ($create_new_queue as $value) {
						$rundom_secure_code = $this->RandomString();
						$result_patient_info = $value;
						$result_patient_info['tokenno'] = '';
						$result_patient_info['secure_code'] = $rundom_secure_code;
						$result_patient_info['patient_location_id'] =  $value['location_id'];
					}

					array_push($addcolum_ptinfo, $result_patient_info);
					$resdetail = $this->genOrder($addcolum_ptinfo, $result_order, $ordernew, $check_api_type);
					echo json_encode(['status' => 'GenNewQueue', 'check_order_api_all' => $get_result_api['check_order_api_all'], 'resdetail' => $resdetail]);
				}
			} else { //เจอใน vw
				if (empty($result_api) && $noOrderRequest == '') {    // ไม่มี order auto เลย
					echo json_encode(['status' => 'noorder', 'check_order_api_all' => '']);
				} else if ($noOrderRequest != '') { // ยืนยัน insert กรณีไม่มี order auto เลย
					//echo json_encode('gen no order');

					$result_order = $this->Xray_md->list_xraycategory(); // ดึงรายการ xray category
					$result_detail = $this->genOrder($result_patient, $result_order, $ordernew, $check_api_type);

					echo json_encode(['status' => 'noorder', 'check_order_api_all' => $get_result_api['check_order_api_all'], 'resdetail' => $result_detail]);
				} else if (!empty($result_api)) {  //มี orderมา
					echo json_encode('find order in api');

					$result_order = $this->Xray_md->list_xraycategory(); // ดึงรายการ xray category

					$hnFindOrder = [];
					foreach ($result_api as $valueApi) {

						// มีapi แต่ ไม่มี order
						$result_patient = $this->Xray_md->search_patient_hnvn($valueApi['hn'], $valueApi['vn']); // ค้นหา patient  ที่มี order [จาก db เดิม]
						array_push($hnFindOrder, $result_patient[0]);
					}
					$check_api_type = '1';
					$this->genOrder($hnFindOrder, $result_order, $ordernew, $check_api_type);
				}
			}
		} else if (($check_close_all[0]['count_hn'] != 0 && $check_close_all[1]['count_hn'] != 0) && $check_close_all[0]['count_hn'] == $check_close_all[1]['count_hn']) { //ปิดครบหมดแล้ว
			//echo json_encode('close all complete');

			if ($completeOrderAgain == '') {
				$html_text = 'completeall'; // ไม่มี order ใหม่
				$saveHnOrderNew = [];
				if (!empty($result_api)) { // api มี order

					foreach ($result_api as $valueApi) {

						$result_patient = $this->Xray_md->search_patient_inorder($valueApi['hn'], $valueApi['vn']); // ค้นหา patient  ที่มี api [จาก db เดิม]
						if ($result_patient != false) { //ถ้ามี vn ใหม่เข้ามาจะไม่สน
							foreach ($valueApi['order'] as $valueApiOrder) {
								foreach ($result_patient  as $resultPt) {

									if (intval($valueApiOrder) == intval($resultPt['xraycategoryuid'])) { //ตรวจเสร็จแล้วแต่มี order ใหม่มา แสดง vn ที่มี order ใหม่เท่านั้น
										$html_text = 'insert new';

										if ($resultPt['used'] == 0) { // จะต้องไม่เคย send เลยสักครั้ง ไม่ว่าจะ scanฒากี่ครั้ง
											if (empty($saveHnOrderNew)) { // กัน push hn vn ซ้ำ
												array_push($saveHnOrderNew, ['hn' => $valueApi['hn'], 'vn' => $valueApi['vn']]);
											} else {

												$checkCateDuplicate = '';
												foreach ($saveHnOrderNew as $valueSaveHnOrderNew) {
													if ($valueSaveHnOrderNew['vn'] == $valueApi['vn']) {
														$checkCateDuplicate = 'duplicate';
													}
												}

												if ($checkCateDuplicate == '') { //เช็คไม่ให้ insert ซ้ำ
													array_push($saveHnOrderNew, ['hn' => $valueApi['hn'], 'vn' => $valueApi['vn']]);
												}
											}
										}
									}
								}
							}
						} else { //ถ้ามี vn ใหม่เข้ามา พร้อม order ใหม่
							$result_patient_vw = $this->Xray_md->search_patient_hnvn($valueApi['hn'], $valueApi['vn']);

							foreach ($result_patient_vw  as $resultPt) {

								if (empty($saveHnOrderNew)) { // กัน push hn vn ซ้ำ
									array_push($saveHnOrderNew, ['hn' => $valueApi['hn'], 'vn' => $valueApi['vn']]);
								} else {
									$checkCateDuplicate = '';
									foreach ($saveHnOrderNew as $valueSaveHnOrderNew) {
										if ($valueSaveHnOrderNew['vn'] == $valueApi['vn']) {
											$checkCateDuplicate = 'duplicate';
										}
									}

									if ($checkCateDuplicate == '') { //เช็คไม่ให้ insert ซ้ำ
										array_push($saveHnOrderNew, ['hn' => $valueApi['hn'], 'vn' => $valueApi['vn']]);
									}
								}
							}
						}
					}



					if (!empty($saveHnOrderNew)) { // เฉพาะ ปิดไปหมดแล้ว แต่มี order มาใหม่ Insert vn ที่มี order ใหม่ เท่านั้น
						$hnFindOrder = [];
						$ordernew = 'insertcompleteagain';
						$result_order = $this->Xray_md->list_xraycategory(); // ดึงรายการ xray category
						foreach ($saveHnOrderNew as $resultsaveHnOrderNew) {

							if ($resultsaveHnOrderNew['vn'] != '-') {
								$result_patient = $this->Xray_md->search_patient_hnvn($resultsaveHnOrderNew['hn'], $resultsaveHnOrderNew['vn']); // ค้นหา patient  ที่มี order [จาก db เดิม]
								array_push($hnFindOrder, $result_patient[0]);
							} else {
								$create_new_queue = $this->Xray_md->createNewQueue($resultsaveHnOrderNew['hn']); // ค้นใน pt information
								if ($create_new_queue != false) {
									$result_patient_info = [];
									$ordernew = 'insert_pt_novw';
									foreach ($create_new_queue as $value) {
										$rundom_secure_code = $this->RandomString();
										$result_patient_info = $value;
										$result_patient_info['tokenno'] = '';
										$result_patient_info['secure_code'] = $rundom_secure_code;
										$result_patient_info['patient_location_id'] =  $value['location_id'];
									}

									array_push($hnFindOrder, $result_patient_info);
								}
							}
						}
						$check_api_type = '1'; //เพื่อ ส่ง api 1 ไป  [ 1 = ส่ง  0 = ไม่ส่ง ]
						$this->genOrder($hnFindOrder, $result_order, $ordernew, $check_api_type);
					} else {
						///
					}
				} else { //api ไม่มี order

				}


				$saveHnOrderNew = (empty($saveHnOrderNew)) ? 'CloseallCompNoOrderNew' : 'CloseallCompFindNotSendOrderNew';
				echo json_encode(['status' => $saveHnOrderNew, 'check_order_api_all' => $get_result_api['check_order_api_all']]); //ส่งออกเป็น [] = ไม่มี orderใหม่  ถ้ามีค่า แสดงว่า มีorder ใหม่
			} else { // กดยืนยัน สิงคิวซ้ำ มาแล้ว

				$result_patient = $this->Xray_md->search_patient(); // ค้นหา vw patient [จาก db เดิม]
				if ($result_patient == false) { //ไม่เจอใน view

					$create_new_queue = $this->Xray_md->createNewQueue(); // ค้นใน pt information
					if ($create_new_queue == false) {
						echo json_encode(['log' => false, 'massage' => 'ไม่มีข้อมูลคนไข้ในโรงพยาบาล', 'data' => $result_patient]);
					} else {
						$result_order = $this->Xray_md->list_xraycategory(); // ดึงรายการ xray category
						$result_patient_info = [];
						$addcolum_ptinfo = [];
						$ordernew = 'insert_pt_novw';
						foreach ($create_new_queue as $value) {
							$rundom_secure_code = $this->RandomString();
							$result_patient_info = $value;
							$result_patient_info['tokenno'] = '';
							$result_patient_info['secure_code'] = $rundom_secure_code;
							$result_patient_info['patient_location_id'] =  $value['location_id'];
						}

						array_push($addcolum_ptinfo, $result_patient_info);
						$resdetail = $this->genOrder($addcolum_ptinfo, $result_order, $ordernew, $check_api_type);
						echo json_encode(['status' => 'GenNewQueue', 'check_order_api_all' => $get_result_api['check_order_api_all'], 'resdetail' => $resdetail]);
					}
				} else {						//เจอใน view
					$ordernew = 'insertcompleteagain';
					$result_order = $this->Xray_md->list_xraycategory(); // ดึงรายการ xray category
					$resdetail = $this->genOrder($result_patient, $result_order, $ordernew, $check_api_type);

					echo json_encode(['status' => 'GenNewQueue', 'check_order_api_all' => $get_result_api['check_order_api_all'], 'resdetail' => $resdetail]);
				}
			}
		} else if (($check_close_all[0]['count_hn'] != 0 || $check_close_all[1]['count_hn'] != 0) && $check_close_all[0]['count_hn'] != $check_close_all[1]['count_hn']) { //ปิดไม่หมด
			//echo json_encode('ยังปิดไม่หมด');

			$html_text = 'findOrderNotClose';

			echo json_encode(['status' => $html_text]);
		}
	}

	public function genOrder($result_patient, $result_order, $ordernew, $check_api_type)
	{
		$insert_PatientDetail = $this->Xray_md->Ins_PatientDetail($result_patient, $result_order, $ordernew, $check_api_type); // insert patientdetail , patientxray , processcontrol

		$head_duplicate = '';

		$html_dup = '';
		if (count($insert_PatientDetail['duplication']) > 0) {

			foreach ($insert_PatientDetail['duplication'] as $key => $value) {
				$head_duplicate = $value['hn'];
				$html_dup .= "<tr>";
				$html_dup .= "<td>" . $value['vn'] . "</td>";
				$html_dup .= "</tr>";
			};
		}

		$html_newhn = '';
		if (count($insert_PatientDetail['new_hn']) > 0) {

			foreach ($insert_PatientDetail['new_hn'] as $key => $value) {
				$head_duplicate = $value['hn'];
				$html_newhn .= "<tr>";
				$html_newhn .= "<td>" . $value['vn'] . "</td>";
				$html_newhn .= "</tr>";
			};
		}

		$count_check = '';

		if (count($insert_PatientDetail['all_hn']) > 0) {
			foreach ($insert_PatientDetail['all_hn'] as $key => $value) {

				$result_find = $this->Xray_md->FindOrderHn($value['hn'], $value['vn']);

				if (count($result_find) > 0) {
					$count_check = 'send_empty';
				}
			};
		}

		$html_showprint = '';
		if (count($insert_PatientDetail['patientdetail_new']) > 0) {
			$result_new_pid = [];
			$result_old_pid = [];

			foreach ($insert_PatientDetail['patientdetail_new'] as $key => $value) {
				array_push($result_new_pid, $this->select_patient_new($value['patientdetailuid']));
			}

			foreach ($insert_PatientDetail['duplication'] as $keydup => $valuedup) {
				array_push($result_old_pid, $valuedup['opd_location']);
			}

			//echo json_encode($result_new_pid); die();

			if (count($result_new_pid) > 0) {

				$check_old_location = '';
				$check_location_get = '';

				foreach ($result_old_pid as $keyold => $valueold) {	// เช็คว่า location ที่เคยเพิ่มเข้ามา มี location ใช้ระบบคิวหรือไม่
					if (trim($valueold) == 'C2' || trim($valueold) == 'CWB' || trim($valueold) == 'CDC' || trim($valueold) == 'C9' || trim($valueold) == 'GI' || trim($valueold) == 'C11' || trim($valueold) == 'C6' || trim($valueold) == 'C7' || trim($valueold) == 'C8' || trim($valueold) == 'EYE' || trim($valueold) == 'CHE' || trim($valueold) == 'HPC' || trim($valueold) == 'IMS' || trim($valueold) == 'C10' || trim($valueold) == 'PT' || trim($valueold) == 'BJC' || trim($valueold) == 'NDT' || trim($valueold) == 'COB') {
						$check_old_location = 'use_queue_system';
					}
				};


				if ($check_old_location == '') { // location ก่อนหน้าไม่มีใช้ระบบคิว หรือ ไม่เคยเพิ่มข้อมูลคนไข้เข้ามาในวันนี้
					foreach ($result_new_pid as $key => $value) {
						if (trim($value[0]['opd_location']) == 'C2' || trim($value[0]['opd_location']) == 'CWB' || trim($value[0]['opd_location']) == 'CDC' || trim($value[0]['opd_location']) == 'C9' || trim($value[0]['opd_location']) == 'GI' || trim($value[0]['opd_location']) == 'C11' || trim($value[0]['opd_location']) == 'C6' || trim($value[0]['opd_location']) == 'C7' || trim($value[0]['opd_location']) == 'C8' || trim($value[0]['opd_location']) == 'EYE' || trim($value[0]['opd_location']) == 'CHE' || trim($value[0]['opd_location']) == 'HPC' || trim($value[0]['opd_location']) == 'IMS' || trim($value[0]['opd_location']) == 'C10' || trim($value[0]['opd_location']) == 'PT' || trim($value[0]['opd_location']) == 'BJC' || trim($value[0]['opd_location']) == 'NDT' || trim($value[0]['opd_location']) == 'COB') {
							$check_location_get = 'noprint';
						}
					};

					if ($check_location_get == '') {		//ไม่มี vn ไหน ใช้ location ระบบคิว
						foreach ($result_new_pid as $key2 => $value2) {
							$html_showprint .= "<tr>";
							$html_showprint .= "<td>" . $value2[0]['queueno'] . "</td>";
							$html_showprint .= "<td>" . $value2[0]['hn'] . "</td>";
							$html_showprint .= "<td>" . $value2[0]['en'] . "</td>";
							$html_showprint .= "<td>" . $value2[0]['opd_location'] . "</td>";
							$html_showprint .= "<td><button class='none btn_print_outofloc' style='outline: none;' hn='" . $value2[0]['hn'] . "' en='" . $value2[0]['en'] . "' patientdetail_uid='" . $value2[0]['patientdetail_uid'] . "' queueno='" . $value2[0]['queueno'] . "' nation='" . $value2[0]['nation'] . "' secure_code='" . $value2[0]['secure_code'] . "' status_type='order_all'><i class='fa fa-print'></i></button></i></td>";
							$html_showprint .= "</tr>";
						};
					}
				}
			}
		} // if patientdetail_new

		return ['log' => true, 'duplication' => $html_dup, 'newhn' => $html_newhn, 'showprint' => $html_showprint, 'showheadduplicate' => $head_duplicate, 'sendempty' => $count_check];
	}

	public function list_patient()
	{

		$status = $this->input->post('status');

		$data = '';

		if ($status == 'order_all') {								//order all เท่านั่น

			$data = $this->Xray_select_md->type_order_all();

			if ($data == false) {
				echo json_encode($data);
			} else {
				$this->tb_order_all($data, $status);
			}
		} else {													//order อื่นๆที่ไม่ไช่ all 
			$data = $this->Xray_select_md->type_order_other();
			
			if ($data == false) {
				echo json_encode($data);
			} else {
				$this->tb_order_other($data, $status);
			}
		}

		//echo json_encode($data); die();


	}

	function duration($begin, $end)
	{
		$remain = intval(strtotime($end) - strtotime($begin));
		$minute = floor($remain / 60);

		return $minute;
	}

	public function tb_order_all($data, $status_type)
	{

		$html = '';
		$class_color = "btn-thg-blue";
		$class_yellow = "btn-thg-yellow";
		$num_row = 0;

		foreach ($data as $key => $value) {

			$print = '';
			$note = '';
			$call_order = ' - ';
			$sex = '-';
			$waitingtime = '';

			if ($value['printcwhen'] != null) {
				$print = $class_yellow;
			} else {
				$print = '';
			}

			if ($value['notedetailcwhen'] != null) {
				$note = $class_color;
			} else {
				$note = '';
			}

			if ($value['callordername'] != null) {

				$call_order = $value['callordername'];
			}

			if ($value['sex'] != null) {
				$sex = $value['sex'];
			}

			$waitingtime = $this->duration($value['cwhen'], date('Y-m-d H:i:s'));

			$get_img_order = $this->Xray_md->FindOrderImg($value['patientdetail_uid']);

			$img_show = '';

			if ($get_img_order != false) {
				$img_show = $this->loopcheck_img($get_img_order);
			}



			$html .= "<tr style='height:50px;'>";
			$html .= "<td>" . ($num_row += 1) . "</td>";
			$html .= "<td>";
			$html .= "<button class='none whbutton icon_td btn_send' style='outline: none;' status_type='" . $status_type . "' uid='" . $value['patientdetail_uid'] . "'><i class='fa fa-paper-plane' aria-hidden='true'></i> </button>";
			$html .= "</td>";
			$html .= "<td>" . $value['queueno'] . "</td>";
			$html .= "<td>" . $value['prename'] . $value['forename'] . ' ' . $value['surname'] . "</td>";
			$html .= "<td>" . $value['hn'] . "</td>";
			$html .= "<td>" . $value['en'] . "</td>";
			$html .= "<td> " . $waitingtime . "</td>";
			$html .= "<td>";
			$html .= "<div>";
			$html .= $img_show;
			$html .= "</div>";
			$html .= "</td>";
			$html .= "<td>" . $call_order . "</td>";
			$html .= "<td>";
			$html .= "<button class='none whbutton icon_td btn_print " . $print . "' style='outline: none;' status_type='" . $status_type . "' patientdetail_uid='" . $value['patientdetail_uid'] . "' secure_code='" . $value['secure_code'] . "'><i class='fa fa-print'></i></button>";
			$html .= "</td>";
			$html .= "<td>";
			$html .= "<button class='none whbutton icon_td btn_note " . $note . "' style='outline: none;' status_type='" . $status_type . "' patientdetail_uid='" . $value['patientdetail_uid'] . "'><i class='fas fa-edit'></i></button>";
			$html .= "</td>";
			$html .= "<td>";
			$html .= "<button class='none whbutton icon_x btn_closeq' style='outline: none;' status_type='" . $status_type . "' patientdetail_uid='" . $value['patientdetail_uid'] . "' queueno='" . $value['queueno'] . "' hn='" . $value['hn'] . "'><span aria-hidden='true'>&times;</span></button>";
			$html .= "</td>";
			$html .= "<td>";
			$html .= "<button class='closeptall' hn='" . $value['hn'] . "' status_type='" . $status_type . "'><i class='fas fa-ellipsis-h'></i></button>";
			$html .= "</td>";
			$html .= "</tr>";
		}

		$result_final = ['status' => 'order_all', 'data' => $html, 'log' => $data];

		echo json_encode($result_final);
	}

	public function tb_order_other($data, $status_type)
	{
		$html = '';

		$class_color = "btn-thg-blue";
		$class_yellow = "btn-thg-yellow";

		$num_row = 0;
		foreach ($data as $key => $value) {

			$print = '';
			$note = '';
			$hold = '';
			$call = '';
			$sex = '-';
			$waitingtime = '';
			$calllastorder = ' - ';

			if ($value['printcwhen'] != null) {
				$print = $class_yellow;
			} else {
				$print = '';
			}

			if ($value['notedetailcwhenorderall'] != null) {
				$note = $class_color;
			} else {
				$note = '';
			}

			if ($value['messagedetailcwhen'] != null) {
				$hold = $class_yellow;
			} else {
				$hold = '';
			}

			if ($value['callqueuecwhen'] != null) {
				$call = $class_yellow;
			} else {
				$call = '';
			}

			if ($value['sex'] != null) {
				$sex = $value['sex'];
			}

			if ($value['lastcallorder'] != null) {
				$calllastorder = $value['lastcallorder'];
			}

			$waitingtime = $this->duration($value['patientxraysendcwhen'], date('Y-m-d H:i:s'));


			$get_img_order = $this->Xray_md->FindOrderImg($value['patientdetail_uid']);

			$img_show = '';

			if ($get_img_order != false) {
				$img_show = $this->loopcheck_img($get_img_order);
			}



			if ($value['worklistuid9'] == null && $value['checklastworklist'] == null) {

				$html .= "<tr style='height:50px;'>";
				$html .= "<td>" . ($num_row += 1) . "</td>";
				$html .= "<td>" . $value['queueno'] . "</td>";
				$html .= "<td>";
				$html .= "<button class='none whbutton icon_td btn_counter " . $call . "' style='outline: none;' status_type='" . $status_type . "' patientdetail_uid='" . $value['patientdetail_uid'] . "' patientxray_uid='" . $value['patientxray_uid'] . "' queueno='" . $value['queueno'] . "' nation='" . $value['nation'] . "' xraycategoryuid_px='" . $value['xraycategoryuid_px'] . "'><i class='fas fa-volume-up'></i> </button>";
				$html .= "</td>";
				$html .= "<td>" . $value['prename'] . $value['forename'] . ' ' . $value['surname'] . "</td>";
				$html .= "<td>" . $value['hn'] . "</td>";
				$html .= "<td>" . $value['en'] . "</td>";
				$html .= "<td>" . $waitingtime . "</td>";
				$html .= "<td>";
				$html .= "<div>";
				$html .= $img_show;
				$html .= "</div>";
				$html .= "</td>";
				$html .= "<td> " . $calllastorder . " </td>";
				//$html .= "<td>";
				//$html .= "<button class='none whbutton icon_td btn_print " . $print . "' style='outline: none;' status_type='" . $status_type . "' patientdetail_uid='" . $value['patientdetail_uid'] . "' patientxray_uid='" . $value['patientxray_uid'] . "'><i class='fa fa-print'></i></button>";
				//$html .= "</td>";
				$html .= "<td>";
				$html .= "<button class='none whbutton icon_td btn_note " . $note . "' style='outline: none;' status_type='" . $status_type . "' patientdetail_uid='" . $value['patientdetail_uid'] . "' patientxray_uid='" . $value['patientxray_uid'] . "'><i class='fa fa-edit'></i></button>";
				$html .= "</td>";
				$html .= "<td>";
				$html .= "<button class='none whbutton icon_td btn_hold " . $hold . "' style='outline: none;' status_type='" . $status_type . "' patientdetail_uid='" . $value['patientdetail_uid'] . "' patientxray_uid='" . $value['patientxray_uid'] . "'><i class='fa fa-ban' ></i></button>";
				$html .= "</td>";
				$html .= "<td>";
				$html .= "<button class='none whbutton icon_td btn_complete' style='outline: none;' status_type='" . $status_type . "' patientdetail_uid='" . $value['patientdetail_uid'] . "' patientxray_uid='" . $value['patientxray_uid'] . "' queueno='" . $value['queueno'] . "' hn='" . $value['hn'] . "'><i class='far fa-check-square'></i></button>";
				$html .= "</td>";
				$html .= "<td>";
				$html .= "<button class='none whbutton icon_x active btn_closeq' style='outline: none;' status_type='" . $status_type . "' patientdetail_uid='" . $value['patientdetail_uid'] . "' patientxray_uid='" . $value['patientxray_uid'] . "' queueno='" . $value['queueno'] . "' hn='" . $value['hn'] . "'><span aria-hidden='true'>&times;</span></button>";
				$html .= "</td>";
				$html .= "</tr>";
			}
		}

		$result_final = ['status' => 'other', 'data' => $html, 'log' => $data];

		echo json_encode($result_final);
	}



	public function list_order()
	{

		$result = $this->Xray_md->list_xraycategory();

		$result2 = $this->Xray_md->list_xraycategory_all();

		$html = '';

		$user_roleuid = $this->input->post('user_roleuid');

		if ($user_roleuid == '1') {
			foreach ($result2 as $key => $value2) {
				$html .= "<div class='center card_ctn'>";
				$html .= "<button class='center card text-center  none btn_pr btn_order' type='button' value='order_all' active=''>";
				$html .= "<div class='center card_h'> All </div>";
				$html .= "<div class='center card_bd'>";
				$html .= single_img('img/icon/iconall_00.png');
				$html .= "</div>";
				$html .= "<div class='text-muted center card_f'>";
				$html .= "<span class='num_pn center'> " . $value2['countcateall'] . " </span>";
				$html .= "</div>";
				$html .= "</button>";
				$html .= "</div>";
			}
		}
		foreach ($result as $key => $value) {
			$html .= "<div class='center card_ctn'>";
			$html .= "<button class='center card text-center  none btn_pr btn_order' type='button' value='" . $value['uid'] . "' active=''>";
			$html .= "<div class='center card_h'> " . $value['name'] . " </div>";
			$html .= "<div class='center card_bd'>";
			$html .= single_img('img/icon/' . $value['icon']);
			$html .= "</div>";
			$html .= "<div class='text-muted center card_f'>";
			$html .= "<span class='num_pn center'> " . $value['patientxraycount'] . " </span>";
			$html .= "</div>";
			$html .= "</button>";
			$html .= "</div>";
		}
		echo json_encode($html);
	}


	public function getorder_send()
	{

		$result = $this->Xray_select_md->list_xraycategory_send();

		$html = '';

		$css = '';

		//echo json_encode($result); die();

		foreach ($result as $key => $value) {
			if ($value['worklistuid'] != '3' && $value['worklistuid'] != '6') {
				if ($value['senddttm'] != null) {
					$css = 'opacity:0.2;pointer-events: none;';
				} else {
					$css = '';
				}
			}
			$html .= "<div class='col-4 center' style='padding: .6rem;'>";
			$html .= "<button class='center card text-center  none select_order_send' active='' type='button' value='" . $value['xraycategoryuid'] . "' patientxrayuid='" . $value['patientxrayuid'] . "' style='" . $css . "'>";
			$html .= single_img('img/icon/' . $value['icon']);
			$html .= "</button>";
			$html .= "</div>";
		}
		echo json_encode($html);
	}


	public function update_ptxray()
	{

		$result = $this->Xray_md->UpdatePtXray();

		echo json_encode($result);
	}


	public function select_counter_call()
	{

		$result = $this->Xray_select_md->SelectCounterCall();

		$html = "";

		if ($result != false) {
			foreach ($result as $key => $value) {
				$html .= "<button class='btn_inct call_counter' roomno='" . $value['roomno'] . "' room_uid='" . $value['uid'] . "' locationuid='" . $value['locationuid'] . "'>" . $value['name'] . "</button>";
			}
		}


		echo json_encode(['log' => $result, 'data' => $html]);
	}

	public function add_callqueue()
	{

		$result = $this->Xray_md->AddCallQueue();

		echo json_encode($result);
	}

	public function select_list_hold()
	{
		$result = $this->Xray_select_md->SelectListHold();
		$gethold = $this->Xray_md->GetHold();

		$selected = '';
		$messageuid = '';
		$holdtext = '';

		$html = '';
		if ($result != false) {
			foreach ($result as $key => $value) {
				$messageuid = $value['uid'];

				if ($gethold != false) {
					foreach ($gethold as $key => $value2) {
						if ($messageuid == $value2['messageuid']) {
							$selected = 'selected';
							$holdtext = $value2['remake'];
						}
					}
				}


				$html .= " <option message_code='" . $value['code'] . "' groupprocessuid='" . $value['groupprocessuid'] . "' message_uid='" . $value['uid'] . "'" . $selected . ">" . $value['description'] . "</option>";
			}
		}

		echo json_encode(['log' => $result, 'data' => $html, 'holdtext' => $holdtext]);
	}

	public function add_hold()
	{
		$result = $this->Xray_md->AddHold();

		echo json_encode($result);
	}

	public function add_note()
	{

		$result = $this->Xray_md->AddNote();

		echo json_encode($result);
	}

	public function select_list_print()
	{
		$check_status_type = $this->input->post('check_status_type');

		if ($check_status_type == 'order_all') {
			$result = $this->Xray_select_md->SelectListPrintOrderAll();
		} else {
			$result = $this->Xray_select_md->SelectListPrint();
		}

		$html = '';
		foreach ($result as $key => $value) {

			$html .= "<tr>";
			$html .= "<td>" . $value['queueno'] . "</td>";
			$html .= "<td>" . $value['hn'] . "</td>";
			$html .= "<td>" . $value['en'] . "</td>";
			$html .= "<td>" . $value['opd_location'] . "</td>";
			$html .= "<td><button class='none click_print' style='outline: none;' hn='" . $value['hn'] . "' en='" . $value['en'] . "' patientdetailuid='" . $value['patientdetail_uid'] . "' queueno='" . $value['queueno'] . "' nation='" . $value['nation'] . "'><i class='fa fa-print'></i></button></i></td>";
			$html .= "</tr>";
		}

		echo json_encode(['log' => $result, 'data' => $html]);
	}

	public function add_print()
	{
		$result = $this->Xray_md->AddPrint();

		echo json_encode($result);
	}

	public function update_complete()
	{
		$result = $this->Xray_md->UpdateComplete();

		$patientdetail_uid = $this->input->post('patientdetail_uid');
		$locationuid_login = $this->input->post('locationuid_login');
		$admin_login_uid = $this->input->post('admin_login_uid');
		$hn = $this->input->post('hn');

		$close_patient_all = $this->close_patient_all($patientdetail_uid, $locationuid_login, $admin_login_uid, $hn);

		echo json_encode($close_patient_all);
	}

	public function update_closeq()
	{
		$result = $this->Xray_md->UpdateCloseq();

		$patientdetail_uid = $this->input->post('patientdetail_uid');
		$locationuid_login = $this->input->post('locationuid_login');
		$admin_login_uid = $this->input->post('admin_login_uid');
		$hn = $this->input->post('hn');

		$close_patient_all = $this->close_patient_all($patientdetail_uid, $locationuid_login, $admin_login_uid, $hn);

		echo json_encode($close_patient_all);
	}

	function close_patient_all($patientdetail_uid, $locationuid_login, $admin_login_uid, $hn = null)
	{

		$checkcloseall = $this->Xray_md->CheckCloseAllHn($patientdetail_uid);
		$patientxrayinfo = $this->Xray_md->SelectPatientXrayInfo($patientdetail_uid);

		$count_finish = 0;

		if ($checkcloseall != false) {
			foreach ($checkcloseall as $key => $value) {
				if ($value['xraydischarge'] != null || $value['worklistuidthreeorsix'] != null) { // เคยปิด หรือ ตรวจเสร็จ
					$count_finish++;
				}
			}
			//return json_encode(count($checkcloseall)  . ' = ' . $count_finish);


			if (count($checkcloseall) == $count_finish) { //ทุกorder ตรวจเสร็จ หรือ ปิดคิวหมด

				$close_all_hn = $this->Xray_md->CloseAllHn($patientdetail_uid, $locationuid_login, $admin_login_uid, $patientxrayinfo);

				$result_gethn = $this->Xray_md->CloseAllPt($admin_login_uid, $hn);

				if (count($result_gethn) > 0) {
					$ins_closeall = $this->Xray_md->InsCloseAll($result_gethn);
				}
			}

			return json_encode(count($checkcloseall)  . ' = ' . $count_finish . ' ++ ' . $hn);
		} else {
			return json_encode('0 = ' . $count_finish . ' ++ ' . $hn);
		}
	}

	public function getnote()
	{
		$result = $this->Xray_md->GetNote();

		echo json_encode(['status' => true, 'data' => $result]);
	}

	public function cleatsession()
	{
		$this->session->unset_userdata('userlogin');

		return true;
	}

	public function loopcheck_img($get_img_order)
	{
		$img_show = '';

		foreach ($get_img_order as $key => $value) {
			if ($value['worklistuidlast'] == '1') {
				$img_show .=  single_img('img/icon/' . $value['icon'], array('class' => 'iconsize_tr'));
			} else if ($value['worklistuidlast'] == '2') {
				$img_show .=  single_img('img/icon/' . $value['iconsend'], array('class' => 'iconsize_tr'));
			} else if ($value['worklistuidlast'] == '3') {
				$img_show .=  single_img('img/icon/' . $value['iconclose'], array('class' => 'iconsize_tr'));
			} else if ($value['worklistuidlast'] == '6') {
				$img_show .=  single_img('img/icon/' . $value['iconcomplete'], array('class' => 'iconsize_tr'));
			} else if ($value['worklistuidlast'] == '9') {
				$img_show .=  single_img('img/icon/' . $value['iconclose'], array('class' => 'iconsize_tr'));
			} else if ($value['worklistuidlast'] == '10') {
				$img_show .=  single_img('img/icon/' . $value['iconresend'], array('class' => 'iconsize_tr'));
			}
		}

		return $img_show;
	}

	public function select_patient_new($data)
	{
		$result = $this->Xray_md->SelectPatientNew($data);

		return $result;
	}

	public function closeallpt()
	{
		$result_gethn = $this->Xray_md->CloseAllPt();


		if (count($result_gethn) > 0) {
			$ins_closeall = $this->Xray_md->InsCloseAll($result_gethn);
		}

		echo json_encode(['status' => true, 'data' => $result_gethn]);
	}

	public function RandomString($length = 8)
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
}
