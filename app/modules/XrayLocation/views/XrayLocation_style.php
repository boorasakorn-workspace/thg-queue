<style>
	/* -- Responsive -- */
	@media (orientation: landscape) {
		:root {
			--text-scale-unit: 1vh;
		}
	}

	@media (orientation: portrait) {
		:root {
			--text-scale-unit: 1vw;
		}
	}

	html {
		font-size: calc(var(--text-scale-unit) * 1.5);
	}

	@media only screen and (min-width: 960px) {

		/* styles for browsers larger than 960px; */
		html {
			font-size: calc(var(--text-scale-unit) * 1.5);
		}
	}

	@media only screen and (min-width: 1366px) {

		/* styles for browsers larger than 1366px; */
		html {
			font-size: calc(var(--text-scale-unit) * 1.5);
		}
	}

	@media only screen and (min-width: 1440px) {

		/* styles for browsers larger than 1440px; */
		html {
			font-size: calc(var(--text-scale-unit) * 1.5);
		}
	}

	@media only screen and (min-width: 2000px) {

		/* styles for browsers larger than 2000px; */
		html {
			font-size: calc(var(--text-scale-unit) * 1.5);
		}
	}

	@media only screen and (max-device-width: 480px) {

		/* styles for mobile browsers smaller than 480px; (iPhone) */
		html {
			font-size: calc(var(--text-scale-unit) * 1);
		}
	}

	/* default iPad screens */
	@media only screen and (device-width: 768px) {
		html {
			font-size: calc(var(--text-scale-unit) * 1);
		}
	}

	/* different techniques for iPad screening */
	@media only screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation: portrait) {

		/* For portrait layouts only */
		html {
			font-size: calc(var(--text-scale-unit) * 1);
		}
	}

	@media only screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation: landscape) {

		/* For landscape layouts only */
		html {
			font-size: calc(var(--text-scale-unit) * 1);
		}
	}

	.size-lg {
		font-size: calc(var(--text-scale-unit) * 15);
	}

	.size-md {
		font-size: calc(var(--text-scale-unit) * 10);
	}

	.size-sm {
		font-size: calc(var(--text-scale-unit) * 5);
	}

	.size-xs {
		font-size: calc(var(--text-scale-unit) * 2);
	}

	/* -- Responsive -- */

	i {
		font-family: "Font Awesome 5 Free" !important;
	}

	html,
	body {
		min-height: 100%;
		height: 100vh;
		margin: 0;
	}

	body {
		background: #337ab7;
		background-repeat: no-repeat;
		background-attachment: fixed;
	}

	.center {
		display: flex;
		justify-content: center;
		align-items: center;
		text-align: center;
	}

	.management-container {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		/*padding:0rem 2rem 0rem 2rem;*/
		padding: 0 0 0 0rem;
	}

	.management-container>.header {
		display: flex;
		align-items: center;
		color: #ffffff;
		background-color: #344fa7;
	}

	.management-container>.content {
		width: 100%;
		background-color: #fdfdfd;
		border-right: 6px solid #a5a9ad;
		border-left: 6px solid #a5a9ad;
	}

	.dashboard {
		width: 100%;
		height: 92%;
		padding: 5px;
		border-radius: 30px;
		background-color: #ffffff;
	}

	.dataTables_wrapper {
		font-size: 1.5rem;
		width: 100%;
		height: 100%;
	}

	.dataTables_scroll {
		height: 100%;
	}

	/* div.dataTables_scrollHead table.table-bordered {
		width: 100% !important;
	} */

	.dataTables_scrollBody {
		padding-bottom: 0%;
	}

	.dataTables_wrapper .dataTables_paginate {
		float: right !important;
		text-align: right;
		padding-top: 0.25em;
	}

	.main_container {
		width: 95%;
		padding-right: 15px;
		padding-left: 15px;
		margin-right: auto;
		margin-left: auto;
	}

	.nowrap_container {
		display: flex;
		justify-content: center;
		/* height: auto; */
		height: 68%;
	}

	.block {
		display: block;
		width: 100%;
		margin-top: 0.25em;
		margin-bottom: 0.25em;
	}

	.none {
		background: transparent;
		border: none;
	}

	.div_hidden {
		display: none;
	}

	.button {
		white-space: nowrap;
		display: inline-block;
		font-weight: 400;
		text-align: center;
		vertical-align: middle;
		-webkit-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
		border: 1px solid transparent;
		padding: 0.25rem 0.5rem;
		font-size: 1.25rem;
		text-shadow: 0px 0px 20px #cecece;
		line-height: 1.5;
		border-radius: 0.2rem;
		transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out,
			border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
	}

	.button:hover {
		color: #000000 !important;
	}

	.button.trapezium {
		border-radius: 200px 200px 0px 0px !important;
	}

	.button.small {
		padding: 0.25rem 0.5rem;
		font-size: 1.25rem;
		line-height: 1.5;
		border-radius: 0.5rem;
		width: 50px;
		height: 32px;
	}

	.button.medium {
		padding: 0.25rem 1rem;
		font-size: 1.5rem;
		line-height: 1.5;
		border-radius: 0.5rem;
	}

	.button.filter {
		font-size: 1.25rem;
		color: #ffffff !important;
		border-color: #2c5364;
		background-color: #2c5364;
		border-radius: 30px;
		color: #ffffff;
	}

	.button.filter.active {
		border-color: #143c78;
		background-color: #0f2027;
	}

	.button.filter:hover {
		border-color: #143c78;
		background-color: #0f2027;
	}

	.button.c_lightblue {
		border-color: #c0c0c0;
		background-color: #e0e0e0;
		color: #000000;
	}

	.button.c_lightblue.active {
		border-color: #143c78 !important;
		background-color: #2c5364 !important;
		color: #ffffff !important;
	}

	.button.c_lightblue:hover {
		border-color: #143c78;
		background-color: #2c5364;
		color: #ffffff !important;
	}

	.button.c_darkblue {
		border-color: #c0c0c0;
		background-color: #e0e0e0;
		color: #000000;
	}

	.button.c_darkblue.active {
		border-color: #2c5364 !important;
		background-color: #0f2027 !important;
		color: #ffffff !important;
	}

	.button.c_darkblue:hover {
		border-color: #2c5364;
		background-color: #203a43;
		color: #ffffff !important;
	}

	.button.c_red {
		border-color: #c0c0c0;
		background-color: #e0e0e0;
		color: #000000;
	}

	.button.c_red.active {
		/*border-color: #EB0000 !important;*/
		background-color: #ff0000 !important;
		color: #ffffff !important;
	}

	.button.c_red:hover {
		border-color: #f18d01;
		background-color: #ff0000;
		color: #ffffff !important;
	}

	.button.c_green {
		border-color: #c0c0c0;
		background-color: #e0e0e0;
		color: #000000;
	}

	.button.c_green.active {
		/*border-color: #28A045 !important;*/
		background-color: #22746f !important;
		color: #ffffff !important;
	}

	.button.c_green:hover {
		border-color: #28a045;
		background-color: #22746f;
		color: #ffffff !important;
	}

	.button.static_blue {
		background: #344fa7;
		color: #ffffff;
		padding: 4px;
		width: 50px;
		font-size: 1.4321rem;
	}

	.button.c_blue {
		border-color: #c0c0c0;
		background-color: #e0e0e0;
		color: #000000;
	}

	.button.c_blue.active {
		border-color: #003cff !important;
		background-color: #253a77 !important;
		color: #ffffff !important;
	}

	.button.c_blue:hover {
		border-color: #f18d01;
		background-color: #005aff;
		color: #ffffff;
	}

	.button.c_yellow {
		border-color: #c0c0c0;
		background-color: #e0e0e0;
		color: #000000;
	}

	.button.c_yellow.active {
		/* border-color: #e5a956 !important;*/
		background-color: #e5a956 !important;
		color: #ffffff !important;
	}

	.button.c_yellow:hover {
		border-color: #ffb400;
		background-color: #ffc800;
		color: #ffffff !important;
	}

	.button.c_grey {
		border-color: #c0c0c0;
		background-color: #e0e0e0;
		color: #000000;
	}

	.button.c_grey.active {
		border-color: #c0c0c0 !important;
		background-color: #e0e0e0 !important;
		color: #ffffff !important;
	}

	.button.c_grey:hover {
		border-color: #c0c0c0;
		background-color: #e0e0e0;
		color: #ffffff !important;
	}

	.button.outline_green {
		border-color: #c0c0c0;
		background-color: #e0e0e0;
		color: #000000;
	}

	.button.outline_green:hover {
		border-color: #28a045;
		background-color: #28b445;
		color: #ffffff !important;
	}

	.trashbtn {
		border: none;
		background-color: transparent;
		text-align: center;
		vertical-align: middle;
		-webkit-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
	}

	button:disabled,
	button[disabled] {
		border-color: #c0c0c0 !important;
		background-color: #e0e0e0 !important;
		color: #ffffff !important;
	}

	button:disabled,
	button[disabled]:hover {
		border-color: #c0c0c0 !important;
		background-color: #e0e0e0 !important;
		color: #ffffff !important;
	}

	.colBtn {
		margin-right: 1%;
		padding-left: 0.5px;
		padding-right: 0.5px;
	}

	.colFilter {
		padding: 0.5% 1%;
	}

	.hidden {
		display: none !important;
	}

	.mod-thin {
		font-weight: 350;
	}

	hr {
		padding: 0 0;
		margin: 0.25rem 0;
	}

	hr.darkblue {
		display: block;
		height: 1px;
		border: 0;
		border-top: 1px solid linear-gradient(90deg, #203a43, #0f2027, #2c5364);
		margin: 0;
		padding: 0;
	}

	ul.inline {
		margin: 0;
		padding: 0;
	}

	ul,
	ul.inline>li {
		display: inline;
	}

	ul.seperate_bd>li {
		border-left: 1px solid RGBA(255, 255, 255, 1);
		padding-left: 0.5rem;
	}

	ul.seperate_bd>li:first-child {
		border-left: none;
	}

	ul.seperate_box li {
		background-color: #156a6c;
		padding: 5px;
		border-radius: 5px;
		-moz-border-radius: 5px;
		-webkit-border-radius: 5px;
	}

	ul.nobullet {
		list-style-type: none;
	}

	ul.nobullet button {
		font-size: 1.5rem;
		font-weight: 500;
		padding: 5px 10px 5px 10px;
		color: #000000;
		background-color: transparent;
		border: none;
	}

	ul.nobullet li {
		border-bottom: 1px solid #eaeaea;
	}

	ul.nobullet li:first-child {
		border-top: 1px solid #eaeaea;
	}

	.sidenav_panel {
		position: absolute;
		bottom: 0;
		left: -25rem;
		width: 25rem;
		height: 100%;
		padding: 2rem 0 0 1rem;
		border-right: 1px solid #2c5364;
		background-color: #ffffff;
		z-index: 1100;
	}

	.sidenav_panel.active {
		left: 0;
		transition: left 0.2s linear;
	}

	.out-panel_btn {
		text-align: center;
		font-size: 2.5rem;
		/*height:calc(100% + 2rem);*/
		width: calc(30px + 4rem);
		/*margin: -1rem 0 -1rem 0;
	padding: 1.5rem 15px;*/
	}

	.out-panel_btn.active {
		background-color: RGBA(255, 255, 255, 0.4);
	}

	.sidenav_panel>.panel_btn {
		position: absolute;
		top: 2rem;
		right: -30px;
		width: 30px;
		font-size: 18px;
		border-radius: 0px 4px 4px 0px;
		color: #333333;
		background-color: #ffffff;
		text-align: center;
		vertical-align: middle;
		z-index: 1000;
	}

	.sidenav_panel.active .panel_btn {
		right: -15px;
		transition: left 0.2s linear;
	}

	.sidenav_panel.active .panel_btn:after {
		content: "";
		position: absolute;
		right: 0;
		width: 50%;
		height: 100%;
		border-radius: 0px 4px 4px 0px;
		border: 1px solid #2c5364;
		border-left: none;
		z-index: 1000;
	}

	table {
		font-size: 1.4vw;
		width: 80%;
		height: auto;
		margin-bottom: 0rem !important;
		color: #212529;
		border-collapse: collapse;
	}

	thead {
		display: table-header-group;
		vertical-align: middle;
		border-color: inherit;
	}

	tr {
		font-weight: 400;
		display: table-row;
		vertical-align: middle;
		border-color: inherit;
		height: auto;
	}

	thead tr {
		background-attachment: fixed;
		background: #344fa7;
	}

	tr th,
	thead {
		padding-top: 0.5rem;
		padding-bottom: 0.5rem;
	}

	tr th,
	td {
		font-weight: 400;
		white-space: nowrap;
		vertical-align: middle;
		border-bottom: 2px solid #dee2e6;
	}

	tr th {
		text-align: left;
		color: #ffffff;
		background: transparent;
	}

	tr td {
		text-align: left;
		padding: 0.1rem 0.1rem;
	}

	tbody tr:hover td {
		background-color: #C890C8;
	}

	tbody tr.active_call {
		color: #000000 !important;
		background-color: RGBA(0, 84, 60, 0.66) !important;
	}

	.tb_main tbody tr:hover td:nth-last-child(1),
	.tb_main tbody tr:hover td:nth-last-child(2),
	.tb_main tbody tr:hover td:nth-last-child(3),
	.tb_main tbody tr:hover td:nth-last-child(4),
	.tb_main tbody tr:hover td:nth-last-child(5) {
		background-color: initial;
	}

	.tb_pn tbody tr:hover td:nth-last-child(1),
	.tb_pn tbody tr:hover td:nth-last-child(2),
	.tb_pn tbody tr:hover td:nth-last-child(3),
	.tb_pn tbody tr:hover td:nth-last-child(4),
	.tb_pn tbody tr:hover td:nth-last-child(5),
	.tb_pn tbody tr:hover td:nth-last-child(6),
	.tb_pn tbody tr:hover td:nth-last-child(7) {
		background-color: initial;
	}

	.tb_print tr:hover td:last-child {
		background-color: initial;
	}

	#head_list_patient>tr>th:last-child {
		padding-right: 15px;
	}



	tbody tr.active_hold {
		color: #000000 !important;
		background-color: RGBA(255, 200, 0, 0.65) !important;
	}

	tr th:first-child {
		border-radius: 8px 0px 0px px !important;
		-moz-border-radius: 8px 0px 0px 8px !important;
		-webkit-border-radius: 8px 0px 0px 8px !important;
	}

	tr th:last-child {
		border-radius: 0px 5px 5px 0px;
		-moz-border-radius: 0px 5px 5px 0px;
		-webkit-border-radius: 0px 5px 5px 0px;
	}

	tr td:first-child {
		border-radius: 8px 0px 0px 8px;
		-moz-border-radius: 8px 0px 0px 8px;
		-webkit-border-radius: 8px 0px 0px 8px;
	}

	tr td:last-child {
		border-radius: 0px 5px 5px 0px;
		-moz-border-radius: 0px 5px 5px 0px;
		-webkit-border-radius: 0px 5px 5px 0px;
	}

	.input_icon,
	.input_dual_icon {
		border-radius: 20px;
	}

	input[type="text"].input_icon {
		padding-left: 2.5em;
		padding-right: 3em;
		margin-left: 0 !important;
	}

	input[type="text"].input_dual_icon {
		padding-left: 2.5em;
		padding-right: 5em;
	}

	input[type="text"] {
		font-size: 1.5rem;
	}

	input[type="text"]::-webkit-input-placeholder {
		/* Edge */
		padding-left: 0.5em;
		text-align: left;
	}

	input[type="text"]:-ms-input-placeholder {
		/* Internet Explorer 10-11 */
		padding-left: 0.5em;
		text-align: left;
	}

	input[type="text"]::placeholder {
		padding-left: 0.5em;
		text-align: left;
	}

	.btn_row a button {
		font-size: 1.5rem;
		font-weight: 500;
		padding: 2px 10px 2px 10px;
		color: #000000;
		background-color: #ffffff;
		border: 0.5px solid #dadada;
	}

	.btn_row a.active button {
		color: #ffffff !important;
		font-weight: 550;
		background-color: #125761;
		border: 0.5px solid #ffffff;
	}

	.btn_row a button:hover {
		color: #ffffff !important;
		font-weight: 550;
		background-color: #b9b8b8;
		box-shadow: 0px 1px 3px 0px #474747;
	}

	.btn_row a:first-child button {
		border-right-width: 0.5px;
		border-radius: 5px 0px 0px 5px;
		-moz-border-radius: 5px 0px 0px 5px;
		-webkit-border-radius: 5px 0px 0px 5px;
	}

	.btn_row a:last-child button {
		border-left-width: 0.5px;
		border-radius: 0px 5px 5px 0px;
		-moz-border-radius: 0px 5px 5px 0px;
		-webkit-border-radius: 0px 5px 5px 0px;
	}

	.building_row {
		position: relative;
	}

	.building_row::before {
		position: absolute;
		bottom: 0;
		margin-left: -20px;
		width: calc(100vw + 15px);
		content: " ";
		font-size: 0px;
		border-color: #d5d5d5;
		border-style: solid;
		border-width: 1px;
	}

	.building_row button {
		position: relative;
		font-size: 1.5rem;
		padding: 5px 15px;
		border-color: #d5d5d5;
		border-style: solid;
		border-width: 1px;
		border-radius: 15px 15px 0 0;
		color: #000000;
		background-color: #e8e8e8;
	}

	.building_row button:focus {
		outline: 0;
	}

	.building_row button::after {
		content: " ";
		position: absolute;
		bottom: 0;
		left: -1px;
		height: 50%;
		width: calc(100% + 2px);
		border-width: 0.5px;
		border-style: solid;
		border-color: #dddddd;
		border-top: none;
		border-bottom: none;
		z-index: 1001;
	}

	.building_row button.active {
		border-bottom: none;
		background-color: #ffffff;
	}

	.building_row button.active::after {
		content: " ";
		position: absolute;
		bottom: -1px;
		left: 0;
		height: 50%;
		width: 101%;
		border-width: 1px;
		border-style: solid;
		border-color: #d5d5d5;
		border-top: none;
		border-bottom: 1px solid #ffffff;
		z-index: 1002;
	}

	.building_room {
		font-size: 1.5rem;
		padding: 10px 0px;
		width: 274px;
		border-style: none;
		border-radius: 100px;
		color: #ffffff;
		height: 63px;
		background: rgb(37, 57, 118);
		background: linear-gradient(90deg,
				rgba(37, 57, 118, 1) 0%,
				rgba(50, 88, 133, 1) 100%);
	}

	.building_room2 {
		font-size: 2rem;
		padding: 10px 0px;
		width: 274px;
		border-style: none;
		border-radius: 100px;
		color: #263b77;
		height: 63px;
		background: #ffffff;
		border: 2px solid #273b77;
	}

	.building_room:disabled,
	.building_room[disabled] {
		color: #ffffff !important;
		background-color: #619aa9 !important;
	}

	.building_room.active {
		background-color: #619aa9;
	}

	.filter_style {
		font-size: 1.987654321rem;
		height: 50px;
		width: 182px;
		border-style: none;
		border-radius: 100px;
		color: #ffffff;
		background: rgb(37, 57, 118);
		background: linear-gradient(90deg,
				rgba(37, 57, 118, 1) 0%,
				rgba(50, 88, 133, 1) 100%);
	}

	.filter_style.active {
		font-size: 2rem;
		padding: 5px 15px;
		border-style: none;
		border-radius: 20px;
		color: rgb(255, 255, 255);
		background-image: linear-gradient(90deg, #235c58, #468984);
	}

	.tr_color_green {
		background-color: #e0eced;
	}

	.tr_color_orange {
		background-color: #faeedc;
	}

	.btn_de {
		font-size: 2.25rem;
		line-height: 0.5;
		font-weight: 700;
		border-radius: 0.5rem 0 0 0.5rem;
		width: 25px;
		height: 25px;
		color: #ffffff !important;
		background-color: #275583 !important;
		border: 0px solid;
	}

	.btn_pl {
		font-size: 2.25rem;
		line-height: 0.5;
		font-weight: 700;
		border-radius: 0rem 0.5rem 0.5rem 0rem;
		width: 25px;
		height: 25px;
		color: #ffffff !important;
		background-color: #253979 !important;
		border: 0px solid;
	}

	.span_limit {}

	.span_limit.active {
		border-top: 1px solid;
		width: 52px;
		height: 25px;
		border-bottom: 1px solid;
	}

	.dropdown-item:hover {
		color: #ffffff;
		background-color: #007bff;
	}

	/* ///////////////////////////////////////////////////////////// */
	.main_h {
		white-space: nowrap;
		margin: 0;
		font-size: 1.8vw;
		text-shadow: 1px 1px 1px #0d173a;
	}

	thead {
		text-shadow: 1px 1px 1px #0d173a;
	}

	.menu_style {
		height: 32%;
		min-height: 20rem;
		padding-top: 4px;
		padding-bottom: 4px;
		padding-right: 2px;
	}

	.btn_pr {
		height: 92%;
		padding: 8%;
		display: block;
		padding-bottom: 0;
		outline: none !important;
		padding-left: 1.4rem;
		padding-right: 1.4rem;
		width: 8vw;
		border-radius: 0.6rem;
		box-shadow: 0px 0px 1px 0px #969696;
	}

	.btn_pr.active {
		background-color: #e8e7e7;
		box-shadow: 0px 1px 3px 2px #969696;
	}

	.dataTables_scrollBody {
		height: 90%;
	}

	.table td,
	.table th {
		padding-top: 0.24rem;
		padding-bottom: 0.24rem;
	}

	th,
	td {
		text-align: center !important;
		vertical-align: middle !important;
	}

	.card_ctn {
		height: 100%;
		padding-left: 0.8rem;
		padding-right: 0.8rem;
	}

	.card_h {
		height: 20%;
		font-size: 1.4vw;
	}

	.card_bd {
		height: 60%;
	}

	.card_bd>img {
		height: 4.4vw;
	}

	.card_f {
		height: 18%;
		font-size: 1.4vw;
	}

	.num_pn {
		height: 90%;
		background-color: #a2a2a2;
		color: #fff;
		border-radius: 0.3rem;
		padding-left: 5px;
		padding-right: 5px;
		min-width: 37px;
	}

	.num_pn.active {
		background-color: #3165ff;
	}

	.iconsize_tr {
		height: 2.2vw;
		margin: 0 2px;
	}

	.icon_td {
		background-color: #707070;
		color: #fff;
		border-radius: 0.3rem;
		box-shadow: 0px 0px 2px #3b3b3b;
		min-width: 36px;
	}

	/* .icon_td.active {
		background-color: #fed455;
		box-shadow: none;
		border: 1px solid #f9c125;
	} */

	.icon_x {
		background-color: #707070;
		color: #fff;
		border-radius: 0.3rem;
		box-shadow: 0px 0px 2px #3b3b3b;
		padding-left: 6px;
		padding-right: 6px;
		min-width: 36px;
	}

	.icon_x:hover {
		background-color: #dc3545;
	}

	.icon_x.active {
		background-color: #dc3545;
	}

	/* manage */

	.page_row {
		position: relative;
	}

	.page_row::before {
		position: absolute;
		bottom: 0;
		margin-left: -20px;
		width: calc(100vw + 15px);
		content: " ";
		font-size: 0px;
		border-color: #d5d5d5;
		border-style: solid;
		border-width: 0.5px;
	}

	.page_row button {
		position: relative;
		font-size: 1.5rem;
		padding: 5px 15px;
		border-color: #d5d5d5;
		border-style: solid;
		border-width: 1px;
		border-radius: 5px 5px 0 0;
		color: #000000;
		background-color: #e8e8e8;
		min-width: 110px;
	}

	.page_row button:focus {
		outline: 0;
	}

	.page_row button::after {
		content: " ";
		position: absolute;
		bottom: 0;
		left: -1px;
		height: 50%;
		width: calc(100% + 2px);
		border-width: 0.5px;
		border-style: solid;
		border-color: #dddddd;
		border-top: none;
		border-bottom: none;
		z-index: 1001;
	}

	.page_row button.active {
		border-bottom: none;
		background-color: #ffffff;
	}

	.page_row button.active::after {
		content: " ";
		position: absolute;
		bottom: -1px;
		left: 0;
		height: 50%;
		width: 101%;
		border-width: 1px;
		border-style: solid;
		border-color: #d5d5d5;
		border-top: none;
		border-bottom: 1px solid #ffffff;
		z-index: 1002;
	}

	.row {
		margin-right: 0 !important;
		margin-left: 0 !important;
	}

	.modal-header {
		color: #FFFFFF;
		background-color: #344fa7;
	}

	.icon_row img {
		margin: 0 auto;
		height: 8rem;
	}

	.main-btn_row {
		min-height: 110px;
	}

	.main-btn_row button {
		margin: 0 auto;
		padding: 10px 30px;
	}

	.norad-btn_row button {
		border-radius: 0;
		margin: 0 auto;
		padding: 10px 30px;
	}

	.btn-main {
		margin: 0 auto;
		padding: 10px 30px;
	}

	.btn-norad {
		border-radius: 0;
		margin: 0 auto;
		padding: 10px 30px;
	}

	.btn-flat {
		margin: 0 auto;
		padding: 5px 30px;
	}

	.btn-cyan {
		color: #fff;
		background-color: #33b5e5;
		border-color: #33b5e5f5;
	}


	.btn_sm_gray {
		min-width: 120px;
		max-width: 250px;
		padding: .6rem !important;
		border-radius: .3rem !important;
		border: none;
		color: #fff;
		background-color: #525252;
	}

	.btn_sm_gray:hover {
		background-color: #9c9c9c;
	}

	.btn_sm_blue {
		min-width: 120px;
		max-width: 250px;
		padding: .6rem !important;
		border-radius: .3rem !important;
		border: none;
		background-color: #344fa7;
		color: #fff;
	}

	.btn_sm_blue:hover {
		background-color: #6b83d0;
	}

	.btn_inct {
		min-width: 120px;
		max-width: 250px;
		padding: .6rem !important;
		border-radius: .3rem !important;
		border: none;
		color: #fff;
		background-color: #33b5e5;
		font-size: 1.4rem;
		margin: 8px !important;
	}

	.btn_inct:hover {
		background-color: #5170d4;
	}

	.select_order_send {
		outline: none !important;
	}

	.close {
		text-shadow: none;
		color: #fff;
		font-size: 1.654321rem;
	}

	textarea.form-control {
		min-height: 200px;
		font-size: 1.5rem;
	}

	.loding_log,
	.loding_log2 {
		display: flex;
		z-index: 9;
		color: #4964b9;
		font-size: 1.4rem;
		background-color: #ffffff7d;
		width: 100%;
		height: 100%;
		position: absolute;
	}

	.dataTables_scrollHeadInner {
		width: 100% !important;
	}

	.btn-thg-green {
		color: #fff;
		background-color: #20844a !important;
		border-color: #38c172 !important;
	}

	.btn-thg-pink {
		color: #fff;
		background-color: #f35581;
		border-color: #f35581;
	}

	.btn-thg-yellow {
		color: #fff;
		background-color: #ffd457 !important;
		border-color: #f5b908 !important;
	}

	.btn-thg-blue {
		color: #fff;
		background: -webkit-gradient(linear, left top, left bottom, from(#1d78da), to(#007bff));
		/*background-color: #559bf3;*/
		border-color: #003af9;
	}

	.modal-content {
		background-color: #344fa7;
		border: 2px solid #344fa7;
		border-radius: 0.3rem;
	}

	.modal-body {
		background-color: #ffffff;
		border-radius: 0 0 .3rem 0.3rem;
	}

	.whbutton {
		width: 38px;
		height: 38px;
	}

	.closeptall {
		border: none;
		outline: none;
		background: none;
	}
</style>

<style>

.page_row button {
			position: relative;
			font-size: 1.5rem;
			padding: 10px 26px;
			border-color: #d5d5d5;
			border-style: solid;
			border-width: 1px;
			border-radius: 5px 5px 0 0;
			color: #000000;
			background-color: #e8e8e8;
			min-width: 110px;
		}

		.page_row button:focus {
			outline: 0;
		}

		.page_row button::after {
			content: " ";
			position: absolute;
			bottom: 0;
			left: -1px;
			height: 50%;
			width: calc(100% + 2px);
			border-width: 0.5px;
			border-style: solid;
			border-color: #dddddd;
			border-top: none;
			border-bottom: none;
			z-index: 1001;
		}

		.page_row button.active {
			border-bottom: none;
			background-color: #ffffff;
		}

		.page_row button.active::after {
			content: " ";
			position: absolute;
			bottom: -1px;
			left: 0;
			height: 50%;
			width: 101%;
			border-style: unset;
			z-index: 1002;
		}

		.center {
			display: flex;
			justify-content: center;
			align-content: center;
			text-align: center;
		}

		.lab_on {
			padding: 8px 12px;
			height: fit-content;
			font-size: 1.35rem;
			border: none;
			border-radius: .3rem;
			color: #ffffff;
			background-color: #344fa7;
		}

		.lab_on.active {
			background-color: #d5d5d5;
		}

		.page_row button.active {
			border-bottom: none;
			background-color: #ffffff;
		}

		.page_row button.active::after {
			content: " ";
			position: absolute;
			bottom: -1px;
			left: 0;
			height: 50%;
			width: 101%;
			border-style: unset;
			z-index: 1002;
		}
</style>