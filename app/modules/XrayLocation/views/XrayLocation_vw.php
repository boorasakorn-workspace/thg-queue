<div class="management-container">

    <div id="ManagementOuterTab" class="header row no-gutters" style="padding-left: 1.2vw; height: 6%;">

        <div class="col-12" style="display:flex;align-items: center;justify-content: space-between;">
            <div>
                <span class="main_h">DASHQUEUE</span>
            </div>
            <div>
                <i class="fas fa-sign-out-alt" style="font-size:16px; padding-right:10px;" id="logout"></i>
            </div>
        </div>
    </div>


    <div id="ManagementDashboard" class="content" style="position: relative;height:94%; border:none;">

        <!-- Management Content -->

        <div id="headDashboard" class="container-fluid" style=" height: 8%; padding-top:0.5rem; padding-left:0.5rem;">
            <div class="row page_row">
                <a>
                    <button class="col active" id='tabpt'>
                        รายชื่อผู้ป่วยห้อง เอกซ์เรย์
                    </button>
                </a>
                <a style="margin-left:0.3rem;">
                    <button class="col" id='tabptcomp'>
                        ตรวจเสร็จ
                    </button>
                </a>
                <div class="col">
                    <h1 id="desk_span" style="float: right;"></h1>
                </div>
            </div>
        </div>

        <div class="container-fluid" style="height: 10%; padding:0;">
            <div class="row no-gutters" style="padding: 0.5rem 2rem 0.5rem .8rem;display:flex;justify-content:space-between;">

                <div class="col-lg-3 col-md-5 col-sm-3">
                    <div class="form-inline center" style="padding: .25rem 0rem;">
                        <div class="row col-12 center loding_log" id="loding_log_scan" style="display:none;">
                            <i id="loading" class="fa fa-spinner fa-spin fa-3x fa-fw" style='font-size:20px;'></i>
                        </div>
                        <input id="box_search" class="form-control form-control-sm ml-3 w-100 input_icon " type="search" placeholder="Scan Check In" aria-label="Search" style="font-size:1.5rem;">

                    </div>
                </div>

            </div>
        </div>

        <!-- Dashboard Table -->
        <div class=" nowrap_container " id='tb_order_other' style="display: none;">
            <table id="" class="table_data  tb_pn table table-striped table-bordered  ui-datatable borderless" style=" margin-top: 0 !important;">
                <thead id="head_list_patient_other">
                    <tr>
                        <th>#</th>
                        <th>Call</th>
                        <th>No</th>
                        <th>Name</th>
                        <th>HN</th>
                        <th>Waiting</th>
                        <th>Room</th>
                        <th>Hold</th>
                        <th>Note</th>
                        <th>Close</th>
                    </tr>
                </thead>
                <tbody style="" id="body_list_patient_other">

                </tbody>
            </table>
        </div>
        <!-- /Dashboard Table -->

        <!-- Dashboard Table -->
        <div class=" nowrap_container " id='tb_pt_comp' style="display: none;">
            <table id="" class="table_data  tb_pn table table-striped table-bordered  ui-datatable borderless" style=" margin-top: 0 !important;">
                <thead id="head_list_patient_other">
                    <tr>
                        <th>#</th>
                        <th>No</th>
                        <th>Name</th>
                        <th>HN</th>
                        <th>Note</th>
                    </tr>
                </thead>
                <tbody style="" id="body_pt_comp">
                </tbody>
            </table>
        </div>
        <!-- /Dashboard Table -->


        <script>
            var TableRowCount = 1;
        </script>
        <!-- /Management Content -->

    </div>



    <!-- modal print -->
    <div class="modal" tabindex="-1" role="dialog" id="md_print">
        <div class="modal-dialog modal-dialog-centered" style="max-width: 700px;" role="document">
            <div class="modal-content" style="border: 2px solid #344fa7;">
                <div class="modal-header center" style="min-height: 3rem; padding: .8rem;">
                    <h5 class="modal-title">เลือก Print</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!-- <div class="modal-header center" style="min-height: 3rem; padding: .6rem;">
                    <h5 class="modal-title">เลือก Print</h5>
                </div> -->
                <div class="modal-body col-12" style="padding: 0rem 1rem; min-height: 250px;">
                    <div class="container-fluid">

                        <div class="row" style="margin-top:10px;">
                            <table class="table table-bordered tb_print" style="font-size: 1.555rem;">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>HN</th>
                                        <th>VN</th>
                                        <th>Location</th>
                                        <th>Re-Print</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody_print">

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
                <div class="row col-12" style="display:none;">
                    <input class="col-4" type="hidden" id="patientdetail_uid_print">
                    <input class="col-4" type="hidden" id="patientxray_uid_print">
                    <input class="col-4" type="hidden" id="xraycategory_uid_print">
                    <input class="col-4" type="hidden" id="check_status_type_print">
                    <input class="col-4" type="hidden" id="location_now_print">
                    <input class="col-4" type="hidden" id="secure_code_print">
                </div>
            </div>
        </div>
    </div>

    <!-- modal send -->
    <div class="modal" tabindex="-1" role="dialog" id="md_send">
        <div class="modal-dialog modal-dialog-centered" style="max-width: 700px;" role="document">
            <div class="modal-content">
                <div class="modal-header center" style="min-height: 3rem; padding: .8rem;">
                    <h5 class="modal-title">เลือกส่งคิว</h5>
                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button> -->
                </div>
                <div class="modal-body center">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12 icon_row row" style="padding-bottom: .8rem;" id="area_order_send">

                            </div>
                            <div class="row col-12" style="display: none;">
                                <input class="col-4" type="hidden" id="PatientXray_uid">
                                <input class="col-4" type="hidden" id="PatientDetail_uid">
                                <input class="col-4" type="hidden" id="status_type">
                                <input class="col-4" type="hidden" id="location_now_send">
                            </div>
                        </div>
                        <div class="row center col-12" style="padding: .8rem;">
                            <div class="norad-btn_row">
                                <button class="btn_sm_blue" style="margin-right: 20px;" id="conf_send">ตกลง</button>
                                <button class="btn_sm_gray" class="close" data-dismiss="modal">ยกเลิก</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- modal counter -->
    <div class="modal" tabindex="-1" role="dialog" id="md_counter">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header center" style="min-height: 3rem; padding: .8rem;">
                    <h5 class="modal-title">เรียกคิวห้อง <span id='tital_counter'></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="row align-items-end  modal-body main-btn_row">
                    <div class="col-12 center" style=" display: block;" id="list_call">

                    </div>
                    <div class="row col-12" style="display: block;">
                        <input class="col-4" type="hidden" id="order_location_uid">
                        <input class="col-4" type="hidden" id="patientdetail_uid_counter">
                        <input class="col-4" type="hidden" id="patientxray_uid_counter">
                        <input class="col-4" type="hidden" id="status_type_counter">
                        <input class="col-4" type="hidden" id="location_now_counter">
                        <input class="col-4" type="hidden" id="nation_counter">
                        <input class="col-4" type="hidden" id="queueno_counter">
                        <input class="col-4" type="hidden" id="xraycategoryuid_counter">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- modal note -->
    <div class="modal" tabindex="-1" role="dialog" id="md_note">
        <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 600px;">
            <div class="modal-content">
                <div class="modal-header center" style="min-height: 3rem; padding: .8rem;">
                    <h5 class="modal-title">Note</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body center">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col" style="padding: .8rem;">
                                <textarea class="form-control" rows="5" id="textarea_note" name="textarea_note"></textarea>
                            </div>
                        </div>
                        <div class="row col-12" style="display:none;">
                            <input class="col-4" type="hidden" id="patientdetail_uid_note">
                            <input class="col-4" type="hidden" id="patientxray_uid_note">
                            <input class="col-4" type="hidden" id="check_status_type_note">
                            <input class="col-4" type="hidden" id="location_now_note">
                        </div>
                        <div class="row">
                            <div class="col-12 center">
                                <button class="btn btn-flat btn-success" id='conf_note'>บันทึก</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- modal hold -->
    <div class="modal" tabindex="-1" role="dialog" id="md_hold">
        <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 600px;">
            <div class="modal-content">
                <div class="modal-header center" style="min-height: 3rem; padding: .8rem;">
                    <h5 class="modal-title">Hold</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body center">
                    <div class="container-fluid" style="padding: 0">
                        <div class="row col-12" style="padding: 0 0 .8rem 0;">
                            <div class="col-12">
                                <select class="form-control" id="hold_message-select" style="font-size: 1.5rem;">
                                </select>
                            </div>
                            <div class="col-12" style="padding-top: 1rem">
                                <textarea class="form-control" rows="5" id="hold_message" name="hold_message"></textarea>
                            </div>
                        </div>
                        <div class="row col-12" style="display: none;">
                            <input class="col-4" type="hidden" id="patientdetail_uid_hold">
                            <input class="col-4" type="hidden" id="patientxray_uid_hold">
                            <input class="col-4" type="hidden" id="check_status_type_hold">
                            <input class="col-4" type="hidden" id="location_now_hold">
                        </div>
                        <div class="row">
                            <div class="col" style="text-align:center;">
                                <button class="btn btn-flat btn-success" id="conf_hold">บันทึก</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- modal complete -->
    <div class="modal" tabindex="-1" role="dialog" id="md_complete">
        <div class="modal-dialog modal-dialog-centered" style="max-width: 600px;" role="document">
            <div class="modal-content" style="border: 2px solid #344fa7;">
                <div class="modal-header center" style="min-height: 3rem; padding: .8rem;">
                    <h5 class="modal-title">ตรวจเสร็จ</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body center">
                    <div class="container-fluid">
                        <div class="row center">
                            <h2>ยืนยันการตรวจเสร็จ ?</h2>
                        </div>
                        <div class="row">
                            <div class="col-12 form-control center" style="padding-bottom: .8rem;min-height: 120px; border: none;">
                                <h1 style="font-size: 4rem;" id="complete_show"></h1>
                            </div>
                        </div>
                        <div class="row center col-12">
                            <div class="norad-btn_row">
                                <button class="btn_sm_blue" style="margin-right: 20px;" id="conf_complete">ใช่</button>
                                <button class="btn_sm_gray" class="close" data-dismiss="modal">ไม่</button>
                            </div>
                        </div>
                        <div class="row col-12" style="display:none;">
                            <input class="col-4" type="hidden" id="patientdetail_uid_complete">
                            <input class="col-4" type="hidden" id="patientxray_uid_complete">
                            <input class="col-4" type="hidden" id="check_status_type_complete">
                            <input class="col-4" type="hidden" id="location_now_complete">
                            <input class="col-4" type="hidden" id="hn_complete">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- modal close Queue -->
    <div class="modal" tabindex="-1" role="dialog" id="md_closeq">
        <div class="modal-dialog modal-dialog-centered" style="max-width: 600px;" role="document">
            <div class="modal-content" style="border: 2px solid #344fa7;">
                <div class="modal-header center" style="min-height: 3rem; padding: .8rem;">
                    <h5 class="modal-title">ปิดคิว</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body center">
                    <div class="container-fluid">
                        <div class="row center">
                            <h2>ยืนยันการปิดคิว ?</h2>
                        </div>
                        <div class="row">
                            <div class="col-12 form-control center" style="padding-bottom: .8rem;min-height: 120px; border: none;">
                                <h1 style="font-size: 4rem;" id="closeq_show"></h1>
                            </div>
                        </div>
                        <div class="row col-12" style="display:none;">
                            <input class="col-4" type="hidden" id="patientdetail_uid_closeq">
                            <input class="col-4" type="hidden" id="patientxray_uid_closeq">
                            <input class="col-4" type="hidden" id="check_status_type_closeq">
                            <input class="col-4" type="hidden" id="location_now_closeq">
                            <input class="col-4" type="hidden" id="hn_closeq">
                        </div>
                        <div class="row center col-12">
                            <div class="norad-btn_row">
                                <button class="btn_sm_blue" style="margin-right: 20px;" id="conf_closeq">ใช่</button>
                                <button class="btn_sm_gray" class="close" data-dismiss="modal">ไม่</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- modal note -->
    <div class="modal" tabindex="-1" role="dialog" id="md_alert_other">
        <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 600px;">
            <div class="modal-content">
                <div class="modal-header center" style="min-height: 3rem; padding: .8rem;">
                    <h5 class="modal-title">แจ้งเตือน</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body center">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col" style="padding: .8rem; font-size:2.4rem;" id="alert_text">

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 center">
                                <button class="btn btn-flat btn-success" data-dismiss="modal">ปิด</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- modal note -->
    <div class="modal" tabindex="-1" role="dialog" id="md_order_newqueue">
        <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 600px;">
            <div class="modal-content">
                <div class="modal-header center" style="min-height: 3rem; padding: .8rem;">
                    <h5 class="modal-title">แจ้งเตือน</h5>
                </div>
                <div class="modal-body center">
                    <div class="row col-12 center loding_log2" id="loding_log_scan2" style="display:none;">
                        <i id="loading" class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
                    </div>
                    <div class="container-fluid">
                        <input type="hidden" id="hn_new_queue">
                        <div class="row">
                            <div class="col" style="padding: .8rem; font-size:2.4rem;" id="">
                                คุณต้องการขอคิวหรือไม่
                            </div>
                        </div>
                        <div class="row center col-12">
                            <div class="norad-btn_row">
                                <button class="btn_sm_blue" style="margin-right: 20px;" id="ordernewqueue">ขอคิว</button>
                                <button class="btn_sm_gray" class="close" data-dismiss="modal">ยกเลิก</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- modal duplicate hn -->
    <div class="modal" tabindex="-1" role="dialog" id="md_alert_dduplicate">
        <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 600px;">
            <div class="modal-content">
                <div class="modal-header center" style="min-height: 3rem; padding: .8rem;">
                    <h5 class="modal-title">แจ้งเตือน</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body center">
                    <div class="container-fluid">
                        <div class="row">
                            <div class='col-12'>
                                <span style="padding: .8rem; font-size:2rem; font-weight:bold;" id="showhnduplicate"><span>
                            </div>
                            <div class='col-12'>
                                <span style="padding: .8rem; font-size:2rem;"> VN ที่เคย Scan ผ่านไปแล้ว </span>
                            </div>
                            <div class="col" style="padding: .8rem; font-size:1.8rem;" id="alert_text_duplicate">
                                <table class="table table-bordered tb_dup" style="font-size: 1.555rem;">
                                    <thead>
                                        <tr>
                                            <th>VN</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbody_dup">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class='col-12'>
                                <span style="padding: .8rem; font-size:2rem;"> VN ที่เพิ่มมาใหม่ </span>
                            </div>
                            <div class="col" style="padding: .8rem; font-size:1.8rem;" id="alert_text_duplicate">
                                <table class="table table-bordered tb_newhn" style="font-size: 1.555rem;">
                                    <thead>
                                        <tr>
                                            <th>VN</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbody_newhn">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 center">
                                <button class="btn btn-flat btn-success" data-dismiss="modal" id="closemdnewvn" data-scannew=''>ปิด</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- modal printoutofloc -->
    <div class="modal" tabindex="-1" role="dialog" id="md_printoutofloc">
        <div class="modal-dialog modal-dialog-centered" style="max-width: 700px;" role="document">
            <div class="modal-content" style="border: 2px solid #344fa7;">
                <div class="modal-header center" style="min-height: 3rem; padding: .8rem;">
                    <h5 class="modal-title">เลือก Print location ที่ไม่ไช้ระบบคิว</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!-- <div class="modal-header center" style="min-height: 3rem; padding: .6rem;">
                    <h5 class="modal-title">เลือก Print</h5>
                </div> -->
                <div class="modal-body col-12" style="padding: 0rem 1rem; min-height: 250px;">
                    <div class="container-fluid">

                        <div class="row" style="margin-top:10px;">
                            <table class="table table-bordered tb_print" style="font-size: 1.555rem;">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>HN</th>
                                        <th>VN</th>
                                        <th>Location</th>
                                        <th>Re-Print</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody_printoutofloc">
                                </tbody>
                            </table>
                        </div>
                        <div class="row" style="margin-top:10px;">
                            <button class="btn btn-flat btn-success" id="btn_after_ourloc" data-dismiss="modal">ปิด</button>
                        </div>


                    </div>
                </div>
                <div class="row col-12" style="display:none;">
                </div>
            </div>
        </div>
    </div>



    <!-- modal closeallpt -->
    <div class="modal" tabindex="-1" role="dialog" id="closeallpt">
        <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 600px;">
            <div class="modal-content">
                <div class="modal-header center" style="min-height: 3rem; padding: .8rem;">
                    <h5 class="modal-title">ยืนยันการปิดคิว HN</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body center">
                    <div class="container-fluid" style="padding: 0">
                        <div class="row col-12" style="padding: 0 0 .8rem 0;">
                            <div class="col-12">
                                <span style="padding: .8rem; font-size:2rem;"> คุณต้องการปิดคิวของ HN นี้ทั้งหมดหรือไม่ </span>
                            </div>
                        </div>
                        <div class="row col-12" style="display: none;">
                            <input class="col-4" type="hidden" id="hn_closeallpt">
                            <input class="col-4" type="hidden" id="admin_login_uid_closeallpt">
                            <input class="col-4" type="hidden" id="status_type_closeallpt">
                        </div>
                        <div class="row">
                            <div class="col" style="text-align:center;">
                                <button class="btn_sm_blue" id="conf_closeallpt" style="margin-right: 20px;">ตกลง</button>
                                <button class="btn_sm_gray" data-dismiss="modal">ปิด</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>