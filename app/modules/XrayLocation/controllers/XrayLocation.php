<?php
defined('BASEPATH') or exit('No direct script access allowed');

class XrayLocation extends MY_Controller
{



	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('assets');
		$this->load->model('XrayLocation_md');
		$this->load->model('XrayLocation_select_md');
		// $this->load->module('StepProcessing/PersonType');
	}


	public function xraytemplate($page, $title, $script)
	{
		$this->load->module('Template_Module');

		$Template = array(
			'Module' => 'XrayLocation',
			'Site_Title' => $title,
			'Content' => $page,
			'Script' => array(
				'Script' => $script,
			),
			'viewCSS' => array(
				'css' => 'XrayLocation_style',
			),
		);
		$this->template_module->Template('Xray_tem', $Template);
	}

	public function XrayLocation_main()
	{
		$page = array('Main' => 'XrayLocation_vw');
		$title = 'Xray';
		$script = 'XrayLocation_script';
		$this->xraytemplate($page, $title, $script);
	}

	public function search_data()
	{

		$ordernew = ($this->input->post('ordernew') == null) ? '' : $this->input->post('ordernew');

		$result_patient = $this->XrayLocation_md->search_patient(); // ค้นหา patient [จาก db เดิม]
		//echo json_encode($result_patient); die();

		if ($result_patient == false) { //ไม่เจอใน view

			$check_close_all = $this->XrayLocation_md->checkCloseAll();

			$create_new_queue = $this->XrayLocation_md->createNewQueue();

			if (($check_close_all[0]['count_hn'] != 0 && $check_close_all[1]['count_hn'] != 0) && $check_close_all[0]['count_hn'] == $check_close_all[1]['count_hn'] && $check_close_all != false) { // hn เก่า ในระบบ xray เคยปิดไปหมดแล้ว

				$ordernew = 'insert_pt_novw';
			}

			if ($create_new_queue == false) {
				echo json_encode(['log' => false, 'massage' => 'ไม่มีข้อมูลคนไข้ในโรงพยาบาล', 'data' => $result_patient]);
			} else { //ไม่มีในวัน แต่เจอใน pt information

				$result_order = $this->XrayLocation_md->list_xraycategory(); // ดึงรายการ xray category

				if ($result_order == false) {
					echo json_encode(['log' => false, 'massage' => 'ไม่มีข้อมูล XrayCategory', 'data' => $result_order]);
				} else {
					$result_patient_info = [];
					$addcolum_ptinfo = [];
					foreach ($create_new_queue as $value) {
						$rundom_secure_code = $this->RandomString();
						$result_patient_info = $value;
						$result_patient_info['tokenno'] = '';
						$result_patient_info['secure_code'] = $rundom_secure_code;
						$result_patient_info['patient_location_id'] =  $value['location_id'];
					}

					array_push($addcolum_ptinfo, $result_patient_info);
					$this->genOrder($addcolum_ptinfo, $result_order, $ordernew);
				}
			}
		} else {

			$result_order = $this->XrayLocation_md->list_xraycategory(); // ดึงรายการ xray category

			if ($result_order == false) {
				echo json_encode(['log' => false, 'massage' => 'ไม่มีข้อมูล XrayCategory', 'data' => $result_order]);
			} else {

				$check_close_all = $this->XrayLocation_md->checkCloseAll();

				if (($check_close_all[0]['count_hn'] != 0 && $check_close_all[1]['count_hn'] != 0) && $check_close_all[0]['count_hn'] == $check_close_all[1]['count_hn'] && $check_close_all != false && $ordernew == '') { // hn เก่า เคยปิดไปหมดแล้ว // $ordernew == '' จะเกิดได้จากมีการกดขอคิวใหม่ให้ข้ามขั้นตอนนี้ไป

					echo json_encode(['log' => 'ordernewqueue', 'massage' => 'hn เก่าเคยปิดไปหมดแล้ว', 'data' => $result_patient, 'check_close_all' => $check_close_all]);
				} else {
					$this->genOrder($result_patient, $result_order, $ordernew);
				}
			}
		}
	}

	public function genOrder($result_patient, $result_order, $ordernew)
	{
		$insert_PatientDetail = $this->XrayLocation_md->Ins_PatientDetail($result_patient, $result_order, $ordernew); // insert patientdetail , patientxray , processcontrol

		$head_duplicate = '';

		$html_dup = '';
		if (count($insert_PatientDetail['duplication']) > 0) {

			foreach ($insert_PatientDetail['duplication'] as $key => $value) {
				$head_duplicate = $value['hn'];
				$html_dup .= "<tr>";
				$html_dup .= "<td>" . $value['vn'] . "</td>";
				$html_dup .= "</tr>";
			};
		}

		$html_newhn = '';
		if (count($insert_PatientDetail['new_hn']) > 0) {

			foreach ($insert_PatientDetail['new_hn'] as $key => $value) {
				$head_duplicate = $value['hn'];
				$html_newhn .= "<tr>";
				$html_newhn .= "<td>" . $value['vn'] . "</td>";
				$html_newhn .= "</tr>";
			};
		}

		$count_check = '';

		if (count($insert_PatientDetail['all_hn']) > 0) {
			foreach ($insert_PatientDetail['all_hn'] as $key => $value) {

				$result_find = $this->XrayLocation_md->FindOrderHn($value['hn'], $value['vn']);

				if (count($result_find) > 0) {
					$count_check = 'send_empty';
				}
			};
		}

		$html_showprint = '';
		if (count($insert_PatientDetail['patientdetail_new']) > 0) {
			$result_new_pid = [];
			$result_old_pid = [];

			foreach ($insert_PatientDetail['patientdetail_new'] as $key => $value) {
				array_push($result_new_pid, $this->select_patient_new($value['patientdetailuid']));
			}

			foreach ($insert_PatientDetail['duplication'] as $keydup => $valuedup) {
				array_push($result_old_pid, $valuedup['opd_location']);
			}

			//echo json_encode($result_new_pid); die();

			if (count($result_new_pid) > 0) {

				$check_old_location = '';
				$check_location_get = '';

				foreach ($result_old_pid as $keyold => $valueold) {	// เช็คว่า location ที่เคยเพิ่มเข้ามา มี location ใช้ระบบคิวหรือไม่
					if (trim($valueold) == 'C2' || trim($valueold) == 'CWB' || trim($valueold) == 'CDC' || trim($valueold) == 'C9' || trim($valueold) == 'GI' || trim($valueold) == 'C11' || trim($valueold) == 'C6' || trim($valueold) == 'C7' || trim($valueold) == 'C8' || trim($valueold) == 'EYE' || trim($valueold) == 'CHE') {
						$check_old_location = 'use_queue_system';
					}
				};


				if ($check_old_location == '') { // location ก่อนหน้าไม่มีใช้ระบบคิว หรือ ไม่เคยเพิ่มข้อมูลคนไข้เข้ามาในวันนี้
					foreach ($result_new_pid as $key => $value) {
						if (trim($value[0]['opd_location']) == 'C2' || trim($value[0]['opd_location']) == 'CWB' || trim($value[0]['opd_location']) == 'CDC' || trim($value[0]['opd_location']) == 'C9' || trim($value[0]['opd_location']) == 'GI' || trim($value[0]['opd_location']) == 'C11' || trim($value[0]['opd_location']) == 'C6' || trim($value[0]['opd_location']) == 'C7' || trim($value[0]['opd_location']) == 'C8' || trim($value[0]['opd_location']) == 'EYE' || trim($value[0]['opd_location']) == 'CHE') {
							$check_location_get = 'noprint';
						}
					};

					if ($check_location_get == '') {		//ไม่มี vn ไหน ใช้ location ระบบคิว
						foreach ($result_new_pid as $key2 => $value2) {
							$html_showprint .= "<tr>";
							$html_showprint .= "<td>" . $value2[0]['queueno'] . "</td>";
							$html_showprint .= "<td>" . $value2[0]['hn'] . "</td>";
							$html_showprint .= "<td>" . $value2[0]['en'] . "</td>";
							$html_showprint .= "<td>" . $value2[0]['opd_location'] . "</td>";
							$html_showprint .= "<td><button class='none btn_print_outofloc' style='outline: none;' hn='" . $value2[0]['hn'] . "' en='" . $value2[0]['en'] . "' patientdetail_uid='" . $value2[0]['patientdetail_uid'] . "' queueno='" . $value2[0]['queueno'] . "' nation='" . $value2[0]['nation'] . "' secure_code='" . $value2[0]['secure_code'] . "' status_type='order_all'><i class='fa fa-print'></i></button></i></td>";
							$html_showprint .= "</tr>";
						};
					}
				}
			}
		} // if patientdetail_new

		echo json_encode(['log' => true, 'duplication' => $html_dup, 'newhn' => $html_newhn, 'showprint' => $html_showprint, 'showheadduplicate' => $head_duplicate, 'sendempty' => $count_check]);
	}

	public function list_patient()
	{

		$status = $this->input->post('status');

		$data = '';

		if ($status == 'order_all') {								//order all เท่านั่น

			$data = $this->XrayLocation_select_md->type_order_all();

			if ($data == false) {
				echo json_encode($data);
			} else {
				$this->tb_order_all($data, $status);
			}
		} else {													//order อื่นๆที่ไม่ไช่ all 
			$data = $this->XrayLocation_select_md->type_order_other();

			if ($data == false) {
				echo json_encode($data);
			} else {
				$this->tb_order_other($data, $status);
			}
		}

		//echo json_encode($data); die();


	}

	public function list_patient_comp()
	{

		$status = $this->input->post('status');

		$data = '';

		$data = $this->XrayLocation_select_md->type_pt_comp();

		if ($data == false) {
			echo json_encode($data);
		} else {
			$this->tb_order_other_comp($data, $status);
		}
	}

	function duration($begin, $end)
	{
		$remain = intval(strtotime($end) - strtotime($begin));
		$minute = floor($remain / 60);

		return $minute;
	}

	public function tb_order_all($data, $status_type)
	{

		$html = '';
		$class_color = "btn-thg-blue";
		$class_yellow = "btn-thg-yellow";
		$num_row = 0;

		foreach ($data as $key => $value) {

			$print = '';
			$note = '';
			$call_order = ' - ';
			$sex = '-';
			$waitingtime = '';

			if ($value['printcwhen'] != null) {
				$print = $class_yellow;
			} else {
				$print = '';
			}

			if ($value['notedetailcwhen'] != null) {
				$note = $class_color;
			} else {
				$note = '';
			}

			if ($value['callordername'] != null) {

				$call_order = $value['callordername'];
			}

			if ($value['sex'] != null) {
				$sex = $value['sex'];
			}

			$waitingtime = $this->duration($value['cwhen'], date('Y-m-d H:i:s'));

			$get_img_order = $this->XrayLocation_md->FindOrderImg($value['patientdetail_uid']);

			$img_show = '';

			if ($get_img_order != false) {
				$img_show = $this->loopcheck_img($get_img_order);
			}



			$html .= "<tr style='height:50px;'>";
			$html .= "<td>" . ($num_row += 1) . "</td>";
			$html .= "<td>";
			$html .= "<button class='none whbutton icon_td btn_send' style='outline: none;' status_type='" . $status_type . "' uid='" . $value['patientdetail_uid'] . "'><i class='fa fa-paper-plane' aria-hidden='true'></i> </button>";
			$html .= "</td>";
			$html .= "<td>" . $value['queueno'] . "</td>";
			$html .= "<td>" . $value['prename'] . $value['forename'] . ' ' . $value['surname'] . "</td>";
			$html .= "<td>" . $value['hn'] . "</td>";
			$html .= "<td>" . $value['en'] . "</td>";
			$html .= "<td> " . $waitingtime . "</td>";
			$html .= "<td>";
			$html .= "<div>";
			$html .= $img_show;
			$html .= "</div>";
			$html .= "</td>";
			$html .= "<td>" . $call_order . "</td>";
			$html .= "<td>";
			$html .= "<button class='none whbutton icon_td btn_print " . $print . "' style='outline: none;' status_type='" . $status_type . "' patientdetail_uid='" . $value['patientdetail_uid'] . "' secure_code='" . $value['secure_code'] . "'><i class='fa fa-print'></i></button>";
			$html .= "</td>";
			$html .= "<td>";
			$html .= "<button class='none whbutton icon_td btn_note " . $note . "' style='outline: none;' status_type='" . $status_type . "' patientdetail_uid='" . $value['patientdetail_uid'] . "'><i class='fas fa-edit'></i></button>";
			$html .= "</td>";
			$html .= "<td>";
			$html .= "<button class='none whbutton icon_x btn_closeq' style='outline: none;' status_type='" . $status_type . "' patientdetail_uid='" . $value['patientdetail_uid'] . "' queueno='" . $value['queueno'] . "' hn='" . $value['hn'] . "'><span aria-hidden='true'>&times;</span></button>";
			$html .= "</td>";
			$html .= "<td>";
			$html .= "<button class='closeptall' hn='" . $value['hn'] . "' status_type='" . $status_type . "'><i class='fas fa-ellipsis-h'></i></button>";
			$html .= "</td>";
			$html .= "</tr>";
		}

		$result_final = ['status' => 'order_all', 'data' => $html, 'log' => $data];

		echo json_encode($result_final);
	}

	public function tb_order_other($data, $status_type)
	{
		$html = '';

		$class_color = "btn-thg-blue";
		$class_yellow = "btn-thg-yellow";

		$num_row = 0;
		foreach ($data as $key => $value) {

			$print = '';
			$note = '';
			$hold = '';
			$call = '';
			$sex = '-';
			$waitingtime = '';
			$calllastorder = ' - ';

			if ($value['printcwhen'] != null) {
				$print = $class_yellow;
			} else {
				$print = '';
			}

			if ($value['notedetailcwhenorderall'] != null) {
				$note = $class_color;
			} else {
				$note = '';
			}

			if ($value['messagedetailcwhen'] != null) {
				$hold = $class_yellow;
			} else {
				$hold = '';
			}

			if ($value['callqueuecwhen'] != null) {
				$call = $class_yellow;
			} else {
				$call = '';
			}

			if ($value['sex'] != null) {
				$sex = $value['sex'];
			}

			if ($value['lastcallorder'] != null) {
				$calllastorder = $value['lastcallorder'];
			}

			$waitingtime = $this->duration($value['patientxraysendcwhen'], date('Y-m-d H:i:s'));


			$get_img_order = $this->XrayLocation_md->FindOrderImg($value['patientdetail_uid']);

			$img_show = '';

			if ($get_img_order != false) {
				$img_show = $this->loopcheck_img($get_img_order);
			}



			if ($value['worklistuid9'] == null && $value['worklistuid6'] == null) {

				$html .= "<tr style='height:50px;'>";
				$html .= "<td>" . ($num_row += 1) . "</td>";
				$html .= "<td>";
				$html .= "<button class='none whbutton icon_td btn_counter " . $call . "' style='outline: none;' status_type='" . $status_type . "' patientdetail_uid='" . $value['patientdetail_uid'] . "' patientxray_uid='" . $value['patientxray_uid'] . "' queueno='" . $value['queueno'] . "' nation='" . $value['nation'] . "' xraycategoryuid_px='" . $value['xraycategoryuid_px'] . "'><i class='fas fa-volume-up'></i> </button>";
				$html .= "</td>";
				$html .= "<td>" . $value['queueno'] . "</td>";
				$html .= "<td>" . $value['prename'] . $value['forename'] . ' ' . $value['surname'] . "</td>";
				$html .= "<td>" . $value['hn'] . "</td>";
				$html .= "<td>" . $waitingtime . "</td>";
				$html .= "<td> " . $calllastorder . " </td>";
				$html .= "<td>";
				$html .= "<button class='none whbutton icon_td btn_hold " . $hold . "' style='outline: none;' status_type='" . $status_type . "' patientdetail_uid='" . $value['patientdetail_uid'] . "' patientxray_uid='" . $value['patientxray_uid'] . "'><i class='fa fa-ban' ></i></button>";
				$html .= "</td>";
				$html .= "<td>";
				$html .= "<button class='none whbutton icon_td btn_note " . $note . "' style='outline: none;' status_type='" . $status_type . "' patientdetail_uid='" . $value['patientdetail_uid'] . "' patientxray_uid='" . $value['patientxray_uid'] . "'><i class='fa fa-edit'></i></button>";
				$html .= "</td>";
				$html .= "<td>";
				$html .= "<button class='none whbutton icon_x active btn_closeq' style='outline: none;' status_type='" . $status_type . "' patientdetail_uid='" . $value['patientdetail_uid'] . "' patientxray_uid='" . $value['patientxray_uid'] . "' queueno='" . $value['queueno'] . "' hn='" . $value['hn'] . "'><span aria-hidden='true'>&times;</span></button>";
				$html .= "</td>";
				$html .= "</tr>";
			}
		}

		$result_final = ['status' => 'other', 'data' => $html, 'log' => $data];

		echo json_encode($result_final);
	}


	public function tb_order_other_comp($data, $status_type)
	{
		$html = '';

		$class_color = "btn-thg-blue";
		$class_yellow = "btn-thg-yellow";

		$num_row = 0;
		foreach ($data as $key => $value) {

			$note = '';

			if ($value['notedetailcwhen'] != null) {
				$note = $class_color;
			} else {
				$note = '';
			}



			$html .= "<tr style='height:50px;'>";
			$html .= "<td>" . ($num_row += 1) . "</td>";
			$html .= "<td>" . $value['queueno'] . "</td>";
			$html .= "<td>" . $value['prename'] . $value['forename'] . ' ' . $value['surname'] . "</td>";
			$html .= "<td>" . $value['hn'] . "</td>";
			$html .= "<td>";
			$html .= "<button class='none whbutton icon_td btn_note " . $note . "' style='outline: none;' status_type='" . $status_type . "' patientdetail_uid='" . $value['patientdetail_uid'] . "' patientxray_uid='" . $value['patientxray_uid'] . "'><i class='fa fa-edit'></i></button>";
			$html .= "</td>";
			$html .= "</tr>";
		}

		$result_final = ['status' => 'other', 'data' => $html, 'log' => $data];

		echo json_encode($result_final);
	}


	public function list_order()
	{

		$result = $this->XrayLocation_md->list_xraycategory();

		$result2 = $this->XrayLocation_md->list_xraycategory_all();

		$html = '';

		$user_roleuid = $this->input->post('user_roleuid');

		if ($user_roleuid == '1') {
			foreach ($result2 as $key => $value2) {
				$html .= "<div class='center card_ctn'>";
				$html .= "<button class='center card text-center  none btn_pr btn_order' type='button' value='order_all' active=''>";
				$html .= "<div class='center card_h'> All </div>";
				$html .= "<div class='center card_bd'>";
				$html .= single_img('img/icon/iconall_00.png');
				$html .= "</div>";
				$html .= "<div class='text-muted center card_f'>";
				$html .= "<span class='num_pn center'> " . $value2['countcateall'] . " </span>";
				$html .= "</div>";
				$html .= "</button>";
				$html .= "</div>";
			}
		}
		foreach ($result as $key => $value) {
			$html .= "<div class='center card_ctn'>";
			$html .= "<button class='center card text-center  none btn_pr btn_order' type='button' value='" . $value['uid'] . "' active=''>";
			$html .= "<div class='center card_h'> " . $value['name'] . " </div>";
			$html .= "<div class='center card_bd'>";
			$html .= single_img('img/icon/' . $value['icon']);
			$html .= "</div>";
			$html .= "<div class='text-muted center card_f'>";
			$html .= "<span class='num_pn center'> " . $value['patientxraycount'] . " </span>";
			$html .= "</div>";
			$html .= "</button>";
			$html .= "</div>";
		}
		echo json_encode($html);
	}


	public function getorder_send()
	{

		$result = $this->XrayLocation_select_md->list_xraycategory_send();

		$html = '';

		$css = '';

		//echo json_encode($result); die();

		foreach ($result as $key => $value) {
			if ($value['worklistuid'] != '3') {
				if ($value['senddttm'] != null) {
					$css = 'opacity:0.2;pointer-events: none;';
				} else {
					$css = '';
				}
			}
			$html .= "<div class='col-4 center' style='padding: .6rem;'>";
			$html .= "<button class='center card text-center  none select_order_send' active='' type='button' value='" . $value['xraycategoryuid'] . "' patientxrayuid='" . $value['patientxrayuid'] . "' style='" . $css . "'>";
			$html .= single_img('img/icon/' . $value['icon']);
			$html .= "</button>";
			$html .= "</div>";
		}
		echo json_encode($html);
	}


	public function update_ptxray()
	{

		$result = $this->XrayLocation_md->UpdatePtXray();

		echo json_encode($result);
	}


	public function select_counter_call()
	{

		$result = $this->XrayLocation_select_md->SelectCounterCall();

		$html = "";

		if ($result != false) {
			foreach ($result as $key => $value) {
				$html .= "<button class='btn_inct call_counter' roomno='" . $value['roomno'] . "' room_uid='" . $value['uid'] . "' locationuid='" . $value['locationuid'] . "'>" . $value['name'] . "</button>";
			}
		}


		echo json_encode(['log' => $result, 'data' => $html]);
	}

	public function add_callqueue()
	{

		$result = $this->XrayLocation_md->AddCallQueue();

		echo json_encode($result);
	}

	public function select_list_hold()
	{
		$result = $this->XrayLocation_select_md->SelectListHold();
		$gethold = $this->XrayLocation_md->GetHold();

		$selected = '';
		$messageuid = '';
		$holdtext = '';

		$html = '';
		if ($result != false) {
			foreach ($result as $key => $value) {
				$messageuid = $value['uid'];

				if ($gethold != false) {
					foreach ($gethold as $key => $value2) {
						if ($messageuid == $value2['messageuid']) {
							$selected = 'selected';
							$holdtext = $value2['remake'];
						}
					}
				}


				$html .= " <option message_code='" . $value['code'] . "' groupprocessuid='" . $value['groupprocessuid'] . "' message_uid='" . $value['uid'] . "'" . $selected . ">" . $value['description'] . "</option>";
			}
		}

		echo json_encode(['log' => $result, 'data' => $html, 'holdtext' => $holdtext]);
	}

	public function add_hold()
	{
		$result = $this->XrayLocation_md->AddHold();

		echo json_encode($result);
	}

	public function add_note()
	{

		$result = $this->XrayLocation_md->AddNote();

		echo json_encode($result);
	}

	public function select_list_print()
	{
		$check_status_type = $this->input->post('check_status_type');

		if ($check_status_type == 'order_all') {
			$result = $this->XrayLocation_select_md->SelectListPrintOrderAll();
		} else {
			$result = $this->XrayLocation_select_md->SelectListPrint();
		}

		$html = '';
		foreach ($result as $key => $value) {

			$html .= "<tr>";
			$html .= "<td>" . $value['queueno'] . "</td>";
			$html .= "<td>" . $value['hn'] . "</td>";
			$html .= "<td>" . $value['en'] . "</td>";
			$html .= "<td>" . $value['opd_location'] . "</td>";
			$html .= "<td><button class='none click_print' style='outline: none;' hn='" . $value['hn'] . "' en='" . $value['en'] . "' patientdetailuid='" . $value['patientdetail_uid'] . "' queueno='" . $value['queueno'] . "' nation='" . $value['nation'] . "'><i class='fa fa-print'></i></button></i></td>";
			$html .= "</tr>";
		}

		echo json_encode(['log' => $result, 'data' => $html]);
	}

	public function add_print()
	{
		$result = $this->XrayLocation_md->AddPrint();

		echo json_encode($result);
	}

	public function update_complete()
	{
		$result = $this->XrayLocation_md->UpdateComplete();

		$patientdetail_uid = $this->input->post('patientdetail_uid');
		$locationuid_login = $this->input->post('locationuid_login');
		$admin_login_uid = $this->input->post('admin_login_uid');
		$hn = $this->input->post('hn');

		$close_patient_all = $this->close_patient_all($patientdetail_uid, $locationuid_login, $admin_login_uid, $hn);

		echo json_encode($close_patient_all);
	}

	public function update_closeq()
	{
		$result = $this->XrayLocation_md->UpdateCloseq();

		$patientdetail_uid = $this->input->post('patientdetail_uid');
		$locationuid_login = $this->input->post('locationuid_login');
		$admin_login_uid = $this->input->post('admin_login_uid');
		$hn = $this->input->post('hn');

		$close_patient_all = $this->close_patient_all($patientdetail_uid, $locationuid_login, $admin_login_uid, $hn);

		echo json_encode($close_patient_all);
	}

	function close_patient_all($patientdetail_uid, $locationuid_login, $admin_login_uid, $hn = null)
	{

		$checkcloseall = $this->XrayLocation_md->CheckCloseAllHn($patientdetail_uid);
		$patientxrayinfo = $this->XrayLocation_md->SelectPatientXrayInfo($patientdetail_uid);

		$count_finish = 0;

		if ($checkcloseall != false) {
			foreach ($checkcloseall as $key => $value) {
				if ($value['xraydischarge'] != null || $value['worklistuidthreeorsix'] != null) { // เคยปิด หรือ ตรวจเสร็จ
					$count_finish++;
				}
			}
			//return json_encode(count($checkcloseall)  . ' = ' . $count_finish);


			if (count($checkcloseall) == $count_finish) { //ทุกorder ตรวจเสร็จ หรือ ปิดคิวหมด

				$close_all_hn = $this->XrayLocation_md->CloseAllHn($patientdetail_uid, $locationuid_login, $admin_login_uid, $patientxrayinfo);

				$result_gethn = $this->XrayLocation_md->CloseAllPt($admin_login_uid, $hn);

				if (count($result_gethn) > 0) {
					$ins_closeall = $this->XrayLocation_md->InsCloseAll($result_gethn);
				}
			}

			return json_encode(count($checkcloseall)  . ' = ' . $count_finish . ' ++ ' . $hn);
		} else {
			return json_encode('0 = ' . $count_finish . ' ++ ' . $hn);
		}
	}

	public function getnote()
	{
		$result = $this->XrayLocation_md->GetNote();

		echo json_encode(['status' => true, 'data' => $result]);
	}

	public function cleatsession()
	{
		$this->session->unset_userdata('userlogin');

		return true;
	}

	public function loopcheck_img($get_img_order)
	{
		$img_show = '';

		foreach ($get_img_order as $key => $value) {
			if ($value['worklistuidlast'] == '1') {
				$img_show .=  single_img('img/icon/' . $value['icon'], array('class' => 'iconsize_tr'));
			} else if ($value['worklistuidlast'] == '2') {
				$img_show .=  single_img('img/icon/' . $value['iconsend'], array('class' => 'iconsize_tr'));
			} else if ($value['worklistuidlast'] == '3') {
				$img_show .=  single_img('img/icon/' . $value['iconclose'], array('class' => 'iconsize_tr'));
			} else if ($value['worklistuidlast'] == '6') {
				$img_show .=  single_img('img/icon/' . $value['iconcomplete'], array('class' => 'iconsize_tr'));
			} else if ($value['worklistuidlast'] == '9') {
				$img_show .=  single_img('img/icon/' . $value['iconclose'], array('class' => 'iconsize_tr'));
			} else if ($value['worklistuidlast'] == '10') {
				$img_show .=  single_img('img/icon/' . $value['iconresend'], array('class' => 'iconsize_tr'));
			}
		}

		return $img_show;
	}

	public function select_patient_new($data)
	{
		$result = $this->XrayLocation_md->SelectPatientNew($data);

		return $result;
	}

	public function closeallpt()
	{
		$result_gethn = $this->XrayLocation_md->CloseAllPt();


		if (count($result_gethn) > 0) {
			$ins_closeall = $this->XrayLocation_md->InsCloseAll($result_gethn);
		}

		echo json_encode(['status' => true, 'data' => $result_gethn]);
	}

	public function RandomString($length = 8)
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
}
