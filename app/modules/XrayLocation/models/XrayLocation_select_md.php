<?php
defined('BASEPATH') or exit('No direct script access allowed');

class XrayLocation_select_md extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        //$this->load->database('database');

        $this->db_xray = $this->load->database('db_xray', TRUE);
    }



    public function type_order_all()
    {
        $this->db_xray->select('*')
            ->from('vw_patientdetail_today');

        $query = $this->db_xray->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function type_order_other()
    {
        $xraycategoryuid = $this->input->post('status');
        $locationuser = $this->input->post('locationuser');

        $this->db_xray->select('*,patientxrayuid as patientxray_uid ,  patientdetailuid as  patientdetail_uid ')
            ->from('vw_patientxray_today')
            ->where('xraycategoryuid_px', $xraycategoryuid)
            ->where('xray_location', $locationuser)
            ->where('sendstatus_px is not null')
            ->order_by('patientxraysendcwhen', 'asc');

        $query = $this->db_xray->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }


    public function type_pt_comp()
    {
        $xraycategoryuid = $this->input->post('status');
        $locationuser = $this->input->post('locationuser');

        $this->db_xray->select('*,patientxrayuid as patientxray_uid,uid as patientdetail_uid')
            ->from('vw_patient_close_today')
            ->where('xray_location', $locationuser)
            ->order_by('closetime', 'asc');

        $query = $this->db_xray->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }


    public function list_xraycategory_send()
    {
        $patiendetailuid = $this->input->post('patiendetailuid');

        $this->db_xray->select('*')
            ->from('vw_ordersend')
            ->where('patientdetailuid_px', $patiendetailuid);

        $query = $this->db_xray->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function SelectCounterCall()
    {

        $xraycatergoryuid = $this->input->post('status_type');

        $this->db_xray->select('*')
            ->from('room')
            ->where('xraycatergoryuid', $xraycatergoryuid)
            ->where('statusflag', 'Y')
            ->order_by('sq', 'ASC');

        $query = $this->db_xray->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function SelectListHold()
    {
        $this->db_xray->select('*')
            ->from('message')
            ->where('active', 'Y');

        $query = $this->db_xray->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function SelectListPrint()
    {

        $patientxray_uid = $this->input->post('patientxray_uid');

        $this->db_xray->select('*,patientxray.uid as patientxray_uid , patientdetail.uid as patientdetail_uid , patientdetail.nation as nation , patientdetail.opd_location as opd_location')
            ->from('patientxray')
            ->join('patientdetail', 'patientxray.patientdetailuid = patientdetail.uid', 'left')
            ->where('patientxray.uid', $patientxray_uid);

        $query = $this->db_xray->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function SelectListPrintOrderAll()
    {

        $patientdetail_uid = $this->input->post('patientdetail_uid');

        $this->db_xray->select('*,patientdetail.uid as patientdetail_uid')
            ->from('patientdetail')
            ->where('patientdetail.uid', $patientdetail_uid);

        $query = $this->db_xray->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
}
