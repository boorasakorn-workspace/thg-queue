<?php
defined('BASEPATH') or exit('No direct script access allowed');

class XrayLocation_md extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        //$this->load->database('database');

        $this->db_xray = $this->load->database('db_xray', TRUE);
    }



    public function search_patient()
    {

        $hn = $this->input->post('hn');

        $query = "select hn,vn,title_th,forename_th,surname_th,nation,tokenno,secure_code,patient_location_id from vw_getscanqueue
                where replace(hn,'-','') like replace('$hn','-','') 
                ";
        $code = $this->db->query($query)->result_array();

        if ($code) {
            return $code;
        } else {
            return false;
        }
    }


    public function list_xraycategory()
    {
        $this->db_xray->select('*')
            ->from('vw_xraycategory_today')
            ->where('statusflag', 'Y')
            ->order_by('sq', 'ASC');

        $query = $this->db_xray->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function list_xraycategory_all()
    {
        $this->db_xray->select('count(patientdetail_uid) as countcateall')
            ->from('vw_patientdetail_today')
            ->where('statusflag', 'Y')
            ->where('xraydischarge is null');

        $query = $this->db_xray->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }


    public function Ins_PatientDetail($result_patient, $result_order, $ordernew, $xray_location_id = null)
    {

        $locationuid_login = $this->input->post('locationuid_login');
        $admin_login_uid = $this->input->post('admin_login_uid');

        $duplicate = [];
        $new_hn = [];
        $patient_detail_all = [];
        $all_hn = [];

        if ($ordernew != '' && $ordernew != 'insert_pt_novw') {

            foreach ($result_patient as $key => $value) {

                $pt_hn = $value['hn'];
                $pt_vn = $value['vn'];

                $data = array(
                    'hn' => $value['hn'],
                    'en' => $value['vn'],
                    // 'idcard' => $value[''],
                    'prename' => $value['title_th'],
                    'forename' => $value['forename_th'],
                    'surname' => $value['surname_th'],
                    // 'sex' => $value[''],
                    // 'locationuid' => $value[''],
                    'nation' => $value['nation'],
                    // 'queuelocationuid' => $value[''],
                    'cuser' => $admin_login_uid,
                    'cwhen' => date("Y-m-d H:i:s"),
                    'muser' => $admin_login_uid,
                    'mwhen' => date("Y-m-d H:i:s"),
                    'queueno' => $value['tokenno'],
                    'statusflag' => 'Y',
                    'secure_code' => $value['secure_code'],
                    'opd_location' => $value['patient_location_id'],
                );

                $this->db_xray->insert('patientdetail', $data); // insert ลง patientdetail

                array_push($new_hn, ['hn' => $value['hn'], 'vn' => $value['vn']]);
                array_push($all_hn, ['hn' => $value['hn'], 'vn' => $value['vn']]);

                $result_patient_uid = $this->db_xray->insert_id();    // ดึง pk ที่ insert ได้มาใช้ต่อ

                array_push($patient_detail_all, ['patientdetailuid' => $result_patient_uid]);

                foreach ($result_order as $key => $value) {     //insert order ทั้งหมด
                    $data2 = array(
                        'patientdetailuid' => $result_patient_uid,
                        'xraycategoryuid' => $value['uid'],
                        'statusflag' => 'Y',
                        'cuser' => $admin_login_uid,
                        'cwhen' => date("Y-m-d H:i:s"),
                        'muser' => $admin_login_uid,
                        'mwhen' => date("Y-m-d H:i:s"),
                    );

                    $this->db_xray->insert('patientxray', $data2);
                    $result_patientxray_uid = $this->db_xray->insert_id();

                    $data3 = array(
                        'patientdetailuid' => $result_patient_uid,
                        'patientxrayuid' => $result_patientxray_uid,
                        'worklistuid' => '1',
                        'createdate' => date("Y-m-d H:i:s"),
                        'cuser' => $admin_login_uid,
                        'cwhen' => date("Y-m-d H:i:s")
                    );
                    $this->db_xray->insert('processcontrol', $data3);
                }


                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, APIAUTOPT . '/api/v1/groupxray?HN=' . $pt_hn . '&VN=' . $pt_vn . '&date=' . date('Ymd', strtotime('+543 years')));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $output = curl_exec($ch);
                curl_close($ch);
            } //foreach ใหญ่
        } else {

            foreach ($result_patient as $key => $value) {

                $pt_hn = $value['hn'];
                $pt_vn = $value['vn'];

                if ($ordernew != 'insert_pt_novw') { // pt ไม่มีใน vw และปิดไปหมดแล้ว มา scanซ็ำ ให้ข้ามขั้นตอน check นี้ไป
                    if ($value['tokenno'] == '') {
                        $check = $this->db_xray->select('*')
                            ->from('patientdetail')
                            ->where('hn', $value['hn'])
                            ->where('en', '-')
                            ->get();
                        $checkrow = $check->num_rows();
                    } else {
                        $check = $this->db_xray->select('*')
                            ->from('patientdetail')
                            ->where('hn', $value['hn'])
                            ->where('en', $value['vn'])
                            ->where('queueno', $value['tokenno'])
                            ->get();
                        $checkrow = $check->num_rows();
                    }
                } else {
                    $checkrow = 0;
                }

                if ($checkrow > 0) {
                    $result = $check->result_array();
                    array_push($duplicate, ['hn' => $result[0]['hn'], 'vn' => $result[0]['en'], 'opd_location' => $result[0]['opd_location']]);
                    array_push($all_hn, ['hn' => $result[0]['hn'], 'vn' => $result[0]['en']]);
                    //return $duplicate;
                    //return false;   //ข้อมูลมีอยู่แล้ว ไม่ insert
                } else {          // ไม่เคยมีใน patientdetail ให้ insert

                    if ($value['tokenno'] == '') { // scan แล้วไม่เจอใน vw scan ไม่มีคนไข้นี้ในวันปุจจุบัน
                        $value['tokenno'] = $this->xrayGenQueue($admin_login_uid); //gen queue ใหม่
                        $value['vn'] = '-';
                    }

                    $data = array(
                        'hn' => $value['hn'],
                        'en' => $value['vn'],
                        // 'idcard' => $value[''],
                        'prename' => $value['title_th'],
                        'forename' => $value['forename_th'],
                        'surname' => $value['surname_th'],
                        // 'sex' => $value[''],
                        // 'locationuid' => $value[''],
                        'nation' => $value['nation'],
                        // 'queuelocationuid' => $value[''],
                        'cuser' => $admin_login_uid,
                        'cwhen' => date("Y-m-d H:i:s"),
                        'muser' => $admin_login_uid,
                        'mwhen' => date("Y-m-d H:i:s"),
                        'queueno' => $value['tokenno'],
                        'statusflag' => 'Y',
                        'secure_code' => $value['secure_code'],
                        'opd_location' => $value['patient_location_id'],
                        'xray_location' => $xray_location_id
                    );

                    $this->db_xray->insert('patientdetail', $data); // insert ลง patientdetail

                    array_push($new_hn, ['hn' => $value['hn'], 'vn' => $value['vn']]);
                    array_push($all_hn, ['hn' => $value['hn'], 'vn' => $value['vn']]);

                    $result_patient_uid = $this->db_xray->insert_id();    // ดึง pk ที่ insert ได้มาใช้ต่อ

                    array_push($patient_detail_all, ['patientdetailuid' => $result_patient_uid]);

                    foreach ($result_order as $key => $value) {     //insert order ทั้งหมด
                        $data2 = array(
                            'patientdetailuid' => $result_patient_uid,
                            'xraycategoryuid' => $value['uid'],
                            'statusflag' => 'Y',
                            'cuser' => $admin_login_uid,
                            'cwhen' => date("Y-m-d H:i:s"),
                            'muser' => $admin_login_uid,
                            'mwhen' => date("Y-m-d H:i:s"),
                        );

                        $this->db_xray->insert('patientxray', $data2);
                        $result_patientxray_uid = $this->db_xray->insert_id();

                        $data3 = array(
                            'patientdetailuid' => $result_patient_uid,
                            'patientxrayuid' => $result_patientxray_uid,
                            'worklistuid' => '1',
                            'createdate' => date("Y-m-d H:i:s"),
                            'cuser' => $admin_login_uid,
                            'cwhen' => date("Y-m-d H:i:s")
                        );
                        $this->db_xray->insert('processcontrol', $data3);
                    }
                } // check num rows

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, APIAUTOPT . '/api/v1/groupxray?HN=' . $pt_hn . '&VN=' . $pt_vn . '&date=' . date('Ymd', strtotime('+543 years')));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $output = curl_exec($ch);
                curl_close($ch);
            } //foreach ใหญ่
        }

        if ($this->db_xray->affected_rows()) {

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, APITRIGGER . "/trigger_xray?location_id=" . $locationuid_login);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec($ch);
            curl_close($ch);

            return ['status' => true, 'duplication' => $duplicate, 'new_hn' => $new_hn, 'patientdetail_new' => $patient_detail_all, 'all_hn' => $all_hn]; //insert ได้
        } else {
            return false; // insert ไม่ได้
        }
    }

    public function Ins_PatientDetailLocation($result_patient, $result_order, $ordernew, $xray_location_id = null)
    {

        $locationuid_login = $this->input->post('locationuid_login');
        $admin_login_uid = $this->input->post('admin_login_uid');

        $duplicate = [];
        $new_hn = [];
        $patient_detail_all = [];
        $all_hn = [];

        if ($ordernew != '' && $ordernew != 'insert_pt_novw') {

            foreach ($result_patient as $key => $value) {

                $pt_hn = $value['hn'];
                $pt_vn = $value['vn'];

                $data = array(
                    'hn' => $value['hn'],
                    'en' => $value['vn'],
                    // 'idcard' => $value[''],
                    'prename' => $value['title_th'],
                    'forename' => $value['forename_th'],
                    'surname' => $value['surname_th'],
                    // 'sex' => $value[''],
                    // 'locationuid' => $value[''],
                    'nation' => $value['nation'],
                    // 'queuelocationuid' => $value[''],
                    'cuser' => $admin_login_uid,
                    'cwhen' => date("Y-m-d H:i:s"),
                    'muser' => $admin_login_uid,
                    'mwhen' => date("Y-m-d H:i:s"),
                    'queueno' => $value['tokenno'],
                    'statusflag' => 'Y',
                    'secure_code' => $value['secure_code'],
                    'opd_location' => $value['patient_location_id'],
                );

                $this->db_xray->insert('patientdetail', $data); // insert ลง patientdetail

                array_push($new_hn, ['hn' => $value['hn'], 'vn' => $value['vn']]);
                array_push($all_hn, ['hn' => $value['hn'], 'vn' => $value['vn']]);

                $result_patient_uid = $this->db_xray->insert_id();    // ดึง pk ที่ insert ได้มาใช้ต่อ

                array_push($patient_detail_all, ['patientdetailuid' => $result_patient_uid]);

                foreach ($result_order as $key => $value) {     //insert order ทั้งหมด

                    if ($value['uid'] == '1') {
                        $data2 = array(
                            'patientdetailuid' => $result_patient_uid,
                            'xraycategoryuid' => $value['uid'],
                            'statusflag' => 'Y',
                            'cuser' => $admin_login_uid,
                            'cwhen' => date("Y-m-d H:i:s"),
                            'muser' => $admin_login_uid,
                            'mwhen' => date("Y-m-d H:i:s"),
                            'sendstatus' => 'M',
                            'senddttm' => date("Y-m-d H:i:s")
                        );

                        $this->db_xray->insert('patientxray', $data2);
                        $result_patientxray_uid = $this->db_xray->insert_id();

                        $data3 = array(
                            'patientdetailuid' => $result_patient_uid,
                            'patientxrayuid' => $result_patientxray_uid,
                            'worklistuid' => '1',
                            'createdate' => date("Y-m-d H:i:s"),
                            'cuser' => $admin_login_uid,
                            'cwhen' => date("Y-m-d H:i:s")
                        );
                        $this->db_xray->insert('processcontrol', $data3);


                        $data4 = array(
                            'patientdetailuid' => $result_patient_uid,
                            'patientxrayuid' => $result_patientxray_uid,
                            'worklistuid' => '2',
                            'createdate' => date("Y-m-d H:i:s"),
                            'cuser' => $admin_login_uid,
                            'cwhen' => date("Y-m-d H:i:s")
                        );
                        $this->db_xray->insert('processcontrol', $data4);
                    }
                }


                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, APIAUTOPT . '/api/v1/groupxray?HN=' . $pt_hn . '&VN=' . $pt_vn . '&date=' . date('Ymd', strtotime('+543 years')));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $output = curl_exec($ch);
                curl_close($ch);
            } //foreach ใหญ่
        } else {

            foreach ($result_patient as $key => $value) {

                $pt_hn = $value['hn'];
                $pt_vn = $value['vn'];

                if ($ordernew != 'insert_pt_novw') { // pt ไม่มีใน vw และปิดไปหมดแล้ว มา scanซ็ำ ให้ข้ามขั้นตอน check นี้ไป
                    if ($value['tokenno'] == '') {
                        $check = $this->db_xray->select('*')
                            ->from('patientdetail')
                            ->where('hn', $value['hn'])
                            ->where('en', '-')
                            ->get();
                        $checkrow = $check->num_rows();
                    } else {
                        $check = $this->db_xray->select('*')
                            ->from('patientdetail')
                            ->where('hn', $value['hn'])
                            ->where('en', $value['vn'])
                            ->where('queueno', $value['tokenno'])
                            ->get();
                        $checkrow = $check->num_rows();
                    }
                } else {
                    $checkrow = 0;
                }

                if ($checkrow > 0) {
                    $result = $check->result_array();
                    array_push($duplicate, ['hn' => $result[0]['hn'], 'vn' => $result[0]['en'], 'opd_location' => $result[0]['opd_location']]);
                    array_push($all_hn, ['hn' => $result[0]['hn'], 'vn' => $result[0]['en']]);
                    //return $duplicate;
                    //return false;   //ข้อมูลมีอยู่แล้ว ไม่ insert
                } else {          // ไม่เคยมีใน patientdetail ให้ insert

                    if ($value['tokenno'] == '') { // scan แล้วไม่เจอใน vw scan ไม่มีคนไข้นี้ในวันปุจจุบัน
                        $value['tokenno'] = $this->xrayGenQueue($admin_login_uid); //gen queue ใหม่
                        $value['vn'] = '-';
                    }

                    $data = array(
                        'hn' => $value['hn'],
                        'en' => $value['vn'],
                        // 'idcard' => $value[''],
                        'prename' => $value['title_th'],
                        'forename' => $value['forename_th'],
                        'surname' => $value['surname_th'],
                        // 'sex' => $value[''],
                        // 'locationuid' => $value[''],
                        'nation' => $value['nation'],
                        // 'queuelocationuid' => $value[''],
                        'cuser' => $admin_login_uid,
                        'cwhen' => date("Y-m-d H:i:s"),
                        'muser' => $admin_login_uid,
                        'mwhen' => date("Y-m-d H:i:s"),
                        'queueno' => $value['tokenno'],
                        'statusflag' => 'Y',
                        'secure_code' => $value['secure_code'],
                        'opd_location' => $value['patient_location_id'],
                        'xray_location' => $xray_location_id
                    );

                    $this->db_xray->insert('patientdetail', $data); // insert ลง patientdetail

                    array_push($new_hn, ['hn' => $value['hn'], 'vn' => $value['vn']]);
                    array_push($all_hn, ['hn' => $value['hn'], 'vn' => $value['vn']]);

                    $result_patient_uid = $this->db_xray->insert_id();    // ดึง pk ที่ insert ได้มาใช้ต่อ

                    array_push($patient_detail_all, ['patientdetailuid' => $result_patient_uid]);

                    foreach ($result_order as $key => $value) {     //insert order ทั้งหมด
                        if ($value['uid'] == '1') {

                            $data2 = array(
                                'patientdetailuid' => $result_patient_uid,
                                'xraycategoryuid' => $value['uid'],
                                'statusflag' => 'Y',
                                'cuser' => $admin_login_uid,
                                'cwhen' => date("Y-m-d H:i:s"),
                                'muser' => $admin_login_uid,
                                'mwhen' => date("Y-m-d H:i:s"),
                                'sendstatus' => 'M',
                                'senddttm' => date("Y-m-d H:i:s")
                            );

                            $this->db_xray->insert('patientxray', $data2);
                            $result_patientxray_uid = $this->db_xray->insert_id();

                            $data3 = array(
                                'patientdetailuid' => $result_patient_uid,
                                'patientxrayuid' => $result_patientxray_uid,
                                'worklistuid' => '1',
                                'createdate' => date("Y-m-d H:i:s"),
                                'cuser' => $admin_login_uid,
                                'cwhen' => date("Y-m-d H:i:s")
                            );
                            $this->db_xray->insert('processcontrol', $data3);


                            $data4 = array(
                                'patientdetailuid' => $result_patient_uid,
                                'patientxrayuid' => $result_patientxray_uid,
                                'worklistuid' => '2',
                                'createdate' => date("Y-m-d H:i:s"),
                                'cuser' => $admin_login_uid,
                                'cwhen' => date("Y-m-d H:i:s")
                            );
                            $this->db_xray->insert('processcontrol', $data4);
                        }
                    }
                } // check num rows

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, APIAUTOPT . '/api/v1/groupxray?HN=' . $pt_hn . '&VN=' . $pt_vn . '&date=' . date('Ymd', strtotime('+543 years')));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $output = curl_exec($ch);
                curl_close($ch);
            } //foreach ใหญ่
        }

        if ($this->db_xray->affected_rows()) {

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, APITRIGGER . "/trigger_xray?location_id=" . $locationuid_login);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec($ch);
            curl_close($ch);

            return ['status' => true, 'duplication' => $duplicate, 'new_hn' => $new_hn, 'patientdetail_new' => $patient_detail_all, 'all_hn' => $all_hn]; //insert ได้
        } else {
            return false; // insert ไม่ได้
        }
    }

    public function UpdatePtXray()
    {

        $patientdetail = $this->input->post('patientdetail');
        $status_type = $this->input->post('status_type');
        $xray_category_sel = $this->input->post('xray_category_sel');
        $location_now = $this->input->post('location_now_send');
        $patientxrayuid = $this->input->post('patientxrayuid');
        $locationuid_login = $this->input->post('locationuid_login');

        $count_loop = 0;
        foreach ($xray_category_sel as $key => $value) {

            $check_cancle = $this->db_xray->select('*')
                ->from('processcontrol pcc')
                ->join('patientxray px', 'pcc.patientxrayuid = px.uid', 'left')
                ->where('pcc.patientdetailuid', $patientdetail)
                ->where('pcc.patientxrayuid', $patientxrayuid[($count_loop)])
                ->where('px.xraycategoryuid', $value)
                ->where('pcc.worklistuid', '3')->get()->result_array();

            $worklist_result_check = '2';
            if (count($check_cancle) > 0) {
                $updatedata = array(
                    'xraydischarge' => null,
                    'xraydischargedate' => null
                );
                $this->db_xray->where('uid', $patientxrayuid[($count_loop)])
                    ->update('patientxray', $updatedata);
                $worklist_result_check = '10';
            }

            $count_loop += 1;

            $data = array(
                'sendstatus' => 'M',
                'senddttm' => date("Y-m-d H:i:s")
            );

            $this->db_xray->where('xraycategoryuid', $value)
                ->where('patientdetailuid', $patientdetail)
                ->update('patientxray', $data);

            $patientxray_uid = $this->db_xray->select('uid')
                ->from('patientxray')
                ->where('xraycategoryuid', $value)
                ->where('patientdetailuid', $patientdetail)->get()->result_array();



            $data_pcc = array(
                'patientdetailuid' => $patientdetail,
                'patientxrayuid' => $patientxray_uid[0]['uid'],
                'worklistuid' => $worklist_result_check,
                'createdate' => date("Y-m-d H:i:s"),
                'cuser' => $location_now,
                'cwhen' => date("Y-m-d H:i:s")
            );

            $insert_pcc = $this->db_xray->insert('processcontrol', $data_pcc);
        }

        if ($this->db_xray->affected_rows()) {

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, APITRIGGER . "/trigger_xray?location_id=" . $locationuid_login);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec($ch);
            curl_close($ch);

            return true;
        } else {
            return false;
        }
    }

    public function AddCallQueue()
    {
        $roomno = $this->input->post('roomno');
        $room_uid = $this->input->post('room_uid');
        $locationuid = $this->input->post('locationuid');
        $order_location_uid = $this->input->post('order_location_uid');
        $patientdetail_uid = $this->input->post('patientdetail_uid');
        $patientxray_uid = $this->input->post('patientxray_uid');
        $status_type = $this->input->post('status_type');
        $location_now_counter = $this->input->post('location_now_counter');
        $nation_get = $this->input->post('nation_counter');
        $queueno_get = $this->input->post('queueno_counter');
        $xraycategoryuid = $this->input->post('xraycategoryuid_counter');
        $locationuid_login = $this->input->post('locationuid_login');
        $sublocationuid_login = $this->input->post('sublocationuid_login');



        $nation = '';
        $call_queue = '';

        $queue = str_split($queueno_get, 1);
        $queue_num = implode('|', $queue);
        $roomno_sound = $this->convert_room_id($roomno);


        if ($nation_get == 'ไทย' || $nation_get == '' || $nation_get == null || $nation_get == 'ไม่ระบุ') {
            $roomname_sound = $this->convert_room_name($xraycategoryuid);
            $nation = 'TH';
            $call_queue = $nation . '|เชิญหมายเลข|' . $queue_num . '|' . $roomname_sound . '|' . $roomno_sound . '|ค่ะ';
        } else {
            $roomname_sound = $this->convert_room_name_en($xraycategoryuid);
            $nation = 'EN';
            $call_queue = $nation . '|number|' . $queue_num . '|' . $roomname_sound . '|' . $roomno_sound;
        }

        $find_call = $this->db_xray->select('*')
            ->from('callqueue')
            ->where('patientxrayuid', intval($patientxray_uid))
            ->where('patientdetailuid', intval($patientdetail_uid))
            ->where('xraycategoryuid', $status_type)
            ->where('roomno', $roomno)
            ->get();

        if ($find_call->num_rows() > 0) {

            $data_update = array(
                'muser' => $location_now_counter,
                'mwhen' => date("Y-m-d H:i:s"),
            );
            $this->db_xray->where('patientxrayuid', intval($patientxray_uid))
                ->where('patientdetailuid', intval($patientdetail_uid))
                ->where('xraycategoryuid', $status_type)
                ->where('roomno', $roomno)
                ->update('callqueue', $data_update);
        } else {

            if ($status_type == 'order_all') {
                $data = array(
                    'patientxrayuid' => intval($patientxray_uid),
                    'roomno' => intval($roomno),
                    'locationuid' => intval($locationuid),
                    'createdate' => date("Y-m-d H:i:s"),
                    'cuser' => $location_now_counter,
                    'cwhen' => date("Y-m-d H:i:s"),
                    'muser' => $location_now_counter,
                    'mwhen' => date("Y-m-d H:i:s"),
                    'patientdetailuid' => intval($patientdetail_uid),
                    'xraycategoryuid' => null
                );
                $this->db_xray->insert('callqueue', $data);
            } else {
                $data = array(
                    'patientxrayuid' => intval($patientxray_uid),
                    'roomno' => intval($roomno),
                    'locationuid' => intval($locationuid),
                    'createdate' => date("Y-m-d H:i:s"),
                    'cuser' => $location_now_counter,
                    'cwhen' => date("Y-m-d H:i:s"),
                    'muser' => $location_now_counter,
                    'mwhen' => date("Y-m-d H:i:s"),
                    'patientdetailuid' => intval($patientdetail_uid),
                    'xraycategoryuid' => $status_type
                );
                $this->db_xray->insert('callqueue', $data);
            }
        }



        $pcc_call = array(
            'patientdetailuid' => intval($patientdetail_uid),
            'patientxrayuid' => intval($patientxray_uid),
            'worklistuid' => '4',
            'createdate' => date("Y-m-d H:i:s"),
            'cuser' => $location_now_counter,
            'cwhen' => date("Y-m-d H:i:s"),
        );
        $this->db_xray->insert('processcontrol', $pcc_call);

        if ($this->db_xray->affected_rows()) {

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, APITRIGGER . "/trigger_xray?location_id=" . $locationuid_login);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec($ch);
            curl_close($ch);

            //trigcall
            $chcall = curl_init();
            curl_setopt($chcall, CURLOPT_URL, APITRIGGER . '/callxray');
            curl_setopt($chcall, CURLOPT_POST, 1);
            curl_setopt(
                $chcall,
                CURLOPT_POSTFIELDS,
                "locationuid=COB&categoryuid=" . $status_type . "&queueno=" . $queueno_get . "&room=" . $roomno . "&queuestring=" . $call_queue
            );
            curl_setopt($chcall, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($chcall);
            curl_close($chcall);
            //trigcall

            return true;
        } else {
            return false;
        }
    }

    public function convert_room_name($xraycategoryuid)
    {

        switch ($xraycategoryuid) {
            case 1:
                return 'ที่ห้องเอกซเรย์'; //xray

            case 2:
                return 'ที่ห้องซีทีแสกน'; //ct

            case 3:
                return 'ที่ห้องอัลตร้าซาวด์'; //us

            case 4:
                return 'ที่ห้องตรวจความหนาแน่นของมวลกระดูก'; //bonedent

            case 5:
                return 'ที่ห้องอัลตร้าซาวด์และเอกซเรย์เต้านม'; //mam

            case 6:
                return 'ที่ห้องเอกซเรย์'; //spa
        }
    }

    public function convert_room_name_en($xraycategoryuid)
    {

        switch ($xraycategoryuid) {
            case 1:
                return 'xrayroom'; //xray

            case 2:
                return 'ctscanroom'; //ct

            case 3:
                return 'ultrasoundroom'; //us

            case 4:
                return 'bonedensityroom'; //bonedent

            case 5:
                return 'mamogramroom'; //mam

            case 6:
                return 'xrayroom'; //spa
        }
    }




    public function convert_room_id($room)
    {
        $length = strlen($room);
        if ($length == '3') {
            return $room[0] . '|' . $room[1] . '|' . $room[2];
        } else {
            return $room;
        }
    }


    public function AddHold()
    {

        $groupprocessuid = $this->input->post('groupprocessuid');
        $patientdetail_uid = $this->input->post('patientdetail_uid');
        $patientxray_uid = $this->input->post('patientxray_uid');
        $text_area_hold = $this->input->post('text_area_hold');
        $message_uid = $this->input->post('message_uid');
        $check_status_type = $this->input->post('check_status_type');
        $location_now_hold = $this->input->post('location_now_hold');
        $locationuid_login = $this->input->post('locationuid_login');
        $locationuser = $this->input->post('locationuser');
        

        $fild_Hold = $this->db_xray->select('*')
            ->from('messagedetail')
            ->where('patientdetailuid', $patientdetail_uid)
            ->where('patientxrayuid', $patientxray_uid)
            ->where('xraycategoryuid', $check_status_type)
            ->get();

        if ($fild_Hold->num_rows() > 0) {

            $hold_update = array(
                'groupprocessuid' => $groupprocessuid,
                'messageuid' => $message_uid,
                'remake' => $text_area_hold,
                'muser' => $location_now_hold,
                'mwhen' => date("Y-m-d H:i:s")
            );

            $this->db_xray->where('patientdetailuid', $patientdetail_uid)
                ->where('patientxrayuid', $patientxray_uid)
                ->update('messagedetail', $hold_update);
        } else {


            if ($check_status_type == 'order_all') {
                $data = array(
                    'patientdetailuid' => $patientdetail_uid,
                    'patientxrayuid' => $patientxray_uid,
                    'groupprocessuid' => $groupprocessuid,
                    'messageuid' => $message_uid,
                    'remake' => $text_area_hold,
                    'cuser' => $location_now_hold,
                    'cwhen' => date("Y-m-d H:i:s"),
                    'muser' => $location_now_hold,
                    'mwhen' => date("Y-m-d H:i:s"),
                    'xraycategoryuid' => null
                );
                $this->db_xray->insert('messagedetail', $data);
            } else {
                $data = array(
                    'patientdetailuid' => $patientdetail_uid,
                    'patientxrayuid' => $patientxray_uid,
                    'groupprocessuid' => $groupprocessuid,
                    'messageuid' => $message_uid,
                    'remake' => $text_area_hold,
                    'cuser' => $location_now_hold,
                    'cwhen' => date("Y-m-d H:i:s"),
                    'muser' => $location_now_hold,
                    'mwhen' => date("Y-m-d H:i:s"),
                    'xraycategoryuid' => $check_status_type

                );
                $this->db_xray->insert('messagedetail', $data);
            }
        }




        $pcc_hold = array(
            'patientdetailuid' => intval($patientdetail_uid),
            'patientxrayuid' => intval($patientxray_uid),
            'worklistuid' => '5',
            'createdate' => date("Y-m-d H:i:s"),
            'cuser' => $location_now_hold,
            'cwhen' => date("Y-m-d H:i:s"),
        );
        $this->db_xray->insert('processcontrol', $pcc_hold);


        if ($this->db_xray->affected_rows()) {

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, APITRIGGER . "/trigger_xray?location_id=" . $locationuid_login);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec($ch);
            curl_close($ch);

            $ch1 = curl_init();
            curl_setopt($ch1, CURLOPT_URL, APITRIGGER . '/trigger?location_id=' . $locationuser);
            curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch1);
            curl_close($ch1);

            return true;
        } else {
            return false;
        }
    }


    public function AddNote()
    {

        $patientdetail_uid = $this->input->post('patientdetail_uid');
        $patientxray_uid = $this->input->post('patientxray_uid');
        $text_area_note = $this->input->post('text_area_note');
        $check_status_type_note = $this->input->post('check_status_type_note');
        $location_now_note = $this->input->post('location_now_note');
        $locationuid_login = $this->input->post('locationuid_login');

        $find_note =  $this->db_xray->select('*')
            ->from('notedetail')
            ->where('patientdetailuid', $patientdetail_uid)
            ->get();

        if ($find_note->num_rows() > 0) {   //เคยมีให้อัพเดท
            $data_update = array(
                'notedetail' => $text_area_note,
                'muser' => $location_now_note,
                'mwhen' => date("Y-m-d H:i:s"),
            );

            $this->db_xray->where('patientdetailuid', $patientdetail_uid)
                ->update('notedetail', $data_update);
        } else {                            //ไม่มี insert ใหม่

            if ($check_status_type_note == 'order_all') {

                $data = array(
                    'patientdetailuid' => $patientdetail_uid,
                    'patientxrayuid' => null,
                    'notedetail' => $text_area_note,
                    'cuser' => $location_now_note,
                    'cwhen' => date("Y-m-d H:i:s"),
                    'muser' => $location_now_note,
                    'mwhen' => date("Y-m-d H:i:s"),
                    'xraycategoryuid' => null
                );
                $this->db_xray->insert('notedetail', $data);
            } else {
                $data = array(
                    'patientdetailuid' => $patientdetail_uid,
                    'patientxrayuid' => $patientxray_uid,
                    'notedetail' => $text_area_note,
                    'cuser' => $location_now_note,
                    'cwhen' => date("Y-m-d H:i:s"),
                    'muser' => $location_now_note,
                    'mwhen' => date("Y-m-d H:i:s"),
                    'xraycategoryuid' => $check_status_type_note
                );
                $this->db_xray->insert('notedetail', $data);
            }
        }



        if ($this->db_xray->affected_rows()) {

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, APITRIGGER . "/trigger_xray?location_id=" . $locationuid_login);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec($ch);
            curl_close($ch);

            return true;
        } else {
            return false;
        }
    }

    public function AddPrint()
    {
        $patientdetail_uid_print = $this->input->post('patientdetail_uid_print');
        $patientxray_uid_print = $this->input->post('patientxray_uid_print');
        $xraycategory_uid_print = $this->input->post('xraycategory_uid_print');
        $hn = $this->input->post('hn');
        $en = $this->input->post('en');
        $patientdetailuid = $this->input->post('patientdetailuid');
        $check_status_type_print = $this->input->post('check_status_type_print');
        $location_now_print = $this->input->post('location_now_print');
        $locationuid_login = $this->input->post('locationuid_login');
        $sublocationuid_login = $this->input->post('sublocationuid_login');
        $queueno = $this->input->post('queueno');
        $nation_get = $this->input->post('nation');
        $secure_code = $this->input->post('secure_code');
        $nation = '';

        $fild_print = $this->db_xray->select('*')
            ->from('printlog')
            ->where('hn', $hn)
            ->where('en', $en)
            ->where('patientdetailuid', $patientdetailuid)
            ->get();

        if ($fild_print->num_rows() > 0) {
            $update_data = array(
                'muser' => $location_now_print,
                'mwhen' => date("Y-m-d H:i:s")
            );
            $this->db_xray->where('hn', $hn)
                ->where('en', $en)
                ->where('patientdetailuid', $patientdetailuid)
                ->update('printlog', $update_data);
        } else {


            if ($check_status_type_print == 'order_all') {
                $data = array(
                    'hn' => $hn,
                    'en' => $en,
                    'patientdetailuid' => $patientdetailuid,
                    // 'detail' => ,
                    'cwhen' => date("Y-m-d H:i:s"),
                    'cuser' => $location_now_print,
                    'xraycategoryuid' => null,
                    'mwhen' => date("Y-m-d H:i:s"),
                    'muser' => $location_now_print,
                );
                $this->db_xray->insert('printlog', $data);
            } else {
                $data = array(
                    'hn' => $hn,
                    'en' => $en,
                    'patientdetailuid' => $patientdetailuid,
                    // 'detail' => ,
                    'cwhen' => date("Y-m-d H:i:s"),
                    'cuser' => $location_now_print,
                    'xraycategoryuid' => $check_status_type_print,
                    'mwhen' => date("Y-m-d H:i:s"),
                    'muser' => $location_now_print,
                );
                $this->db_xray->insert('printlog', $data);
            }
        }



        if ($nation_get == 'ไทย' || $nation_get == '' || $nation_get == null || $nation_get == 'ไม่ระบุ') {
            $nation = 'TH';
        } else {
            $nation = 'EN';
        }


        //trigcall
        $chcall = curl_init();
        curl_setopt($chcall, CURLOPT_URL, APITRIGGER . '/print');
        curl_setopt($chcall, CURLOPT_POST, 1);
        curl_setopt(
            $chcall,
            CURLOPT_POSTFIELDS,
            "location=COB&sub_location=" . $sublocationuid_login . "&Queue=" . $queueno . "&HN=" . $hn . "&VN=" . $en . "&When=" . date('Y-m-d H:i:s') . "&Language=" . $nation . "&Secure=" . $secure_code
        );
        curl_setopt($chcall, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($chcall);
        curl_close($chcall);
        //trigcall


        if ($this->db_xray->affected_rows()) {

            $ch = curl_init(); //
            curl_setopt($ch, CURLOPT_URL, APITRIGGER . "/trigger_xray?location_id=" . $locationuid_login);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec($ch);
            curl_close($ch);

            return true;
        } else {
            return false;
        }
    }


    public function UpdateComplete()
    {
        $patientdetail_uid = $this->input->post('patientdetail_uid');
        $patientxray_uid = $this->input->post('patientxray_uid');
        $check_status_type = $this->input->post('check_status_type');
        $location_now = $this->input->post('location_now');
        $locationuid_login = $this->input->post('locationuid_login');

        $data = array(
            'patientdetailuid' => $patientdetail_uid,
            'patientxrayuid' => $patientxray_uid,
            'worklistuid' => '6',
            'createdate' => date("Y-m-d H:i:s"),
            'cuser' => $location_now,
            'cwhen' => date("Y-m-d H:i:s")
        );

        $this->db_xray->insert('processcontrol', $data);

        if ($this->db_xray->affected_rows()) {

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, APITRIGGER . "/trigger_xray?location_id=" . $locationuid_login);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec($ch);
            curl_close($ch);

            return true;
        } else {
            return false;
        }
    }

    public function UpdateCloseq()
    {
        $patientdetail_uid = $this->input->post('patientdetail_uid');
        $patientxray_uid = $this->input->post('patientxray_uid');
        $check_status_type = $this->input->post('check_status_type');
        $location_now = $this->input->post('location_now');
        $locationuid_login = $this->input->post('locationuid_login');
        $locationuser = $this->input->post('locationuser');
        

        if ($check_status_type == 'order_all') {
            $data_insert_pcc = array(
                'patientdetailuid' => $patientdetail_uid,
                'patientxrayuid' => null,
                'worklistuid' => '3',
                'createdate' => date("Y-m-d H:i:s"),
                'cuser' => $location_now,
                'cwhen' => date("Y-m-d H:i:s")
            );
        } else {
            $data_insert_pcc = array(
                'patientdetailuid' => $patientdetail_uid,
                'patientxrayuid' => $patientxray_uid,
                'worklistuid' => '3',
                'createdate' => date("Y-m-d H:i:s"),
                'cuser' => $location_now,
                'cwhen' => date("Y-m-d H:i:s")
            );
        }


        $data_pt_detail = array(
            'xraydischarge' => 'Y',
            'xraydischargedate' => date("Y-m-d H:i:s")
        );

        $data_pt_xray = array(
            'xraydischarge' => 'Y',
            'xraydischargedate' => date("Y-m-d H:i:s")
        );



        $insert_pcc = $this->db_xray->insert('processcontrol', $data_insert_pcc);


        if ($check_status_type == 'order_all') {
            $update_pt_detail =  $this->db_xray->where('uid', $patientdetail_uid)->update('patientdetail', $data_pt_detail);
            $update_pt_xray = $this->db_xray->where('patientdetailuid', $patientdetail_uid)->update('patientxray', $data_pt_xray);
        } else {
            $update_pt_xray = $this->db_xray->where('uid', $patientxray_uid)->update('patientxray', $data_pt_xray);
        }


        if ($this->db_xray->affected_rows()) {

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, APITRIGGER . "/trigger_xray?location_id=" . $locationuid_login);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec($ch);
            curl_close($ch);


            $ch1 = curl_init();
            curl_setopt($ch1, CURLOPT_URL, APITRIGGER . '/trigger?location_id=' . $locationuser);
            curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch1);
            curl_close($ch1);

            return true;
        } else {
            return false;
        }
    }


    public function CheckCloseAllHn($ptduid)
    {

        $this->db_xray->select('*')
            ->from('vw_checkcloseall')
            ->where('patientdetailuid', $ptduid);

        $query = $this->db_xray->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }


    public function CloseAllHn($patientdetail_uid, $locationuid_login, $admin_login_uid, $patientxrayinfo)
    {

        $data_pt_detail = array(
            'xraydischarge' => 'Y',
            'xraydischargedate' => date("Y-m-d H:i:s")
        );

        $data_pt_xray = array(
            'xraydischarge' => 'Y',
            'xraydischargedate' => date("Y-m-d H:i:s")
        );

        $update_pt_detail =  $this->db_xray->where('uid', $patientdetail_uid)->update('patientdetail', $data_pt_detail);
        $update_pt_xray = $this->db_xray->where('patientdetailuid', $patientdetail_uid)->update('patientxray', $data_pt_xray);



        $data_insert_pcc_main = array(                 //บันทึกรายการของ patientdetail หลัก
            'patientdetailuid' => $patientdetail_uid,
            'patientxrayuid' => null,
            'worklistuid' => '7',
            'createdate' => date("Y-m-d H:i:s"),
            'cuser' => $admin_login_uid,
            'cwhen' => date("Y-m-d H:i:s")
        );

        $insert_pcc_main = $this->db_xray->insert('processcontrol', $data_insert_pcc_main);

        foreach ($patientxrayinfo as $keypcc => $valuepcc) {

            $data_insert_pcc_cate = array(              //บันทึกรายการของ patientdetail + order ย่อยทุกอัน
                'patientdetailuid' => $valuepcc['patientdetailuid'],
                'patientxrayuid' => $valuepcc['patientxrayuid'],
                'worklistuid' => '7',
                'createdate' => date("Y-m-d H:i:s"),
                'cuser' => $admin_login_uid,
                'cwhen' => date("Y-m-d H:i:s")
            );

            $insert_pcc_cate = $this->db_xray->insert('processcontrol', $data_insert_pcc_cate);
        }

        if ($this->db_xray->affected_rows()) {

            // $ch = curl_init();
            // curl_setopt($ch, CURLOPT_URL, APITRIGGER . "/trigger_xray?location_id=" . $locationuid_login);
            // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            // $output = curl_exec($ch);
            // curl_close($ch);

            return true;
        } else {
            return false;
        }
    }


    public function GetNote()
    {
        $patientdetail_uid = $this->input->post('patientdetail_uid');
        $patientxray_uid = $this->input->post('patientxray_uid');
        $check_status_type = $this->input->post('check_status_type');


        $this->db_xray->select('*')
            ->from('notedetail')
            ->where('patientdetailuid', $patientdetail_uid)
            ->order_by('notedetail.uid', 'desc');

        // if ($check_status_type == 'order_all') {
        //     $this->db_xray->select('*')
        //         ->from('notedetail')
        //         ->where('patientdetailuid', $patientdetail_uid)
        //         ->where('patientxrayuid', $patientxray_uid)
        //         ->where('xraycategoryuid is null');
        // } else {
        //     $this->db_xray->select('*')
        //         ->from('notedetail')
        //         ->where('patientdetailuid', $patientdetail_uid)
        //         ->where('patientxrayuid', $patientxray_uid)
        //         ->where('xraycategoryuid', $check_status_type);
        // }


        $query = $this->db_xray->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function GetHold()
    {
        $patientdetail_uid = $this->input->post('patientdetail_uid');
        $patientxray_uid = $this->input->post('patientxray_uid');
        $check_status_type = $this->input->post('check_status_type');

        if ($check_status_type == 'order_all') {

            $this->db_xray->select('*')
                ->from('messagedetail')
                ->where('patientdetailuid', $patientdetail_uid)
                ->where('patientxrayuid', $patientxray_uid)
                ->where('xraycategoryuid is null');
        } else {

            $this->db_xray->select('*')
                ->from('messagedetail')
                ->where('patientdetailuid', $patientdetail_uid)
                ->where('patientxrayuid', $patientxray_uid)
                ->where('xraycategoryuid', $check_status_type);
        }

        $query =  $this->db_xray->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function FindOrderImg($patientdetail_uid)
    {

        $this->db_xray->select('*')
            ->from('vw_orderimage')
            ->where('patientdetailuid', $patientdetail_uid);


        $query =  $this->db_xray->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function SelectPatientNew($data)
    {
        $this->db_xray->select('*')
            ->from('vw_patientdetail_today')
            ->where('patientdetail_uid', $data);


        $query =  $this->db_xray->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function SelectPatientXrayInfo($patientdetail_uid)
    {
        $this->db_xray->select('*')
            ->from('vw_patientdetail_today_checkclose')
            ->where('patientdetailuid', $patientdetail_uid);


        $query = $this->db_xray->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function CloseAllPt($admin_login_uid = null, $hn = null)
    {

        $status_type = $this->input->post('status_type');

        if (is_null($this->input->post('hn') == null)) {
            $hn = $hn;
            $admin_login = $admin_login_uid;
        } else {
            $hn = $this->input->post('hn');
            $admin_login = $this->input->post('admin_login');
        }

        $this->db_xray->select('*')
            ->from('vw_checksendpatient')
            ->where('hn', $hn);

        $query = $this->db_xray->get();

        return $query->result_array();
    }

    public function InsCloseAll($result_gethn)
    {
        $admin_login = $this->input->post('admin_login');
        $locationuid_login = $this->input->post('locationuid_login');
        $locationuser = $this->input->post('locationuser');
        


        $data_pt_detail = array(
            'xraydischarge' => 'Y',
            'xraydischargedate' => date("Y-m-d H:i:s")
        );

        $data_pt_xray = array(
            'xraydischarge' => 'Y',
            'xraydischargedate' => date("Y-m-d H:i:s")
        );



        $check_ptuid = '';
        foreach ($result_gethn as $key => $value) {


            if ($check_ptuid != $value['uid']) {

                $update_pt_detail =  $this->db_xray->where('uid', $value['uid'])->update('patientdetail', $data_pt_detail);
                $update_pt_xray = $this->db_xray->where('patientdetailuid', $value['uid'])->update('patientxray', $data_pt_xray);

                $data_insert_pcc_main = array(                 //บันทึกรายการของ patientdetail หลัก
                    'patientdetailuid' => $value['uid'],
                    'patientxrayuid' => null,
                    'worklistuid' => '7',
                    'createdate' => date("Y-m-d H:i:s"),
                    'cuser' => $admin_login,
                    'cwhen' => date("Y-m-d H:i:s")
                );
                $insert_pcc_main = $this->db_xray->insert('processcontrol', $data_insert_pcc_main);
            }
            $check_ptuid = $value['uid'];


            $data_insert_pcc_cate = array(              //บันทึกรายการของ patientdetail + order ย่อยทุกอัน
                'patientdetailuid' => $value['uid'],
                'patientxrayuid' => $value['patientxrayuid'],
                'worklistuid' => '7',
                'createdate' => date("Y-m-d H:i:s"),
                'cuser' => $admin_login,
                'cwhen' => date("Y-m-d H:i:s")
            );

            $insert_pcc_cate = $this->db_xray->insert('processcontrol', $data_insert_pcc_cate);
        }

        if ($this->db_xray->affected_rows()) {

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, APITRIGGER . "/trigger_xray?location_id=" . $locationuid_login);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec($ch);
            curl_close($ch);


            return true;
        } else {
            return false;
        }
    }

    public function FindOrderHn($hn, $vn)
    {

        $query = "select * from vw_checksendpatient
                where replace(hn,'-','') like replace('$hn','-','') 
                and replace(en,'/','') like replace('$vn','/','') 
                ";

        $code = $this->db_xray->query($query)->result_array();

        return $code;
    }

    public function checkCloseAll()
    {
        $hn = $this->input->post('hn');

        $query = "select count(pd.uid) as count_hn
                    from patientdetail pd
                    where pd.cwhen >= '" . date('Y-m-d 00:00:00') . "'
                    and pd.cwhen <= '" . date('Y-m-d 23:59:59') . "'
                    and replace(pd.hn,'-','') like replace('$hn','-','')
                    union all
                    select count(pd.uid) as count_hn
                    from patientdetail pd
                    where  pd.cwhen >= '" . date('Y-m-d 00:00:00') . "'
                    and pd.cwhen <= '" . date('Y-m-d 23:59:59') . "'
                    and replace(pd.hn,'-','') like replace('$hn','-','')
                    and pd.xraydischarge is not null
                ";
        $code = $this->db_xray->query($query);

        if ($code->num_rows() > 0) {
            return $code->result_array();
        } else {
            return false;
        }
    }

    public function createNewQueue()
    {
        $hn = $this->input->post('hn');

        $query = "  select * from patient_information
                    where replace(hn,'-','') like replace('$hn','-','')
                    order by patient_information_id desc LIMIT 1
                ";
        $code = $this->db->query($query);

        if ($code->num_rows() > 0) {
            return $code->result_array();
        } else {
            return false;
        }
    }

    public function xrayGenQueue($admin_login_uid)
    {

        $this->db_xray->select('*')
            ->from('queuenumber')
            ->where('patientcategoryuid', '1')
            ->where('createdate >=', date('Y-m-d 00:00:00'))
            ->where('createdate <=', date('Y-m-d 23:59:59'));
        $query = $this->db_xray->get();

        if ($query->num_rows() > 0) { //วันนี้เคย gen ไปแล้ว
            $get_last_queue = $query->result_array()[0]['lasttokenno'];

            $tem = explode('x', $get_last_queue);
            $newqueue = (intval($tem[1]) + 1);

            if ($newqueue < 10) {
                $newqueue = 'x000' . $newqueue;
            } else if ($newqueue >= 10 && $newqueue <= 99) {
                $newqueue = 'x00' . $newqueue;
            } else if ($newqueue >= 100 && $newqueue <= 999) {
                $newqueue = 'x0' . $newqueue;
            } else {
                $newqueue = 'x' . $newqueue;
            }

            $update_queue = [
                'lasttokenno' => $newqueue,
                'muser' => $admin_login_uid,
                'mwhen' => date('Y-m-d H:i:s'),
            ];

            $this->db_xray->where('uid', $query->result_array()[0]['uid'])
                ->update('queuenumber', $update_queue);
            return $newqueue;
        } else {
            $new_queue = [
                'patientcategoryuid' => '1',
                'createdate' => date('Y-m-d'),
                'lasttokenno' => 'x0001',
                'statusflag' => null,
                'cuser' => $admin_login_uid,
                'cwhen' => date('Y-m-d H:i:s'),
                'muser' => $admin_login_uid,
                'mwhen' => date('Y-m-d H:i:s'),
                'queuelocationuid' => '1'
            ];

            $this->db_xray->insert('queuenumber', $new_queue);

            return 'x0001';
        }
    }
}
