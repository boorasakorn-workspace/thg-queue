<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class api_lab extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
        $this->load->helper('assets');
        $this->load->helper('json');
		$this->load->helper('recursiveconvert');
		$this->load->helper('calculate');
    }

    public function get($secure_code,$version = 'v1'){
        switch($version):
            case 'v1':
                $this->load->model('MDL_APILab');
                $result = $this->MDL_APILab->getDataApi($secure_code);
                echo json_encode($result);
            break;
        endswitch;
    }

    public function insert($version = 'v1'){
        $Post = array(
            'hn' => $this->input->post('hn',TRUE),
            'location' => $this->input->post('location',TRUE),
        );
        switch($version):
            case 'v1':
                $this->load->model('MDL_APILab');
                $InsertData = $this->MDL_APILab->insertLab($Post);
                $this->load->module('Lab');
                $this->lab->InsertPatient($InsertData);
                $ch1 = curl_init();
                curl_setopt($ch1, CURLOPT_URL,APITRIGGER."/trigger?location_id=${Post['location']}");
                curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
                $output = curl_exec($ch1);
                curl_close($ch1);
            break;
        endswitch;
    }

    public function countdisplay($location,$version = 'v1'){
        switch($version):
            case 'v1':
                $this->load->model('MDL_APILab');
                $result = $this->MDL_APILab->getWaitingHold($location);
                echo json_encode($result);
            break;
        endswitch;
    }
}