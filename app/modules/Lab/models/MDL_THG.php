<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MDL_THG extends CI_Model {

    function __construct(){
        parent::__construct();
    }

    function DBSelect($Table){
        $this->db->select('*');
        $this->db->from($Table);
        $query = $this->db->get();
        return $query;
    }

    function getPatientRow($data){
        if(isset($data)){
            $this->db->select('*');
            $this->db->from('vw_getscanqueue');
            $this->db->where($data);
            $query = $this->db->get();
            //return ( $query->num_rows()>0 ? $query->row() : false );
            return ( $query->num_rows()>0 ? $query->row() : false );
        }else{
            return false;
        }
    }

    function getPatientInfo($data){
        if(isset($data['hn']) && $data['hn']){
            // $SearchHN = $data['hn'];
            // $this->db->select('*');
            // $this->db->from('vw_getscanqueue');
            // $this->db->where('hn', $SearchHN);
            // $this->db->order_by('vn','ASC');
            // $this->db->limit('1');
            // $query = $this->db->get();
            // return ( $query->num_rows()>0 ? $query->result() : false );
            $SearchHN = $data['hn'];
            $queryStr = "SELECT * FROM vw_getscanqueue 
            WHERE replace(hn,'-','') LIKE replace('$SearchHN','-','') 
            ORDER BY vn ASC 
            LIMIT 1";
            $query = $this->db->query($queryStr);
            return ( $query->num_rows()>0 ? $query->result() : false );
        }else{
            return false;
        }
    }
}