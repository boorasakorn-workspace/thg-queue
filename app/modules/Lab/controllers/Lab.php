<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lab extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('assets');
		$this->load->helper('json');
		$this->load->helper('recursiveconvert');
		$this->load->helper('calculate');
		$this->load->model('MDL_THG');
		$this->load->model('MDL_Lab');
	}

	public function index(){
		if( $this->session->userdata('recent_management') ){
			$Recent = $this->session->userdata('recent_management');
			redirect('Lab/'.$Recent);
		}else{
			redirect('Lab/Lab_Main');
		}
	}
	//Logout
	public function Logout(){
		$this->session->unset_userdata('userlogin');
		redirect('Lab');
		return true;
	}

	private function renderView($Data){
		$this->load->module('Template_Module');
		$Data['Module'] = 'Lab';
		$this->template_module->Template('Lab_tem', $Data);
	}
	// JSON View
	public function MainManagement(){
		$this->session->set_userdata('recent_management', 'Lab_Main');
		$user_table = 0;
		switch($this->session->userdata('userlogin')['username']) {
			case 'lab01':
				$user_table = 1;
				break;
			case 'lab02':
				$user_table = 2;
				break;
			case 'lab03':
				$user_table = 3;
				break;    
			default:
				$user_table = 0;
				break;
		}
		header( "Content-Type: application/json" );
		$data = array(
            'Data' => array(
				'usertable' => $user_table,
				'table' => $this->MDL_Lab->getTable(),
				'queuedata' => $this->MDL_Lab->getToday_Queue(),
				'holdmessage' => $this->MDL_Lab->getMessage(),
			),
		);
		$view = isset($this->session->userdata('userlogin')['locationuser']) && $this->session->userdata('userlogin')['locationuser'] != 'LAB' ? "Lab/Lab_Queue_Location_vw" : "Lab/Lab_Queue_vw"; 
		$output = array(
			'html'=>$this->load->view($view,$data,TRUE),
		);
		echo json_encode($output);
	}
	public function MainTable(){
		$view = isset($this->session->userdata('userlogin')['locationuser']) && $this->session->userdata('userlogin')['locationuser'] != 'LAB' ? "Lab/json_views/Lab_Queue_Location_TR" : "Lab/json_views/Lab_Queue_TR"; 
		$viewsData = array(
			'Data' => array(
				'queuedata' => $this->MDL_Lab->getToday_Queue(),
			),
		);
		header( "Content-Type: application/json" );
		$message = array(
			'method' => 'MainTable',
			'result' => 'Success',
			'target' => '#Lab_Queue_TBody',
			'html' => $this->load->view($view,$viewsData,TRUE),
		);
		echo json_response(200, $message);
	}
	public function ClosedManagement(){
		$this->session->set_userdata('recent_management', 'Lab_Closed');
		header( "Content-Type: application/json" );
		$data = array(
            'Data' => array(
				'queuedata' => $this->MDL_Lab->getToday_Closed(),
			),
		);
		$view = isset($this->session->userdata('userlogin')['locationuser']) && $this->session->userdata('userlogin')['locationuser'] != 'LAB'? "Lab/Lab_Closed_Location_vw" : "Lab/Lab_Closed_vw"; 
		$output = array(
			'html'=>$this->load->view($view,$data,TRUE),
		);
		echo json_encode($output);
	}
	public function ClosedTable(){
		$view = isset($this->session->userdata('userlogin')['locationuser']) && $this->session->userdata('userlogin')['locationuser'] != 'LAB' ? "Lab/json_views/Lab_Closed_Location_TR" : "Lab/json_views/Lab_Closed_TR"; 
		$viewsData = array(
			'Data' => array(
				'queuedata' => $this->MDL_Lab->getToday_Closed(),
			),
		);
		header( "Content-Type: application/json" );
		$message = array(
			'method' => 'ClosedTable',
			'result' => 'Success',
			'target' => '#Lab_Closed_TBody',
			'html' => $this->load->view($view,$viewsData,TRUE),
		);
		echo json_response(200, $message);
	}

	// Main
	public function Lab_Main(){
		if(!$this->session->userdata('userlogin')){
			redirect(base_url(),'refresh');
		}
		$Template = array(
			'Site_Title' => 'Dashqueue',
			'Script' => array(
				'Script' => 'Lab_script',
			),
            'Data' => array(
				'table' => $this->MDL_Lab->getTable(),
				'queuepage' => base_url('Lab/MainManagement'),
			),
		);
		$this->renderView($Template);
	}
	public function Lab_Closed(){
		if(!$this->session->userdata('userlogin')){
			redirect(base_url(),'refresh');
		}
		$Template = array(
			'Site_Title' => 'Dashqueue',
			'Script' => array(
				'Script' => 'Lab_script',
			),
            'Data' => array(
				'queuepage' => base_url('Lab/ClosedManagement'),
			),
		);
		$this->renderView($Template);
	}

	// API
	public function API($Method = NULL,$JSONResponse = TRUE){
		if($Method){
			if( $this->input->post() !== NULL ){
				$Input = $this->input->post();
				switch ($Method) {
					case 'NewPatient':
						$result = $this->NewPatient($Input,FALSE);
						if($JSONResponse){
							$message = array(
								'API' => 'POST',
								'Method' => $Method,
								'Input' => $Input,
								'response' => $result,
							);
							echo json_response(200, $message);
						}else{
							return $result;
						}
					break;
					case 'Scan':
						$result = $this->ScanPatient($Input,FALSE);
						if($JSONResponse){
							$message = array(
								'API' => 'POST',
								'Method' => $Method,
								'Input' => $Input,
								'response' => $result,
							);
							echo json_response(200, $message);
						}else{
							return $result;
						}
					break;
					case 'Call':
						$result = $this->Queue_Call($Input,FALSE);
						if($JSONResponse){
							$message = array(
								'API' => 'POST',
								'Method' => $Method,
								'Input' => $Input,
								'response' => $result,
							);
							echo json_response(200, $message);
						}else{
							return $result;
						}
					break;
					case 'Note':
						$result = $this->Queue_Note($Input,FALSE);
						if($JSONResponse){
							$message = array(
								'API' => 'POST',
								'Method' => $Method,
								'Input' => $Input,
								'response' => $result,
							);
							echo json_response(200, $message);
						}else{
							return $result;
						}
					break;
					case 'Close':
						$result = $this->Queue_Close($Input,FALSE);
						if($JSONResponse){
							$message = array(
								'API' => 'POST',
								'Method' => $Method,
								'Input' => $Input,
								'response' => $result,
							);
							echo json_response(200, $message);
						}else{
							return $result;
						}
					break;
					case 'Revert':
						$result = $this->Queue_Revert($Input,FALSE);
						if($JSONResponse){
							$message = array(
								'API' => 'POST',
								'Method' => $Method,
								'Input' => $Input,
								'response' => $result,
							);
							echo json_response(200, $message);
						}else{
							return $result;
						}
					break;
					default:
						if($JSONResponse){
							$message = array(
								'API' => 'POST',
								'Method' => $Method,
								'Input' => $Input,
								'response' => 'Invalid Method',
							);
							echo json_response(400, $message);
						}else{
							return false;
						}
					break;
				}
			}else{
				if($JSONResponse){
					$message = array(
						'API' => 'POST',
						'Method' => $Method,
						'response' => 'No Input',
					);
					echo json_response(400, $message);
				}else{
					return false;
				}
			}
		}else{
			if($JSONResponse){
				$message = array(
					'API' => 'POST',
					'response' => 'No Method',
				);
				echo json_response(400, $message);
			}else{
				return false;
			}
		}
	}

	// Call Main THG API
	public function NewPatient($Data = NULL,$JSONResponse = TRUE){		
		$InsertData = to_stdClass(($Data ? $Data : ($this->input->post() !== NULL ? $this->input->post() : array())));
		
		if( $InsertData != to_stdClass(array()) ){
			if( isset($InsertData->HN) && $InsertData->HN && isset($InsertData->VN) && $InsertData->VN && isset($InsertData->VisitDate) && $InsertData->VisitDate){
				$NewInfo = array(
					'HN' => $InsertData->HN,
					'VN' => $InsertData->VN,
					'VisitDate' => $InsertData->VisitDate,
					'TitleTh' => 'นาย',
					'ForenameTh' => 'เหลง',
					'SurnameTh' => 'นะจ๊ะ',
					'TitleEn' => 'Mr',
					'ForenameEn' => 'leng',
					'SurnameEn' => 'naja',
					'Gender' => 'M',
					'BirthDate' => '2019-04-10',
					'Nation' => 'ไทย',
					'Religion' => 'ไทย',
					'PatientComeType' => 'N',
					'Payor' => 'ผู้ป่วยโรงพยาบาล',
					'LocationID' => 'C2',
					'LocationName' => 'ห้องตรวจเด็ก',
					'CurrentLocationID' => '',
					'CurrentLocationName' => '',
					'CurrentDoctorCode' => '06031',
					'CurrentDoctorName' => 'ชื่อหมอ',
					'PatientStatus' => 'ห้องยา 1 (ER)',
					'MedicalDischargeDate' => '',
					'MedicalDischargeNote' => '',
					'MedicalDischargeStat' => '',
					'MedicalDischargedType' => '0/1',
					'ScanDate' => '00:00',
					'FinancialDischargeDate' => '',
					'IsBillFinalzed' => 'N',
					'PHLocation' => 'จบส่งห้องยา',
					'FINLocation' => '',
					'Prescription_No' => '',
					'PrescriptionDateTime' => '12/06/2562 12:00',
					'DocumentType' => 'DR',
					'DN' => '',
					'ArriveDateTime' => '10/01/2562 14:49',
					'PatientType' => 'Walkin',
					'PatientStatusCancel' => '',
					'PatientStatusChange' => '',
					'Status_datetime' => '10/01/2562 14:49:10',
					'NewHN' => ''
				);
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL,"https://telecorp.co.th/ws/PatientInformation.php");
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS,$NewInfo);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$result = curl_exec($ch);
				if($result){
					$ScanData = array(
						'hn'=> $InsertData->HN,
					);
					return $this->ScanPatient($ScanData,$JSONResponse);
				}else{
					if($JSONResponse){
						$message = array(
							'method' => 'NewPatient',
							'result' => 'Failed',
						);
						echo json_response(200, $message);
					}else{
						return false;
					}
				}
			}else{
				if($JSONResponse){
					$message = array(
						'Method' => 'NewPatient',
						'result' => 'Success',
						'Input' => $InsertData,
						'response' => 'Invalid Input',
					);
					echo json_response(400, $message);
				}else{
					return false;
				}
			}
		}else{
			if($JSONResponse){
				$message = array(
					'Method' => 'NewPatient',
					'response' => 'No Input',
				);
				echo json_response(400, $message);
			}else{
				return false;
			}
		}
	}
	
	// Function
	public function SearchPatient($Data = NULL,$JSONResponse = TRUE){
		$Search = to_Array(($Data ? $Data : ($this->input->post() !== NULL ? $this->input->post() : array())));

		$PatientInfo = $this->MDL_THG->getPatientInfo($Search);
		$SearchDB = $this->MDL_Lab->getQueueInfo_Available($Search,FALSE);
		if($JSONResponse){
			$message = array(
				'method' => 'Search',
				'result' => ($PatientInfo != false ? 'Success' : ($SearchDB != false ? 'Success' : 'Failed')),
				'text' => ($PatientInfo != false ? $PatientInfo : ($SearchDB != false ? $SearchDB : 'Not Found Patient Info'))
			);
			if($PatientInfo != false) {
				$message['data'] = $PatientInfo;
			}
			if($SearchDB != false) {
				$message['dbdata'] = $SearchDB;
			}
			echo json_response(($PatientInfo != false ? 200 : 200), $message);
		}else{
			return ($PatientInfo != false ? $PatientInfo : ($SearchDB != false ? $SearchDB : false));
		}
	}
	public function SearchPatientLab($Data = NULL,$JSONResponse = TRUE){
		$Search = to_Array(($Data ? $Data : ($this->input->post() !== NULL ? $this->input->post() : array())));

		$LabSearch = to_stdClass(array( 'hn' => $Search['hn'] ));
		$PatientInfo = $this->MDL_Lab->getPatientDetail($LabSearch);
		if($JSONResponse){
			$message = array(
				'method' => 'Search In LabDB',
				'result' => ($PatientInfo != false ? 'Success' : 'Failed'),
				'text' => ($PatientInfo != false ? $PatientInfo : 'Not Found Patient Info')
			);
			if($PatientInfo != false) $message['data'] = $PatientInfo;
			echo json_response(($PatientInfo != false ? 200 : 200), $message);
		}else{
			return ($PatientInfo != false ? $PatientInfo : false);
		}
	}
	public function InsertPatient($Data = NULL,$JSONResponse = TRUE){
		$InsertData = to_stdClass(($Data ? $Data : ($this->input->post() !== NULL ? $this->input->post() : array())));

		//$InsertResult = $this->MDL_Lab->InsertPatientDetail($InsertData);
		if( isset($InsertData->location) ){
			$InsertResult = $this->MDL_Lab->InsertPatientDetail($InsertData);
			$queuedata = array($InsertResult);
		}else if( isset($InsertData->uid) || (isset($InsertData->dbdata) && $InsertData->dbdata) ){
			$InsertResult = $this->MDL_Lab->InsertPatientDetail($InsertData);
			$queuedata = array($InsertResult);
		}else if( (isset($InsertData->hn) && $InsertData->hn) && ((isset($InsertData->en) && $InsertData->en) || (isset($InsertData->vn) && $InsertData->vn)) && ((isset($InsertData->queueno) && $InsertData->queueno) || (isset($InsertData->tokenno) && $InsertData->tokenno))){
			if((isset($InsertData->en) && $InsertData->en)) $InsertData->vn = $InsertData->en;
			if((isset($InsertData->queueno) && $InsertData->queueno)) $InsertData->tokenno = $InsertData->queueno;
			$InsertResult = $this->MDL_Lab->InsertPatientDetail($InsertData);
			$queuedata = array($InsertResult);
		}else{
			$queuedata = array();
			foreach($InsertData as $ID_Key => $ID_Val){
				$Sub_Insert = $this->MDL_Lab->InsertPatientDetail($ID_Val);
				if($Sub_Insert){
					array_push($queuedata, $Sub_Insert);
				}
				usleep( 1000*1000 );
			}
			$InsertResult = $queuedata;
		}
		if($InsertResult){
			$this->process_trigger( ($this->session->userdata('userlogin')['locationuid'] && $this->session->userdata('userlogin')['locationuser'] != 'LAB'?$this->session->userdata('userlogin')['locationuid']: (isset($InsertData->tableuid)?$InsertData->tableuid:0) ) );
			if($JSONResponse){
				$view = isset($this->session->userdata('userlogin')['locationuser']) && $this->session->userdata('userlogin')['locationuser'] != 'LAB' ? "Lab/json_views/Lab_Queue_Location_TR" : "Lab/json_views/Lab_Queue_TR"; 
				$viewsData = array(
					'Data' => array(
						'queuedata' => $queuedata,
					),
				);
				header( "Content-Type: application/json" );
				$message = array(
					'method' => 'Insert',
					'result' => 'Success',
					'text' => 'Insert Success',
					'html' => $this->load->view($view,$viewsData,TRUE),
				);
				if($queuedata[0]){
					$message['queuedata'] = $queuedata;
				}
				echo json_response(200, $message);
			}else{
				return ($InsertResult != false ? $InsertResult : false);
			}
		}else{
			if($JSONResponse){
				$message = array(
					'method' => 'Insert',
					'result' => 'Failed',
					'text' => 'Insert Failed',
				);
				echo json_response(200, $message);				
			}else{
				return false;
			}
		}
	}
	public function ScanPatient($Data = NULL,$JSONResponse = TRUE){
		$Search = to_Array(($Data ? $Data : ($this->input->post() !== NULL ? $this->input->post() : array())));
		$SearchResult = $this->SearchPatient($Search,FALSE);
		if($SearchResult){
			if($JSONResponse){
				$this->InsertPatient($SearchResult);
			}else{
				return $this->InsertPatient($SearchResult,FALSE);
			}
		}else{
			$SearchDB = $this->SearchPatientLab($Search,FALSE);
			if($SearchDB){
				if($JSONResponse){
					$this->InsertPatient($SearchDB);
				}else{
					return $this->InsertPatient($SearchDB,FALSE);
				}
			}else{
				if($JSONResponse){
					$message = array(
						'method' => 'Scan',
						'result' => 'Failed',
						'text' => 'Not Found Patient Info',
					);
					echo json_response(200, $message);
				}else{
					return false;
				}
			}
		}
	}
	public function Queue_Call($Data = NULL,$JSONResponse = TRUE){
		$InsertData = to_stdClass(($Data ? $Data : ($this->input->post() !== NULL ? $this->input->post() : array())));
		
		if(isset($InsertData->patientuid) && $InsertData->patientuid){
			$InsertResult = $this->MDL_Lab->CallQueue($InsertData);
			if($InsertResult){
				$cURLCall = $this->process_call($InsertData);
				$cURLTrigger = $this->process_trigger( (isset($InsertData->tableuid)?$InsertData->tableuid:0) );
				if($JSONResponse){
					$message = array(
						'method' => 'Call Queue',
						'result' => 'Success',
						'active' => 'button.action_call[data-patientuid="'.$InsertData->patientuid.'"]',
						'trigger' => $cURLTrigger,
						'callcURL' => $cURLCall, 
					);
					echo json_response(200, $message);
				}else{
					return $InsertResult;
				}
			}else{
				if($JSONResponse){
					$message = array(
						'method' => 'Call Queue',
						'result' => 'Failed',
					);
					echo json_response(200, $message);				
				}else{
					return false;
				}
			}
		}else if(isset($InsertData->queueno) && $InsertData->queueno){
			$Search = to_stdClass(array(
				'queueno' => $InsertData->queueno,
			));
			$PatientDetail = $this->MDL_Lab->getPatientDetail($Search);
			if($PatientDetail){
				$NewData = array(
					'tableuid' => (isset($InsertData->tableuid) && $InsertData->tableuid ? $InsertData->tableuid : NULL ),
					'patientuid' => $PatientDetail->uid,
					'queueno' => $PatientDetail->queueno,
				);
				$this->Queue_Call($NewData,$JSONResponse);
			}else{
				if($JSONResponse){
					$message = array(
						'method' => 'Call Queue',
						'result' => 'Failed',
						'text' => 'Not Found Patient Info From Queueno',
					);
					echo json_response(200, $message);				
				}else{
					return false;
				}
			}

		}else{
			if($JSONResponse){
				$message = array(
					'method' => 'Call Queue',
					'result' => 'Failed',
					'text' => 'No Patientuid/Queueno',
				);
				echo json_response(400, $message);				
			}else{
				return false;
			}
		}
	}
	public function Queue_Note($Data = NULL,$JSONResponse = TRUE){
		$InsertData = to_stdClass(($Data ? $Data : ($this->input->post() !== NULL ? $this->input->post() : array())));
		if(isset($InsertData->patientuid) && $InsertData->patientuid){
			$InsertResult = $this->MDL_Lab->InsertNote($InsertData);
			if($InsertResult){
				if($JSONResponse){
					$this->process_trigger( ($this->session->userdata('userlogin')['locationuid']?$this->session->userdata('userlogin')['locationuid']:0) );
					$message = array(
						'method' => 'Insert Note',
						'result' => 'Success',
						'text' => 'Insert Success',
						'active' => 'button.action_note[data-patientuid="'.$InsertData->patientuid.'"]',
					);
					echo json_response(200, $message);
				}else{
					return ($InsertResult != false ? $InsertResult : false);
				}
			}else{
				if($JSONResponse){
					$message = array(
						'method' => 'Insert Note',
						'result' => 'Failed',
						'text' => 'Insert Failed',
					);
					echo json_response(200, $message);				
				}else{
					return false;
				}
			}
		}else if(isset($InsertData->queueno) && $InsertData->queueno){
			$Search = to_stdClass(array(
				'queueno' => $InsertData->queueno,
			));
			$PatientDetail = $this->MDL_Lab->getPatientDetail($Search);
			if($PatientDetail){
				$NewData = array(
					'textarea_note' => $InsertData->textarea_note,
					'patientuid' => $PatientDetail->uid,
					'queueno' => $PatientDetail->queueno,
				);
				$this->Queue_Note($NewData,$JSONResponse);
			}else{
				if($JSONResponse){
					$message = array(
						'method' => 'Insert Note',
						'result' => 'Failed',
						'text' => 'Not Found Patient Info From Queueno',
					);
					echo json_response(200, $message);				
				}else{
					return false;
				}
			}	
		}else{
			if($JSONResponse){
				$message = array(
					'method' => 'Insert Note',
					'result' => 'Failed',
					'text' => 'No Patientuid',
				);
				echo json_response(400, $message);				
			}else{
				return false;
			}
		}
	}
	public function Queue_Hold($Data = NULL,$JSONResponse = TRUE){
		$InsertData = to_stdClass(($Data ? $Data : ($this->input->post() !== NULL ? $this->input->post() : array())));
		if(isset($InsertData->patientuid) && $InsertData->patientuid){
			$InsertResult = $this->MDL_Lab->InsertHold($InsertData);
			if($InsertResult){
				if($JSONResponse){
					$this->process_trigger( ($this->session->userdata('userlogin')['locationuid']?$this->session->userdata('userlogin')['locationuid']:0) );
					$message = array(
						'method' => 'Insert Hold',
						'result' => 'Success',
						'text' => 'Insert Success',
						'active' => 'button.action_hold[data-patientuid="'.$InsertData->patientuid.'"]',
					);
					echo json_response(200, $message);
				}else{
					return ($InsertResult != false ? $InsertResult : false);
				}
			}else{
				if($JSONResponse){
					$message = array(
						'method' => 'Insert Hold',
						'result' => 'Failed',
						'text' => 'Insert Failed',
					);
					echo json_response(200, $message);				
				}else{
					return false;
				}
			}
		}else if(isset($InsertData->queueno) && $InsertData->queueno){
			$Search = to_stdClass(array(
				'queueno' => $InsertData->queueno,
			));
			$PatientDetail = $this->MDL_Lab->getPatientDetail($Search);
			if($PatientDetail){
				$NewData = array(
					'messageuid' => isset($InsertData->messageuid) && $InsertData->messageuid?$InsertData->messageuid:(isset($InsertData->remake) && $InsertData->remake?0:NULL),
					'remake' => isset($InsertData->remake) && $InsertData->remake?0:NULL,
					'patientuid' => $PatientDetail->uid,
					'queueno' => $PatientDetail->queueno,
				);
				$this->Queue_Hold($NewData,$JSONResponse);
			}else{
				if($JSONResponse){
					$message = array(
						'method' => 'Insert Hold',
						'result' => 'Failed',
						'text' => 'Not Found Patient Info From Queueno',
					);
					echo json_response(200, $message);				
				}else{
					return false;
				}
			}	
		}else{
			if($JSONResponse){
				$message = array(
					'method' => 'Insert Hold',
					'result' => 'Failed',
					'text' => 'No Patientuid',
				);
				echo json_response(400, $message);				
			}else{
				return false;
			}
		}
	}
	public function Queue_Close($Data = NULL,$JSONResponse = TRUE){
		$InsertData = to_stdClass(($Data ? $Data : ($this->input->post() !== NULL ? $this->input->post() : array())));
		if(isset($InsertData->patientuid) && $InsertData->patientuid){
			$InsertResult = $this->MDL_Lab->CloseQueue($InsertData);
			if($InsertResult){
				$this->process_trigger( ($this->session->userdata('userlogin')['locationuid']?$this->session->userdata('userlogin')['locationuid']:0) );
				if($JSONResponse){
					$message = array(
						'method' => 'Close Queue',
						'result' => 'Success',
					);
					if(isset($InsertData->queueno) && $InsertData->queueno) $message['remove'] = '#RowQueue_'.$InsertData->queueno;
					echo json_response(200, $message);
				}else{
					return $InsertResult;
				}
			}else{
				if($JSONResponse){
					$message = array(
						'method' => 'Close Queue',
						'result' => 'Failed',
					);
					echo json_response(200, $message);				
				}else{
					return false;
				}
			}
		}else if(isset($InsertData->queueno) && $InsertData->queueno){
			$Search = to_stdClass(array(
				'queueno' => $InsertData->queueno,
			));
			$PatientDetail = $this->MDL_Lab->getPatientDetail($Search);
			if($PatientDetail){
				$NewData = array(
					'patientuid' => $PatientDetail->uid,
					'queueno' => $PatientDetail->queueno,
				);
				$this->Queue_Close($NewData,$JSONResponse);
			}else{
				if($JSONResponse){
					$message = array(
						'method' => 'Close Queue',
						'result' => 'Failed',
						'text' => 'Not Found Patient Info From Queueno',
					);
					echo json_response(200, $message);				
				}else{
					return false;
				}
			}

		}else{
			if($JSONResponse){
				$message = array(
					'method' => 'Close Queue',
					'result' => 'Failed',
					'text' => 'No Patientuid',
				);
				echo json_response(400, $message);				
			}else{
				return false;
			}
		}
	}	
	public function Queue_Revert($Data = NULL,$JSONResponse = TRUE){
		$InsertData = to_stdClass(($Data ? $Data : ($this->input->post() !== NULL ? $this->input->post() : array())));
		if(isset($InsertData->patientuid) && $InsertData->patientuid){
			$InsertResult = $this->MDL_Lab->RevertQueue($InsertData);
			if($InsertResult){
				$this->process_trigger( ($this->session->userdata('userlogin')['locationuid']?$this->session->userdata('userlogin')['locationuid']:0) );
				if($JSONResponse){
					$message = array(
						'method' => 'Revert Queue',
						'result' => 'Success',
					);
					if(isset($InsertData->queueno) && $InsertData->queueno) $message['remove'] = '#RowClosedQueue_'.$InsertData->queueno;
					echo json_response(200, $message);
				}else{
					return $InsertResult;
				}
			}else{
				if($JSONResponse){
					$message = array(
						'method' => 'Revert Queue',
						'result' => 'Failed',
					);
					echo json_response(200, $message);				
				}else{
					return false;
				}
			}
		}else if(isset($InsertData->queueno) && $InsertData->queueno){
			$Search = to_stdClass(array(
				'queueno' => $InsertData->queueno,
			));
			$PatientDetail = $this->MDL_Lab->getPatientDetail($Search);
			if($PatientDetail){
				$NewData = array(
					'patientuid' => $PatientDetail->uid,
					'queueno' => $PatientDetail->queueno,
				);
				$this->Queue_Revert($NewData,$JSONResponse);
			}else{
				if($JSONResponse){
					$message = array(
						'method' => 'Revert Queue',
						'result' => 'Failed',
						'text' => 'Not Found Patient Info From Queueno',
					);
					echo json_response(200, $message);				
				}else{
					return false;
				}
			}

		}else{
			if($JSONResponse){
				$message = array(
					'method' => 'Revert Queue',
					'result' => 'Failed',
					'text' => 'No Patientuid',
				);
				echo json_response(400, $message);				
			}else{
				return false;
			}
		}
	}
	public function toggleTable($Data = NULL,$JSONResponse = TRUE){
		$UpdateData = to_stdClass(($Data ? $Data : ($this->input->post() !== NULL ? $this->input->post() : array())));
		if(isset($UpdateData->tableuid) && isset($UpdateData->active)){
			$UpdateResult = $this->MDL_Lab->toggleTable($UpdateData);
			if($UpdateResult){
				$cURLTrigger = $this->process_table();
				if($JSONResponse){
					$message = array(
						'method' => 'Toggle Table',
						'result' => 'Success',
						'TriggercURL' => $cURLTrigger,
					);
					echo json_response(200, $message);
				}else{
					return $UpdateResult;
				}
			}else{
				if($JSONResponse){
					$message = array(
						'method' => 'Toggle Table',
						'result' => 'Failed',
					);
					echo json_response(200, $message);				
				}else{
					return false;
				}
			}
		}else{
			if($JSONResponse){
				$message = array(
					'method' => 'Toggle Table',
					'result' => 'Failed',
					'text' => 'Invalid Input',
				);
				echo json_response(400, $message);				
			}else{
				return false;
			}
		}
	}
	// cURL Process
	private function get_cURL($URL){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$URL);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		curl_close($ch);
		return $result;
	}
	private function post_cURL($URL,$DATA = ""){
		//$DATA = "queuestring=$text&counter=$call_counter&queue=$call_queue_num&type=$string[0]&locationid=$call_location";
		$POST_FIELD = "";
		foreach($DATA as $Key => $Value){
			$POST_FIELD .= ( $POST_FIELD != "" ? '&':'') . $Key . '=' . $Value;
		}
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$URL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$POST_FIELD);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		curl_close($ch);
		return $result;
	}
	private function process_trigger($Location = 0){		
		if(isset($this->session->userdata('userlogin')['locationuser'])&& $this->session->userdata('userlogin')['locationuser'] != 'LAB'){
			$location = $this->session->userdata('userlogin')['locationuser'];
			$ch1 = curl_init();
			curl_setopt($ch1, CURLOPT_URL,APITRIGGER."/trigger?location_id=$location");
			curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
			$output = curl_exec($ch1);
			curl_close($ch1);
		}
		$TriggerURL = APITRIGGER . '/trigger_lab?location_id=' . $Location;
		return $this->get_cURL($TriggerURL);
	}
	private function process_scan(){
		
	}
	private function process_call($Data){
		$TriggerURL = APITRIGGER . '/calllab';
		$Language = ( !isset($Data->nation) || $Data->nation == 'TH' || $Data->nation == 'ไทย' ? 'TH':'EN' );
		if($this->session->userdata('userlogin')['locationuser'] == "LAB"){
			switch ($Language) {
				case 'TH':
					$Word = $Language.'|เชิญหมายเลข|'.implode(str_split($Data->queueno,1),'|') .'|ที่โต๊ะเจาะเลือด|'.$Data->tableuid.'|ค่ะ';
					break;		
				case 'EN':
					$Word = $Language.'|number|'.implode(str_split($Data->queueno,1),'|') .'|examinationroom|'.$Data->tableuid;
					break;			
				default:
					$Word = $Language.'|เชิญหมายเลข|'.implode(str_split($Data->queueno,1),'|') .'|ที่โต๊ะเจาะเลือด|'.$Data->tableuid.'|ค่ะ';
					break;
			}
		}else{
			switch ($Language) {
				case 'TH':
					$Word = $Language.'|เชิญหมายเลข|'.implode(str_split($Data->queueno,1),'|') .'|ที่ห้อง|'.$Data->tableuid.'|ค่ะ';
					break;		
				case 'EN':
					$Word = $Language.'|number|'.implode(str_split($Data->queueno,1),'|') .'|examinationroom|'.$Data->tableuid;
					break;			
				default:
					$Word = $Language.'|เชิญหมายเลข|'.implode(str_split($Data->queueno,1),'|') .'|ที่ห้อง|'.$Data->tableuid.'|ค่ะ';
					break;
			}
		}
		$POST = array(
			"locationuid" => $this->session->userdata('userlogin')['locationuser'],
			"queueno" => $Data->queueno,
			"roomno" => $Data->tableuid,
			"queuestring" => $Word,
		);
		return $this->post_cURL($TriggerURL,$POST);
	}
	private function process_note(){
		
	}
	private function process_close(){
		
	}
	private function process_revert(){
		
	}
	private function process_table(){
		$TriggerURL = APITRIGGER . '/lab_update_status_counter';
		return $this->get_cURL($TriggerURL);		
	}

	// Get PatientStatus Info
	public function PatientStatus_HN($HN = NULL,$JSONResponse = TRUE){
		if($HN){
			$Search = array(
				'vw_patientdetail_today.hn' => $HN,
			);

			$QueueInfo = $this->MDL_Lab->getQueueInfo_Available($Search);
			if($QueueInfo){
				if($JSONResponse){
					$message = array(
						'method' => 'getQueueInfo',
						'hn' => $HN,
						'current_queueno' => $QueueInfo->queueno,
						'result' => 'Success',
						'data' => $QueueInfo,
					);
					echo json_response(200, $message);
				}else{
					return $QueueInfo;
				}			
			}else{
				if($JSONResponse){
					$message = array(
						'method' => 'getQueueInfo',
						'hn' => $HN,
						'result' => 'Failed',
						'text' => 'Not Found Queue Info',
					);
					echo json_response(200, $message);
				}else{
					return false;
				}
			}
		}else{
			if($JSONResponse){
				$message = array(
					'method' => 'getQueueInfo',
					'result' => 'Failed',
					'text' => 'Invalid Queueno Input',
				);
				echo json_response(400, $message);
			}else{
				return false;
			}
		}		
	}

	// Get Queue Info
	public function QueueInfo_Queueno($Queueno = NULL,$JSONResponse = TRUE){
		if($Queueno){
			$Search = array(
				'vw_patientdetail_today.queueno' => $Queueno,
			);

			$QueueInfo = $this->MDL_Lab->getQueueInfo($Search);
			if($QueueInfo){
				if($JSONResponse){
					$message = array(
						'method' => 'getQueueInfo',
						'queueno' => $Queueno,
						'result' => 'Success',
						'data' => $QueueInfo,
					);
					echo json_response(200, $message);
				}else{
					return $QueueInfo;
				}			
			}else{
				if($JSONResponse){
					$message = array(
						'method' => 'getQueueInfo',
						'queueno' => $Queueno,
						'result' => 'Failed',
						'text' => 'Not Found Queue Info',
					);
					echo json_response(200, $message);
				}else{
					return false;
				}
			}
		}else{
			if($JSONResponse){
				$message = array(
					'method' => 'getQueueInfo',
					'result' => 'Failed',
					'text' => 'Invalid Queueno Input',
				);
				echo json_response(400, $message);
			}else{
				return false;
			}
		}		
	}
	// Check Queue Closed
	public function QueueClosed($Queueno,$JSONResponse = TRUE){
		$Search = array(
			'vw_patientdetail_today.queueno' => $Queueno,
		);

		$QueueInfo = $this->MDL_Lab->getQueueInfo($Search);
		if($QueueInfo){
			if($JSONResponse){
				$message = array(
					'method' => 'getQueueInfo',
					'queueno' => $Queueno,
					'result' => 'Success',
					'closed' => ($QueueInfo->closed_queue != NULL ? true : false),
				);
				echo json_response(200, $message);
			}else{
				return ($QueueInfo->closed_queue != NULL ? true : false);
			}			
		}else{
			if($JSONResponse){
				$message = array(
					'method' => 'Check Queue Closed',
					'queueno' => $Queueno,
					'result' => 'Failed',
					'text' => 'Not Found Queue Info',
				);
				echo json_response(200, $message);
			}else{
				return false;
			}
		}
	}
	// Get Queue Waiting
	public function getQueue_Waiting($JSONReponse = TRUE){
		$Data = $this->MDL_Lab->getQueueWaiting();
		if($JSONReponse){
			if($Data){
				$message = array(
					'method' => 'getQueue_Waiting',
					'result' => 'Success',
					'data' => $Data,
				);
				echo json_response(200, $message);
			}else{
				$message = array(
					'method' => 'getQueue_Waiting',
					'result' => 'Failed',
					'text' => 'Not Found Data',
				);
				echo json_response(200, $message);
			}
		}else{
			return $Data;
		}
	}
	// Get Queue Note
	public function getQueue_Note($PUID = NULL,$JSONResponse = TRUE){
		$Search = array('patientdetailuid'=>$PUID);
		
		if($PUID != NULL){
			$SearchResult = $this->MDL_Lab->getNote($Search);
			if($SearchResult){
				if($JSONResponse){
					$message = array(
						'method' => 'Get Queue Note',
						'result' => 'Success',
						'data' => $SearchResult,
					);
					echo json_response(200, $message);
				}else{
					return $SearchResult;
				}
			}else{
				if($JSONResponse){
					$message = array(
						'method' => 'Get Queue Note',
						'result' => 'Failed',
						'text' => 'Not Found Note',
					);
					echo json_response(200, $message);
				}else{
					return false;
				}
			}
		}else{
			if($JSONResponse){
				$message = array(
					'method' => 'Get Queue Note',
					'result' => 'Failed',
					'text' => 'No Patientuid',
				);
				echo json_response(400, $message);				
			}else{
				return false;
			}
		}
	}
	// Get Queue Hold
	public function getQueue_Hold($PUID = NULL,$JSONResponse = TRUE){
		$Search = array('patientdetailuid'=>$PUID);
		
		if($PUID != NULL){
			$SearchResult = $this->MDL_Lab->getHold($Search);
			if($SearchResult){
				if($JSONResponse){
					$message = array(
						'method' => 'Get Queue Hold',
						'result' => 'Success',
						'data' => $SearchResult,
					);
					echo json_response(200, $message);
				}else{
					return $SearchResult;
				}
			}else{
				if($JSONResponse){
					$message = array(
						'method' => 'Get Queue Hold',
						'result' => 'Failed',
						'text' => 'Not Found Hold',
					);
					echo json_response(200, $message);
				}else{
					return false;
				}
			}
		}else{
			if($JSONResponse){
				$message = array(
					'method' => 'Get Queue Hold',
					'result' => 'Failed',
					'text' => 'No Patientuid',
				);
				echo json_response(400, $message);				
			}else{
				return false;
			}
		}
	}
	// Get Table
	public function getTable($JSONResponse = TRUE){
		$GetResult = $this->MDL_Lab->getTable();
		if($GetResult){
			if($JSONResponse){
				$message = array(
					'method' => 'Get Table',
					'result' => 'Success',
					'data' => $GetResult,
				);
				echo json_response(200, $message);
			}else{
				return $GetResult;
			}
		}else{
			if($JSONResponse){
				$message = array(
					'method' => 'Get Table',
					'result' => 'Failed',
				);
				echo json_response(200, $message);				
			}else{
				return false;
			}
		}
	}

	public function TestModel($Table){
		$this->load->model('MDL_Lab');
		$query = $this->MDL_Lab->DBSelect($Table);
		echo '<pre>'.var_export($query->result(),TRUE).'</pre>';
	}

	public function Test(){
		return "HI";
	}
}
