<script>
    console.log(<?= json_encode($this->session->userdata('location_id')) ?>);
    console.log(<?= json_encode($this->session->userdata('location_kiosk', 'location_kiosk')) ?>);

    var location_kiosk = JSON.parse('<?= json_encode($this->session->userdata('location_kiosk', 'location_kiosk')) ?>');

    $(document).mouseup(function(e) {
        $("#scan_prescription").focus();
    });


    $(document).ready(function() {

        var base_url = "<?= base_url() ?>";

        var queue = '';
        var when = '';
        var hn = '';
        var vn = '';
        var language = '';
        var secure = '';
        var location = '';
        var sub_location = '';

        console.log(base_url);

        $("#scan_prescription").focus();

        $("#scan_prescription").keyup(function(e) {
            if (e.keyCode == 13) {
                search_prescription();
            }
        });


        function search_prescription() {

            if ($("#scan_prescription").val() == '') {
                alert('กรุณากรอกเลข');
                return false;
            }
            $.ajax({
                url: base_url + "Kiosk/ScanPrescription",
                type: "post",
                data: {
                    prescription_no: $("#scan_prescription").val()
                },
                success: function(data) {

                    console.log(JSON.parse(data));

                    var main_data = JSON.parse(data);


                    if (main_data == false) { //ค้นหาไม่เจอ
                        md_not_found();
                        $("#md_scan").modal('show');
                    } else {

                        $.each(main_data, function(index, value) {

                            queue = value['tokenno'];
                            when = ''; // ป ด ว H I S
                            hn = value['hn'];
                            vn = value['vn'];
                            language = 'TH';
                            secure = value['secure_code'];
                            location = location_kiosk['location_kiosk'];
                            sub_location = '0';

                            if (value['medcashier_datetime'] != null || value['medpharmacy_datetime'] != null) { //จบการรักษาแล้ว
                                if (value['paymentlocation'] == location_kiosk['location_kiosk'] || value['pharmacylocation'] == location_kiosk['location_kiosk']) { // location ตรง
                                    md_find_suc_location(value['tokenno']);
                                    $("#md_scan").modal('show');
                                } else { // location ไม่ตรง
                                    md_find_suc_no_location(value['pharmacylocationname'], value['paymentlocationname'], value['tokenno']);
                                    $("#md_scan").modal('show');
                                }
                            } else { //ยังไม่จบการรักษา
                                md_not_heal();
                                $("#md_scan").modal('show');
                            }
                        });
                    } //else ใหญ่
                }
            });
        }



        function md_not_found() {

            var html = '';

            html += "<p class='col-12' style='color:red;'>ไม่พบข้อมูล กรุณาติดต่อเจ้าหน้าที่</p>";

            var btn = '';

            btn += "<button type='button' data-dismiss='modal' class='col-2 btn btn_pri btn-primary' style=''>ปิด</button> ";

            $("#md_cont").html(html);
            $("#md_btn").html(btn);
            $("#scan_prescription").val('');
        }


        function md_not_heal() {

            var html = '';

            html += "<p class='col-12' style='color:red;'>ยังไม่จบการรักษา กรุณาติดต่อเจ้าหน้าที่</p>";

            var btn = '';

            btn += "<button type='button' data-dismiss='modal' class='col-2 btn btn_pri btn-primary' style=''>ปิด</button> ";

            $("#md_cont").html(html);
            $("#md_btn").html(btn);
            $("#scan_prescription").val('');
        }




        function md_find_suc_location(tokenno) {

            var html = '';

            html += "<p class='col-12'>หมายเลขบริการของคุณ</p>";
            html += "<p class='col-12'>" + tokenno + "</p>";
            html += "<p class='col-12'>ต้องการพิมพ์บัตรคิวหรือไม่</p>";

            var btn = '';

            btn += "<button type='button' class='col-3 btn btn_pri btn-secondary print' style='margin-left: 80px;'>พิมพ์บัตรคิว</button>";
            btn += "<button type='button' data-dismiss='modal' class='col-3 btn btn_pri btn-primary' style='margin-right: 80px;'>ปิด</button>";

            $("#md_cont").html(html);
            $("#md_btn").html(btn);
            call_btn();
        }


        function md_find_suc_no_location(pharmacylocationname, paymentlocationname, tokenno) {

            var pharmacy = (pharmacylocationname == null) ? '' : pharmacylocationname;
            var payment = (paymentlocationname == null) ? '' : paymentlocationname;
            var html = '';
            html += "<p class='col-12' style='color:red;'>กรุณาติดต่อ " + pharmacy + ' ' + payment + "</p>";
            html += "<p class='col-12'>หมายเลขบริการของคุณ</p>";
            html += "<p class='col-12'>" + tokenno + "</p>";
            html += "<p class='col-12'>ต้องการพิมพ์บัตรคิวหรือไม่</p>";

            var btn = '';

            btn += "<button type='button' class='col-3 btn btn_pri btn-secondary print' style='margin-left: 80px;'>พิมพ์บัตรคิว</button>";
            btn += "<button type='button' data-dismiss='modal' class='col-3 btn btn_pri btn-primary' style='margin-right: 80px;'>ปิด</button>";

            $("#md_cont").html(html);
            $("#md_btn").html(btn);
            call_btn();
        }


        function call_btn() {
            $('.print').click(function() {

               //alert(queue + ' ' + when + ' ' + hn + ' ' + vn + ' ' + language + ' ' + secure + ' ' + location + ' ' + sub_location);
               
                $.ajax({
                    url: base_url + "Kiosk/PrintQueue",
                    type: "post",
                    data: {
                        queue: queue,
                        when: when,
                        hn: hn,
                        vn: vn,
                        language: language,
                        secure: secure,
                        location: location,
                        sub_location: sub_location
                    },
                    success: function(data) {
                        console.log(data);
                        $("#scan_prescription").val('');
                        $("#md_scan").modal('hide');
                    }
                });
            });
        }

    });
</script>