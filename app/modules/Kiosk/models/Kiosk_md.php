<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kiosk_md extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        //$this->load->database('database');
    }

    public function Scan_pres()
    {
        $prescription_no = $this->input->post('prescription_no');



        $this->db->select('*')
            ->from('vw_getscanqueue')
            ->where('prescription_no', $prescription_no)
            ->limit(10);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function print_queue()
    {

        $queue          = $this->input->post('queue');
        $when           = date("Y-m-d H:i:s");
        $hn             = $this->input->post('hn');
        $vn             = $this->input->post('vn');
        $language       = $this->input->post('language');
        $secure         = $this->input->post('secure');
        $location       = $this->input->post('location');
        $sub_location   = $this->input->post('sub_location');

        if ($language == "ไทย" || $language == "TH") {
            $language = 'TH';
        } else {
            $language = 'EN';
        }


        $print_trigger = APIPRINT;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $print_trigger);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt(
            $ch,
            CURLOPT_POSTFIELDS,
            "Language=$language&Queue=$queue&When=$when&HN=$hn&VN=$vn&Secure=$secure&location=$location&sub_location=$sub_location"
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //$queue.' '.$when.' '.$hn.' '.$vn.' '.$language.' '.$secure.' '.$location.' '.$sub_location
        return true;
    }
}
