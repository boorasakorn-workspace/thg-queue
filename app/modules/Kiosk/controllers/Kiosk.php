<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kiosk extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('assets');
		$this->load->model('Kiosk_md');
		// $this->load->module('StepProcessing/PersonType');
	}

	public function create_location()
	{
		$location_id = 'C2';

		$data = array('location_id' => $location_id);

		$this->session->set_userdata('location_id', $data);

		echo json_encode($this->session->userdata('location_id'));
	}

	public function kiosktemplate($page, $title, $script)
	{
		$this->load->module('Template_Module');

		$Template = array(
			'Module' => 'Kiosk',
			'Site_Title' => $title,
			'Content' => $page,
			'Script' => array(
				'Script' => $script,
			),
			'viewCSS' => array(
				'css' => 'Kiosk_style',
			),
		);
		$this->template_module->Template('Kiosk_tem', $Template);
	}

	public function Kiosk_main($location = null)
	{

		$data = array('location_kiosk' => $location);

		$this->session->set_userdata('location_kiosk', $data);

		//echo json_encode($this->session->userdata('location_kiosk'));


		$page = array('Main' => 'Kiosk_vw');
		$title = 'Kiosk';
		$script = 'Kiosk_script';
		$this->kiosktemplate($page, $title, $script);
	}

	public function ScanPrescription()
	{

		$result = $this->Kiosk_md->Scan_pres();

		echo json_encode($result);
	}

	public function PrintQueue()
	{
		$result = $this->Kiosk_md->print_queue();

		echo json_encode($result);
	}
}
